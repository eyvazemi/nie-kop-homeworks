#ifndef DYNAMIC_ARRAY_ENTRY_H
#define DYNAMIC_ARRAY_ENTRY_H

#include <vector>

struct DynamicArrayEntry {
    std::vector<int> m_vec_items; // items that are currenty in the knapsack
    int m_weight, m_cost;
};

#endif // DYNAMIC_ARRAY_ENTRY_H
