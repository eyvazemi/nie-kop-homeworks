#include "knapsack.h"

KnapSack::KnapSack(const std::vector<int> & items, const std::vector<int> & solution_items, bool flag_target_cost) {
    int items_start_id = 4;
    m_items_count = 0;
    m_real_items_count = 0;
    m_max_weight = items[2];

    // Solution could not be present, thus we have to check for it.
    const bool is_solution_present = solution_items.size() >= 2;

    if(is_solution_present) {
        m_solution_cost = solution_items[2];
    }

    if(flag_target_cost)
        m_target_cost = items[3];
    else {
        m_target_cost = -1;
        items_start_id = 3;
    }

    for(int i = items_start_id, j = 3; i < items.size(); i += 2, ++j) {
        Item item;
        item.m_weight = items[i];
        item.m_cost = items[i + 1];
        item.m_real_id = (i - items_start_id) / 2;

        if(item.m_weight <= m_max_weight) {
            m_items.push_back(item);

            if(is_solution_present) {
                m_solution.push_back(solution_items[j]);
            }

            ++m_items_count;
        }

        ++m_real_items_count;
    }
}

bool KnapSack::compare_solutions(const std::vector<int> & vec_solution, int solution_cost) const {
    if(vec_solution.size() != m_solution.size() || m_solution_cost != solution_cost)
        return false;

    for(int i = 0; i < m_solution.size(); i++) {
        if(m_solution[i] != vec_solution[i])
            return false;
    }

    return true;
}

int KnapSack::get_max_weight() const {
    return m_max_weight;
}

int KnapSack::get_items_count() const {
    return m_items_count;
}

int KnapSack::get_real_items_count() const {
    return m_real_items_count;
}

int KnapSack::get_target_cost() const {
    return m_target_cost;
}

int KnapSack::get_solution_cost() const {
    return m_solution_cost;
}

const std::vector<Item> * KnapSack::get_items() const {
    return &m_items;
}

const std::vector<int> * KnapSack::get_solution() const {
    return &m_solution;
}
