#include "sat.h"

SAT::SAT(const std::vector<int> & clauses, const std::vector<int> & weights,
    int num_variables, int num_clauses, int solution_weight
) : m_weights(weights), m_num_variables(num_variables), 
    m_solution_weight(solution_weight)
{
    for(int clause_id = 0; clause_id < clauses.size(); ++clause_id) {
        SATClause sat_clause;

        for(int variable_id = 0; variable_id < SAT_CLAUSE_SIZE; ++variable_id) {
            // We are multiplying the clause index by 4, as every clause consists of 3 
            // variables + 0 at the end, to indicate the end of the clause.
            // As the clause is 0 terminated, the variables start with 1, 
            // which means that the first variable has index 1 and not 0.
            // A positive variable, e.g. 3, has actual index 2 and a negative
            // variable, e.g. -2, has actual index -1. Thus, we have to add -1 to the 
            // positive variable and +1 to the negative variable to obtain their actual index.
            int variable = clauses[clause_id * 4 + variable_id];
            sat_clause.m_variables[variable_id] = variable > 0 ? variable - 1 : variable + 1;
        }

        m_clauses.push_back(sat_clause);
    }

    // Usually file contains number of clauses that is greater by 1,
    // than the number of actual clauses, thus we deduct 1.
    m_num_clauses = num_clauses - 1;
}

SAT::~SAT() = default;

SATClause SAT::get_clause(int clause_id) const {
    return m_clauses[clause_id];
}

int SAT::get_weight(int variable_id) const {
    return m_weights[variable_id];
}

int SAT::get_num_variables() const {
    return m_num_variables;
}

int SAT::get_num_clauses() const {
    return m_num_clauses;
}

int SAT::get_solution_weight() const {
    return m_solution_weight;
}
