#ifndef SAT_H
#define SAT_H

#include <vector>

#define SAT_CLAUSE_SIZE 3

struct SATClause {
    // Clauses of exactly 3 variables per each.
    std::vector<int> m_variables = std::vector<int>(SAT_CLAUSE_SIZE);
};

class SAT {
public: // methods
    SAT(const std::vector<int> & clauses, const std::vector<int> & weights, 
        int num_variables, int num_clauses, int solution_weight
    );

    ~SAT();

    SATClause get_clause(int clause_id) const;
    int get_weight(int variable_id) const;
    int get_num_variables() const;
    int get_num_clauses() const;
    int get_solution_weight() const;

private: // members
    // SAT clauses.
    std::vector<SATClause> m_clauses;

    // Weights of all variables.
    std::vector<int> m_weights;

    int m_num_variables;
    int m_num_clauses;

    // The optimum that is given to us.
    int m_solution_weight;
};

#endif // SAT_H
