#ifndef KNAPSACK_H
#define KNAPSACK_H

#include <iostream>
#include <vector>
#include "item.h"

class KnapSack {
public:
    KnapSack(const std::vector<int> & items, const std::vector<int> & solution_items, bool flag_target_cost = false);
    ~KnapSack() = default;
    int get_max_weight() const;
    int get_items_count() const;

    /**
     * @brief Returns the real number of items before items
     * with weight larger than maximum were thrown off.
     */
    int get_real_items_count() const;

    int get_target_cost() const;
    int get_solution_cost() const;
    const std::vector<Item> * get_items() const;
    const std::vector<int> * get_solution() const;
    void add_solution(const std::vector<int> & items);
    bool compare_solutions(const std::vector<int> & vec_solution, int solution_cost) const;
private:
    std::vector<Item> m_items;
    std::vector<int> m_solution;
    int m_max_weight;
    int m_items_count;
    int m_real_items_count;
    int m_target_cost;
    int m_solution_cost;
};

#endif // KNAPSACK_H
