#ifndef EXEC_RESULTS_H
#define EXEC_RESULTS_H

#include <set>
#include <string>
#include <vector>

struct ExecResults {
    std::vector<float> m_costs; // used only for Simulated Annealing.
    std::set<int> m_set_item_ids;
    std::string m_solver_name;
    double m_complexity;
    double m_elapsed_time;
    float m_simulated_annealing_initial_temperature;
    int m_highest_cost;
    int m_total_weight;
    int m_fptas_num_chunks;
    int m_num_unsat_clauses; // used only in SAT.
};

#endif // EXEC_RESULTS_H
