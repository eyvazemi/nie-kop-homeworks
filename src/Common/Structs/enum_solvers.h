#ifndef ENUM_SOLVERS_H
#define ENUM_SOLVERS_H

enum class EnumSolvers {
    BruteForce,
    BranchBound,
    Heuristics,
    ExtendedHeuristics,
    DynamicProgrammingWeight,
    DynamicProgrammingCost,
    DynamicProgrammingCostFPTAS,
    SimulatedAnnealingKnapsack,
    SimulatedAnnealingSAT
};

enum class EnumProblemType {
    Decision,
    Optimisation,
    FPTAS,
    Experimental,
    SimulatedAnnealingKnapsack,
    SimulatedAnnealingSAT
};

#endif // ENUM_SOLVERS_H
