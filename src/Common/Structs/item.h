#ifndef ITEM_H
#define ITEM_H

struct Item {
    int m_weight;
    int m_cost;
    int m_real_id; // index of this item in the original items list
};

#endif // ITEM_H
