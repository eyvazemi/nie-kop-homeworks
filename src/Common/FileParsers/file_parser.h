#ifndef FILE_PARSER_H
#define FILE_PARSER_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <cmath>
#include "../Structs/knapsack.h"

class FileParser {
public:
    FileParser() = default;
    virtual ~FileParser();

    /**
    * @brief Parses the file and returns the knapsacks within the given range.
    *
    * @param Name of the file that will be parsed.
    * @param Name of the solution file that will be passed.
    *   In case if it is an empty string, solution file won't be parsed.
    *  
    * @param Id of the first knapsack in the file that will be parsed.
    *   Line counting starts with 0.
    * 
    * @param Id of the last knapsack in the file that will be parsed.
    */
    std::vector<KnapSack *> parse(const std::string & file_name_instance,
        const std::string & file_name_solution, int range_start, int range_end
    );

protected:
    virtual KnapSack * parse_line(const std::vector<int> & vec_items,
        const std::vector<int> & vec_solution
    ) = 0;
};

#endif // FILE_PARSER_H
