#include "file_parser_decision.h"

KnapSack * FileParserDecision::parse_line(const std::vector<int> & vec_items,
    const std::vector<int> & vec_solution
) {
    KnapSack * knapsack = new KnapSack(vec_items, vec_solution, true);
    return knapsack;
}
