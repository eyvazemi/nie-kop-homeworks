#ifndef FILE_PARSER_OPTIMISATION_H
#define FILE_PARSER_OPTIMISATION_H

#include "file_parser.h"
#include "../Structs/knapsack.h"

class FileParserOptimisation : public FileParser {
public:
    FileParserOptimisation() : FileParser() {}
    ~FileParserOptimisation() = default;

private:
    KnapSack * parse_line(const std::vector<int> & vec_items,
        const std::vector<int> & vec_solution
    ) override;
};

#endif // FILE_PARSER_OPTIMISATION_H
