#include "file_parser_optimisation.h"

KnapSack * FileParserOptimisation::parse_line(const std::vector<int> & vec_items,
    const std::vector<int> & vec_solution
) {
    KnapSack * knapsack = new KnapSack(vec_items, vec_solution, false);
    return knapsack;
}
