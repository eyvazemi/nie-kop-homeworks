#include <sstream>
#include "file_parser.h"

FileParser::~FileParser() {}

std::vector<KnapSack *> FileParser::parse(const std::string & file_name_instance,
        const std::string & file_name_solution, int range_start, int range_end
) {
    int knapsack_id = 1, line_counter = 0, num;
    std::vector<KnapSack *> vec_knapsacks;
    std::string line_inst, line_sol;
    char c;

    // open the file for reading
    std::fstream file_inst, file_sol;
    file_inst.open(file_name_instance, std::ios::in);

    if(!file_name_solution.empty()) {
        file_sol.open(file_name_solution, std::ios::in);
    }

    // read file line by line
    while(std::getline(file_inst, line_inst)) {
        // check if line id is out of bounds
        if(range_start > line_counter) {
            line_counter++;
            continue;
        }

        if(range_end < line_counter)
            break;

        // create vector for knapsack
        std::vector<int> vec_items;
        std::vector<int> vec_solution;

        // parse string into vector
        std::stringstream iss_inst(line_inst);
        
        while(iss_inst >> num)
            vec_items.push_back(num);

        if(file_sol.is_open()) {
            // get the correct solution
            while(std::getline(file_sol, line_sol)) {
                // parse solution line
                std::stringstream iss_sol(line_sol);
                vec_solution.clear();

                while(iss_sol >> num)
                    vec_solution.push_back(num);

                // check if knapsack id in the solution is equal
                //  to the one in the items file
                if(std::abs(vec_solution[0]) == std::abs(vec_items[0]))
                    break;
            }
        }

        //--
        /*std::cout << "items: " << std::endl;
        for(const auto & it : vec_items)
            std::cout << it << ", ";
        std::cout << std::endl;

        std::cout << "solution: " << std::endl;
        for(const auto & it : vec_solution)
            std::cout << it << ", ";
        std::cout << std::endl;*/
        //--

        // create knapsack with given items
        vec_knapsacks.push_back(parse_line(vec_items, vec_solution));

        line_counter++;
    }

    file_inst.close();

    if(file_sol.is_open()) {
        file_sol.close();
    }

    return vec_knapsacks;
}
