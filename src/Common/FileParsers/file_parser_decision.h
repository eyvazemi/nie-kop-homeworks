#ifndef FILE_PARSER_DECISION_H
#define FILE_PARSER_DECISION_H

#include "file_parser.h"
#include "../Structs/knapsack.h"

class FileParserDecision : public FileParser {
public:
    FileParserDecision() : FileParser() {}
    ~FileParserDecision() = default;

private:
    KnapSack * parse_line(const std::vector<int> & vec_items,
        const std::vector<int> & vec_solution
    ) override;
};

#endif
