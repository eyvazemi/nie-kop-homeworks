#include "file_csv_parser.h"

void FileCSVParser::write_line(
    const std::string & file_name,
    const std::vector<std::string> & vec_csv_header, 
    const std::vector<std::string> & vec_values
) {
    std::string file_name_correct = file_name.find(".csv") == std::string::npos ? file_name + ".csv" : file_name;
    std::ifstream file_csv_read;
    std::ofstream file_csv_write;
    file_csv_read.open(file_name_correct, std::ios::in);
    file_csv_write.open(file_name_correct, std::ios::app);

    // if file is empty, write the csv header into it
    if(file_csv_read.peek() == std::ifstream::traits_type::eof()) {
        std::string str_header = FileCSVParser::create_csv_line(vec_csv_header);
        file_csv_write.write(str_header.c_str(), str_header.length());
    }

    file_csv_read.close();

    // create a csv line and add to the file
    std::string str_csv_line = FileCSVParser::create_csv_line(vec_values);
    file_csv_write.write(str_csv_line.c_str(), str_csv_line.length());

    file_csv_write.close();
}

void FileCSVParser::write_full(
    const std::string & file_name,
    const std::vector<std::string> & vec_csv_header, 
    const std::vector<std::vector<std::string>> & vec_values
) {
    std::ofstream file_csv;
    std::string file_name_correct = file_name.find(".csv") == std::string::npos ? file_name + ".csv" : file_name;
    std::string str_csv_contents = FileCSVParser::create_csv_line(vec_csv_header);

    // open a csv file
    file_csv.open(file_name_correct, std::ios::out);

    // create a string of csv contents
    for(const auto & vec_csv_line : vec_values)
        str_csv_contents += FileCSVParser::create_csv_line(vec_csv_line);
    
    // write into csv file
    file_csv.write(str_csv_contents.c_str(), str_csv_contents.length());

    // close csv file
    file_csv.close();
}

std::string FileCSVParser::create_csv_line(const std::vector<std::string> & vec_values) {
    std::string str_csv_line;

    for(int i = 0; i < vec_values.size(); i++) {
        str_csv_line += vec_values[i];
        if(i == vec_values.size() - 1)
            str_csv_line += '\n';
        else
            str_csv_line += ',';
    }

    return str_csv_line;
}
