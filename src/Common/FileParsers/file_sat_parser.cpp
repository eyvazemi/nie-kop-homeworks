#include "file_sat_parser.h"

FileSATParser::FileSATParser() = default;

FileSATParser::~FileSATParser() = default;

SAT FileSATParser::parse(const std::string & file_name_instance, const std::string & file_name_solution) {
    // Open the file for reading.
    std::fstream file_inst;
    file_inst.open(file_name_instance, std::ios::in);

    // Vector with variables that represent all clauses of this SAT instance.
    std::vector<int> vec_variables;

    // Vector with weights.
    std::vector<int> vec_weights;

    // Line that will each instance from the file will be parsed in.
    std::string line_inst;

    int num_variables = -1;
    int num_clauses = -1;
    int solution_weight = -1;

    // First 11 lines are just a metadata, thus we parse them separately.
    for(int line_id = 0; line_id < 11; ++line_id) {
        // Get the current line.
        std::getline(file_inst, line_inst);

        // Parse string into vector.
        std::stringstream iss_inst(line_inst);

        if(line_id == 7) {
            // This line contains the number of variables and number of clauses.
            std::string mwcnf;
            char p;

            iss_inst >> p;
            iss_inst >> mwcnf;
            iss_inst >> num_variables;
            iss_inst >> num_clauses;
        } else if(line_id == 8) {
            // This line contains the solution file name and the line in that
            // file with the solution weight value for this SAT instance.
            std::string SAT;
            std::string instance;
            std::string solution_name;
            char c;

            iss_inst >> c;
            iss_inst >> SAT;
            iss_inst >> instance;
            iss_inst >> solution_name;

            // Solution name consists of <solution-file>/<line-in-solution-file>.cnf.
            // Thus, we have to open the solution file, which is located in 
            // `solutions_dir`/<solution-file>.csv and find the line
            // <line-in-solution-file>;<solution-value> and extract <solution-value> from that line.
            size_t position_slash = solution_name.find('/');
            std::string solution_file = solution_name.substr(0, position_slash);
            std::string line_in_solution_file = solution_name.substr(position_slash + 1, solution_name.find('.') - position_slash - 1);

            std::fstream file_solution;
            file_solution.open(file_name_solution, std::ios::in);

            while(std::getline(file_solution, line_inst)) {
                // This line contains the solution we are looking for,
                // thus take value after ';' character.
                if(line_inst.find(line_in_solution_file + ' ') != std::string::npos) {
                    const std::string str_with_solution = line_inst.substr(line_inst.find(' ') + 1);
                    const int space_position = str_with_solution.find(' ');
                    solution_weight = std::stoi(str_with_solution.substr(0, space_position));
                    break;
                }
            }

            file_solution.close();
        } else if(line_id == 9) {
            int weight;
            char w;

            iss_inst >> w;

            for(int variable_id = 0; variable_id < num_variables; ++variable_id) {
                iss_inst >> weight;
                vec_weights.push_back(weight);
            }
        }
    }

    // Read file line by line.
    while(std::getline(file_inst, line_inst)) {
        // Parse string into vector.
        std::stringstream iss_inst(line_inst);

        // Number, where each SAT clause variable will be parsed in.
        int num;
        
        while(iss_inst >> num) {
            vec_variables.push_back(num);
        }
    }

    file_inst.close();

    // Create SAT instance with parsed clause variables.
    return SAT(vec_variables, vec_weights, num_variables, num_clauses, solution_weight);
}
