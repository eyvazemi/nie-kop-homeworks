#ifndef FILE_CSV_PARSER_H
#define FILE_CSV_PARSER_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

class FileCSVParser {
public:
    FileCSVParser() = default;
    ~FileCSVParser() = default;

    /**
    * @brief Writes one line into CSV file.
    *
    * @param Name of the csv file.
    * @param Vector with csv header.
    * @param Vector with items.
    */
    static void write_line(const std::string & file_name, 
        const std::vector<std::string> & vec_csv_header,
        const std::vector<std::string> & vec_values
    );

    /**
    * @brief Write the full CSV file.
    *
    * @param Name of the csv file.
    * @param Vector with csv header.
    * @param Vector with items.
    */
    static void write_full(const std::string & file_name, 
        const std::vector<std::string> & vec_csv_header,
        const std::vector<std::vector<std::string>> & vec_values
    );

private:
    /**
     * @brief Create a string of comma separated values.
     *
     * @param Vector with values that will be in csv file.
     *
     * @return Comma separated string of values given in parameter.
     */
    static std::string create_csv_line(const std::vector<std::string> & vec_values);
};

#endif
