#ifndef FILE_SAT_PARSER_H
#define FILE_SAT_PARSER_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <cmath>
#include "../Structs/sat.h"

class FileSATParser {
public: // methods
    FileSATParser();
    ~FileSATParser();

    /**
    * @brief Parses the file and returns the knapsacks within the given range.
    *
    * @param file_name_instance of the file that will be parsed.
    * @param file_name_solution Solutions file.
    */
    static SAT parse(const std::string & file_name_instance, const std::string & file_name_solution);
};

#endif // FILE_SAT_PARSER_H
