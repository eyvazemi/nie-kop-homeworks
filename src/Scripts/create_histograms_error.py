import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
import sys
from os.path import exists
import random as rand

# read arguments
in_file_name = sys.argv[1]
out_file_name = sys.argv[2]
y_label = sys.argv[3]
instance_name = sys.argv[4]

if not exists(in_file_name):
    print('File ' + in_file_name + ' does not exist.')
    exit(1)

# parse csv file into dataframe
df = pd.read_csv(in_file_name)

# get csv headers
list_csv_headers = list(df.columns.values)

# get list of solver names
list_solver_names = list_csv_headers[1:]

# assign color to each solver
list_solver_colors = []
for i in range(0, len(list_solver_names)):
    r = rand.random()
    g = rand.random()
    b = rand.random()
    if((r, g, b) not in list_solver_colors):
        list_solver_colors.append((r, g, b))

# give size to the figure
plt.figure(figsize = (10, 6))

# set labels to the axes
plt.xlabel('Instance size (' + instance_name + ' instances)')
plt.ylabel(y_label)

# create a list of X and Y coordinate values for histgram bars
list_X_values = []
list_Y_values = []
counter = 0

for i in df.index:
    for (solver_id, solver_name) in enumerate(list_solver_names):
        list_X_values.append(df['Instance size'][i] + (solver_id - len(list_solver_names) / 2) * 0.5)
        list_Y_values.append(df[solver_name][i] + 0.5)
        
        if i == 0:
            plt.bar(list_X_values[counter], list_Y_values[counter], label = solver_name, \
                color = list_solver_colors[solver_id], edgecolor = 'black', bottom = -0.5 \
            )
        else:
            plt.bar(list_X_values[counter], list_Y_values[counter], \
                color = list_solver_colors[solver_id], edgecolor = 'black', bottom = -0.5 \
            )

        counter += 1

# logarithmically scale the Y-axis
ax = plt.gca()
ax.set_yscale('log')

# set yticks
plt.yticks()

# show a legend
#plt.legend(bbox_to_anchor=(0, -0.15, 1.13, 0), loc = "lower right")
plt.legend()

# save the histogram as an image
plt.savefig(out_file_name)
