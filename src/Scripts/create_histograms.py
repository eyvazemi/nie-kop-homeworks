import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
import sys
from os.path import exists

# read arguments
in_file_name = sys.argv[1]
out_file_name = sys.argv[2]

if not exists(in_file_name):
    print('File ' + in_file_name + ' does not exist.')
    exit(1)

# parse csv file into dataframe
df = pd.read_csv(in_file_name)

# give size to the figure
plt.figure(figsize = (8, 5))

# set labels to the axes
plt.xlabel("Complexity")
plt.ylabel("Frequency")

# create a histogram
plt.bar(df['Complexity'], df['Frequency'])

# save the histogram as an image
plt.savefig(out_file_name)
