import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
import sys
from os.path import exists
import random as rand
from os import walk

# Returns the float value between the subtring and next '_' character.
def extract_parameter_from_str(orig_str, sub_str):
    substr_id = orig_str.find(sub_str)
    float_id = substr_id + len(sub_str)
    underscore_id = orig_str.find('_', float_id)

    return orig_str[float_id : underscore_id]


def main():
    execution_dir = sys.argv[1]
    csv_file_name_head = sys.argv[2]
    graphs_dir = sys.argv[3]

    list_csv_files = []

    for (dirpath, dirnames, filenames) in walk(execution_dir):
        # We are only interested in the CSV files that contain costs,
        # thus we pick only those.
        for filename in filenames:
            if csv_file_name_head in filename:
                list_csv_files.append(filename)

        break

    for csv_file in list_csv_files:
        # Parse csv file into dataframe.
        df = pd.read_csv(execution_dir + '/' + csv_file)

        # Give size to the figure.
        plt.figure(figsize = (10, 6))

        # Set labels to the axes.
        plt.xlabel('Iteration')
        plt.ylabel('Cost')

        # Create a scatter plot.
        plt.scatter(df['Iteration'], df['Cost'], marker = '.', color = 'blue')

        # Scale the iterations logarithmically, as there is a high chance of the optimum
        # being reached quite soon, resulting the plot to be squashed on the left.
        ax = plt.gca()
        ax.set_xscale('log')

        # Get the instance type and size based on the filename.
        # It comes right after the common for all CSVs with cost header.
        instance_type_underscore_id = csv_file.find('_', len(csv_file_name_head + '_'))
        instance_type = csv_file[len(csv_file_name_head + '_') : instance_type_underscore_id]

        instance_size_underscore_id = csv_file.find('_', instance_type_underscore_id + 1)
        instance_size = csv_file[instance_type_underscore_id + 1 : instance_size_underscore_id]

        # Get the Simulated Annealing parameters based on the filename.
        final_temperature = extract_parameter_from_str(csv_file, '_f_')
        cooling_alpha = extract_parameter_from_str(csv_file, '_c_')
        heating_alpha = extract_parameter_from_str(csv_file, '_h_')
        inner_loop_to_items_ratio = extract_parameter_from_str(csv_file, '_i_')

        # Give a title to the plot.
        plot_title = 'Cost per iteration with instance type: ' + instance_type + ' ' + str(instance_size) + \
            ', final temperature: ' + str(final_temperature) + '\nCooling alpha: ' + str(cooling_alpha) + \
            ', heating alpha: ' + str(heating_alpha) + ', inner loop to items ratio: ' + str(inner_loop_to_items_ratio)

        plt.suptitle(plot_title)

        # Save the plot into the image file.
        image_file_name = graphs_dir + '/simulated_annealing_costs_' + instance_type + '_' + str(instance_size) + \
            '_f_' + str(final_temperature) + '_c_' + str(cooling_alpha) + '_h_' + str(heating_alpha) + \
            '_i_' + str(inner_loop_to_items_ratio) + '.png'

        plt.savefig(image_file_name)

        plt.close()


# Call `main()` function if this script is called directly.
if __name__ == '__main__':
    main()