import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
import sys
from os.path import exists
import random as rand

def generate_graph(str_x_label, str_y_label, list_solver_names, \
    list_x_values, dict_y_values, measurement, str_title, str_image_file_name \
):
    # Give size to the figure.
    plt.figure(figsize = (10, 6))

    # Set labels to the axes.
    plt.xlabel(str_x_label)
    plt.ylabel(str_y_label)

    # Markers that will be used for each solver in the plot.
    # These markers are static, thus we will overuse them, 
    # in case if there are more solvers, by finding a modulus
    # of the solver index with the size of this list of markers.
    list_plot_markers = ['o', '^', 's', 'x', 'D', '+']

    for solver_id, solver_name in enumerate(list_solver_names):
        # Some X values could not have a Y values, thus we have to 
        # take as many X values, as there are Y values.
        plt.plot(list_x_values[0: len(dict_y_values[solver_name])], dict_y_values[solver_name], \
            label = solver_name, marker = list_plot_markers[solver_id % len(list_plot_markers)], \
            markerfacecolor = 'none', alpha = 1 - solver_id * 0.15  \
        )


    plt.suptitle(str_title)

    if 'time' in measurement:
        ax = plt.gca()
        ax.set_yscale('log')

    # Show legend.
    plt.legend()

    # Save the graph as an image.
    plt.savefig(str_image_file_name)

    plt.close()

def main():
    csv_file_path = sys.argv[1]
    graphs_dir = sys.argv[2]
    default_maximum_cost = sys.argv[3]
    default_maximum_weight = sys.argv[4]
    default_capacity_total_weight_ratio = sys.argv[5]
    default_cost_weight_correlation = sys.argv[6]
    default_weight_balance = sys.argv[7]
    default_granularity = sys.argv[8]

    dict_default_parameter_values = {
        'Maximum cost': default_maximum_cost,
        'Maximum weight': default_maximum_weight,
        'Capacity to total weight ratio': default_capacity_total_weight_ratio,
        'Cost/weight correlation': default_cost_weight_correlation,
        'Weight balance': default_weight_balance,
        'Granularity': default_granularity
    }

    if not exists(csv_file_path):
        print('File ' + csv_file_path + ' does not exist.')
        exit(1)

    # Parse csv file into dataframe.
    df = pd.read_csv(csv_file_path)

    # Get csv headers and a list of parameters,
    # that are the first 7 headers of the CSV headers.
    # We exclude the first header from the parameters list,
    # as it is an instance size that is variated over every other 
    # variation of other parameters.
    list_csv_headers = list(df.columns.values)
    list_parameters = list_csv_headers[1:7]

    # Get unique values of each parameter of the instance generator.
    list_instance_size = df['Instance size'].unique()
    list_maximum_cost = df['Maximum cost'].unique()
    list_maximum_weight = df['Maximum weight'].unique()
    list_capacity_to_total_weight_ratio = df['Capacity to total weight ratio'].unique()
    list_cost_weight_correlation = df['Cost/weight correlation'].unique()
    list_weight_balance = df['Weight balance'].unique()
    list_granularity = df['Granularity'].unique()

    # Data frame for instance size parameter graphs.
    df_instance_size = pd.DataFrame()

    # Dictionary with solver names as keys and an instance size as value,
    # up until which that solver was tested.
    dict_solver_name_instance_size = {}

    # We will group instances by their size and for instances of the same size,
    # we will create a list of permutations, where each permutation will have the
    # same instance generator parameters.
    for instance_size in list_instance_size:
        # Get the name of the solvers, for that, we start iterating the
        # list of CSV header names from 7, as first 7 parameters are the
        # parameter names of instance generator. We iterate by 4, as
        # each solver has 4 CSV headers.
        # This is done for each instance size, as not every solver is run
        # for every instance size.
        list_solver_names = []

        for csv_header in list_csv_headers[7::4]:
            # The name of the solver is followed by either '(averate error/time)'
            # or '(maximum error/type)', thus the name of the solver comes before
            # the opening paranthesis. There is also a space between the name of the
            # solver and an opening paranthesis, thus we have to remove the last space too.
            solver_name = csv_header.partition('(')[0].rstrip(' ')
            
            # In case if the maximum time of the given solver is 0 (we compare floating point values),
            # then this solver didn't run at all, thus we can not include it into the list of solver names.
            solver_maximum_time = float(df[df['Instance size'] == instance_size][solver_name + ' (maximum time)'].iloc[0])

            if abs(solver_maximum_time - 0) <= min(abs(solver_maximum_time), 0) * 1e-05:
                continue

            dict_solver_name_instance_size[solver_name] = instance_size
            list_solver_names.append(solver_name)

        # Create a list of data frames with permutations of instances,
        # i.e. instances with the same values of the instance generator parameters.
        list_permutations = []

        for index, row in (df[df['Instance size'] == instance_size]).iterrows():
            # Search the list for the existing permutation.
            # If it doesn't exist, add it there.

            # Create a dictionary from the row.
            dict_row = {}

            for key in row.keys():
                dict_row[key] = row[key]

            # Set to `True` if permutation with the same
            # parameters was found in the list of permutations.
            flag_permutation_found = False
            
            for permudation_id, permutation_df in enumerate(list_permutations):
                flag_permutation_found = True

                for parameter in list_parameters:
                    # We don't create a separate permutation for `Weight balance`,
                    # as it will be taken together with `Granularity` parameter.
                    if parameter == 'Weight balance':
                        continue

                    if dict_row[parameter] != permutation_df[parameter].iloc[0]:
                        flag_permutation_found = False
                        break

                if flag_permutation_found:
                    # Permutation was found in the list, thus simply
                    # append this row to this permutation's data frame.
                    list_permutations[permudation_id] = pd.concat([list_permutations[permudation_id], \
                        pd.DataFrame([dict_row])], ignore_index = True \
                    )

                    break

            if flag_permutation_found == False:
                # It is the last permutation in the list and a permutation
                # with given parameters wasn't found, thus we have to add it.
                new_df = pd.DataFrame(dict_row, index=[0])
                list_permutations.append(new_df)

        # todo:_ remove this
        #for permudation_id, permutation_df in enumerate(list_permutations):
        #    print(permutation_df.iloc[:, 0: 7])

        #exit(0)

        # For each parameter, create a data frame with rows, where only
        # this parameter's values are variated.
        for parameter in list_parameters:
            # We don't create a graph for `Weight balance` parameter,
            # as it will be generated along with the `Granularity` parameter.
            if parameter == 'Weight balance':
                continue

            # Create the empty data frame with the columns of the given CSV file header.
            df_parameter = pd.DataFrame(columns = list_csv_headers)

            for permutation in list_permutations:
                # We always take the first row of the given permutation,
                # as all other rows are permutations of the instance
                # from the first row.
                parameter_value = permutation[parameter].iloc[0]
                parameter_row = permutation.iloc[0]

                # In order to create a data frame, where only given parameter's value is
                # different in each row, we should make sure, that all the other parameters
                # have a default value in this permutation.
                flag_wrong_permutation = False
                
                for other_parameter in list_parameters:
                    if other_parameter == parameter or other_parameter == 'Weight balance':
                        continue

                    if parameter_row[other_parameter] != dict_default_parameter_values[other_parameter]:
                        # There is a parameter in this permutation that doesn't have a default value,
                        # thus, we have to continue to another permutation.
                        flag_wrong_permutation = True
                        break

                # todo:_ check this
                #if flag_wrong_permutation:
                #    continue

                # For the 'Granularity' parameter, we should also take
                # into account `Weight balance` parameter, which means
                # that we also add the rows from this permutation that have 
                # different values of 'Weight balance' parameter.
                if parameter == 'Granularity':
                    for parameter_weight_balance in list_weight_balance:
                        if parameter_weight_balance in permutation['Weight balance'].values and \
                            len(df_parameter.loc[(df_parameter['Weight balance'] == parameter_weight_balance) & \
                            (df_parameter['Granularity'] == parameter_value)] \
                        ) == 0:
                            row_weight_balance = permutation.loc[permutation['Weight balance'] == parameter_weight_balance].iloc[0]

                            if df_parameter.empty:
                                # As warning occurs, when we concatenate an empty data frame
                                # with the row, we check if the data frame is empty, in which 
                                # case we just instantiate it with this row.
                                df_parameter = pd.DataFrame([row_weight_balance])
                            else:
                                df_parameter = pd.concat([df_parameter, \
                                    pd.DataFrame([row_weight_balance])], ignore_index = True \
                                )
                elif parameter_value not in df_parameter[parameter].values:
                    if df_parameter.empty:
                        # As warning occurs, when we concatenate an empty data frame
                        # with the row, we check if the data frame is empty, in which 
                        # case we just instantiate it with this row.
                        df_parameter = pd.DataFrame([parameter_row])
                    else:
                        df_parameter = pd.concat([df_parameter, \
                            pd.DataFrame([parameter_row])], ignore_index = True \
                        )

            # Sort the rows by the given parameter value in case if this column is of numerical type.
            df_parameter = df_parameter.sort_values(by = [parameter])

            # todo:_ remove this
            #if parameter == 'Granularity':
            #    print(df_parameter[['Weight balance', 'Granularity']])

            # todo:_ remove this
            #continue

            # Create graphs of average error/time and maximum error/time for each solver.
            for measurement in ['average error', 'maximum error', 'average time', 'maximum time']:
                # This loop is only for 'Granularity' parameter, as for every value of 'Weight balance'
                # we will create a plot with all 'Granularity' values. For all other parameters,
                # this loop will run only once.
                for parameter_weight_balance in list_weight_balance:
                    # Give size to the figure.
                    plt.figure(figsize = (10, 6))

                    # Set labels to the axes.
                    plt.xlabel(parameter)
                    plt.ylabel(measurement.title())

                    # Markers that will be used for each solver in the plot.
                    # These markers are static, thus we will overuse them, 
                    # in case if there are more solvers, by finding a modulus
                    # of the solver index with the size of this list of markers.
                    list_plot_markers = ['o', '^', 's', 'x', 'D', '+']

                    list_X_values = df_parameter.loc[df_parameter['Weight balance'] == \
                            parameter_weight_balance][parameter]

                    # All Y-axis values of all solvers.
                    list_Y_values = []

                    for solver_id, solver_name in enumerate(list_solver_names):
                        # In order to be able to differentiate intersecting plots,
                        # each plot is given an `alpha` value for its color, in such a way
                        # that the first plot has `alpha = 1`, and every next graph, 
                        # is plotted with lower alpha value than the previous one, so that
                        # in case if it overlaps with one of the previous plots, that previous
                        # plot will be visible, as it will have higher alpha value than the 
                        # currently drawn one.
                        solver_Y_values = df_parameter.loc[df_parameter['Weight balance'] == \
                            parameter_weight_balance][solver_name + ' (' + measurement + ')']

                        plt.plot(list_X_values, solver_Y_values, \
                            label = solver_name, marker = list_plot_markers[solver_id % len(list_plot_markers)], \
                            markerfacecolor = 'none', alpha = 1 - solver_id * 0.15  \
                        )

                        list_Y_values.extend(solver_Y_values)

                    # Give this plot a title based on its parameter and instance size.
                    plot_title = 'Dependency of ' + measurement + ' from ' + \
                        parameter + ' in instances of size ' + str(instance_size)

                    # File, where plot image will be saved into. For 'Granularity' parameter,
                    # 'Weight balance' parameter is also taken into accound.
                    image_file_name = graphs_dir + '/parameter_' + parameter.replace(' ', '_').replace('/', '_')
                    
                    if parameter == 'Granularity':
                        image_file_name += '_weight_balance_' + parameter_weight_balance
                    
                    image_file_name += '_' + measurement.replace(' ', '_') + \
                        '_instance_size_' + str(instance_size) + '.png'

                    # For title, we should also add the weight balance parameter to the title,
                    # as this parameter comes coupled with granularity parameter.
                    if parameter == 'Granularity':
                        plot_title += ' (Weight balance = ' + \
                            df_parameter.loc[df_parameter['Weight balance'] == \
                                parameter_weight_balance]['Weight balance'].iloc[0] + ')'

                    plt.suptitle(plot_title)

                    # As solvers like 'Brute Force' and 'Branch Bound' are much slower
                    # than any other solver, we scale the Y-axis logarithmically.
                    if 'time' in measurement:
                        ax = plt.gca()
                        ax.set_yscale('log')
                        #ax.set_yticks(list_Y_values)
                        #ax.yaxis.set_major_formatter(plt.ScalarFormatter())
                        #ax.ticklabel_format(useOffset = False, style = 'plain', axis = 'y')

                    # For the granularity, as we have some values between [0, 1) and
                    # some values, reaching up to 50, we have to logarithmically scale
                    # the X-axis for this parameter.
                    if parameter == 'Granularity':
                        ax = plt.gca()
                        ax.set_xscale('log')
                        ax.set_xticks(list_X_values)
                        ax.xaxis.set_major_formatter(plt.ScalarFormatter())
                        ax.ticklabel_format(useOffset = False, style = 'plain', axis = 'x')

                    # Show legend.
                    plt.legend()

                    # Save the graph as an image.
                    plt.savefig(image_file_name)

                    plt.close()

                    # In case if the current parameter is no 'Granularity'
                    # there is no need to traverse through the values of 'Weight balance'.
                    if parameter != 'Granularity':
                        break

        # Plot the graph of changes of average error/time and maximum error/time in a given permutation.
        # For the given instance size, we will only take the first permutation, as it is enough to analyze
        # the difference between solvers in one permutation.
        if len(list_permutations) > 0:
            # Write the default parameters results for this
            # instance into the respective data frame.
            row_instance = list_permutations[0].iloc[0]

            if df_instance_size.empty:
                df_instance_size = pd.DataFrame([row_instance])
            else:
                df_instance_size = pd.concat([df_instance_size, \
                    pd.DataFrame([row_instance])], ignore_index = True \
                )

            df_permutation = list_permutations[0]

            # Construct the new data frame that has the same parameter values,
            # as the ones in the first row of this permutation.
            df_identical = pd.DataFrame()

            for index, row in df_permutation.iterrows():
                if df_identical.empty:
                    # In case if the data frame is empty,
                    # instantiate it with this row.
                    df_identical = pd.DataFrame([row])
                else:
                    flag_parameters_equal = True

                    for parameter in list_parameters:
                        if row[parameter] != df_identical[parameter].iloc[0]:
                            flag_parameters_equal = False
                            break

                    if flag_parameters_equal:
                        # As all parameters of this row are equal to the first row
                        # of this permutation's data frame, we add this row to our data frame.
                        df_identical = pd.concat([df_identical, \
                            pd.DataFrame([row])], ignore_index = True \
                        )

            # Generate graphs.
            for measurement in ['average error', 'maximum error', 'average time', 'maximum time']:
                dict_y_values = {}

                for solver_id, solver_name in enumerate(list_solver_names):
                    dict_y_values[solver_name] = df_identical[solver_name + ' (' + measurement + ')']

                generate_graph('Index of the permutation', measurement.title(), list_solver_names, \
                    df_identical.index, dict_y_values, measurement, 'Dependency of ' + measurement + \
                    ' from the given permutation ' + ' in instances of size ' + str(instance_size), \
                    graphs_dir + '/permutation_' + measurement.replace(' ', '_') + '_instance_size_' + \
                    str(instance_size) + '.png' \
                )

    # Generate graphs for `Instance size` parameter.
    for measurement in ['average error', 'maximum error', 'average time', 'maximum time']:
        dict_y_values = {}

        for solver_id, solver_name in enumerate(dict_solver_name_instance_size):
            # Write values of the given solver up until the instance that it was tested.
            list_y_values = []

            for index, row in df_instance_size.iterrows():
                if row['Instance size'] <= dict_solver_name_instance_size[solver_name]:
                    list_y_values.append(row[solver_name + ' (' + measurement + ')'])

            dict_y_values[solver_name] = list_y_values

        generate_graph('Instance size', measurement.title(), dict_solver_name_instance_size.keys(), \
            df_instance_size['Instance size'].values, dict_y_values, measurement, \
            'Dependency of ' + measurement + ' from the instance size', graphs_dir + \
            '/parameter_Instance_size_' + measurement.replace(' ', '_') + '.png' \
        )

# Call `main()` function if this script is called directly.
if __name__ == '__main__':
    main()

