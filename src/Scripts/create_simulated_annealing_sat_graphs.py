import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
import sys
from os.path import exists
import random as rand

def main():
    csv_file_path = sys.argv[1]
    num_instances = int(sys.argv[2])
    graphs_dir = sys.argv[3]
    final_temperature = float(sys.argv[4])
    cooling_alpha = float(sys.argv[5])
    heating_alpha = float(sys.argv[6])
    inner_loop_length = int(sys.argv[7])

    # Dictionary with default values of parameters.
    dict_parameters_default_values = {
        'Final temperature': final_temperature,
        'Cooling alpha': cooling_alpha,
        'Heating alpha': heating_alpha,
        'Inner loop length': inner_loop_length
    }

    if not exists(csv_file_path):
        print('File ' + csv_file_path + ' does not exist.')
        exit(1)

    # Parse csv file into dataframe.
    df = pd.read_csv(csv_file_path)

    # Get CSV headers and a list of parameters
    # that are first 5 headers in the list.
    list_csv_headers = list(df.columns.values)
    list_parameters = list_csv_headers[1:5]

    # Instance type.
    instance_type = df.iloc[0]['Instance type']

    # First, we have to find the average/maximum error/time for each configuration of parameters.
    #list_parameters_configuration = []

    # Create a new data frame with calculated average/maximum error/time.
    df_parameter = pd.DataFrame(columns = ['Final temperature', 'Cooling alpha', 'Heating alpha', \
        'Inner loop length', 'Average error', 'Maximum error', 'Average time', 'Maximum time'])

    for configuration_id in range(0, int(int(df.shape[0]) / num_instances), 1):
        starting_id = configuration_id * num_instances
        ending_id = starting_id + num_instances
        df_configuration = df.iloc[starting_id:ending_id,:]

        df_new_row = pd.DataFrame([[df_configuration.iloc[0]['Final temperature'], df_configuration.iloc[0]['Cooling alpha'], \
            df_configuration.iloc[0]['Heating alpha'], df_configuration.iloc[0]['Inner loop length'], \
            df_configuration['Simulated Annealing SAT (error)'].mean(), \
            df_configuration['Simulated Annealing SAT (error)'].max(), \
            df_configuration['Simulated Annealing SAT (time)'].mean(), \
            df_configuration['Simulated Annealing SAT (time)'].max()]], columns = ['Final temperature', 'Cooling alpha', 'Heating alpha', \
            'Inner loop length', 'Average error', 'Maximum error', 'Average time', 'Maximum time'] \
        )

        df_parameter = pd.concat([df_new_row, df_parameter])

        '''
        list_parameters_configuration.append({
            'Final temperature': df_configuration.iloc[0]['Final temperature'],
            'Cooling alpha': df_configuration.iloc[0]['Cooling alpha'],
            'Heating alpha': df_configuration.iloc[0]['Heating alpha'],
            'Inner loop length': df_configuration.iloc[0]['Inner loop length'],
            'Average error': df_configuration['Simulated Annealing SAT (error)'].mean(),
            'Maximum error': df_configuration['Simulated Annealing SAT (error)'].max(),
            'Average time': df_configuration['Simulated Annealing SAT (time)'].mean(),
            'Maximum time': df_configuration['Simulated Annealing SAT (time)'].max()
        })
        '''

    #print(df_parameter)
    #print(list_parameters_configuration[0])

    for parameter in list_parameters:
        # For each parameter, find its different values, when the other parameters have their
        # corresponding default values.
        # For this, we first find rows, where `parameter` has unique values and drop the rows,
        # where it has default value, as default value of each parameter is used to permute the other 
        # parameter, i.e. the other parameter won't have its default value.
        df_unique_parameter_values = df_parameter.drop_duplicates(parameter)
        df_unique_parameter_values = df_unique_parameter_values[df_unique_parameter_values[parameter] != \
            dict_parameters_default_values[parameter]]

        # Sort the data frame based on `parameter` value.
        df_unique_parameter_values = df_unique_parameter_values.sort_values(by = [parameter])

        for measurement in ['Average error', 'Maximum error', 'Average time', 'Maximum time']:
            # Give size to the figure.
            plt.figure(figsize = (10, 6))

            # Set labels to the axes.
            plt.xlabel(parameter)
            plt.ylabel(measurement.title())

            # Plot the graph.
            plt.plot(df_unique_parameter_values[parameter], df_unique_parameter_values[measurement], \
                label = 'Simulated Annealing SAT', marker = 'o' \
            )

            if 'time' in measurement:
                ax = plt.gca()
                ax.set_yscale('log')

            # Create a title for the graph.
            plot_title = 'Dependency of ' + measurement + ' from ' + parameter + \
                ' with ' + instance_type + ' instances'
            
            plt.suptitle(plot_title)

            # Show legend.
            plt.legend()

            # Save the graph as an image.
            image_file_name = graphs_dir + '/parameter_' + parameter.replace(' ', '_') + '_' + \
                measurement.replace(' ', '_') + '_' + instance_type + '.png'

            plt.savefig(image_file_name)

            plt.close()


# Call `main()` function if this script is called directly.
if __name__ == '__main__':
    main()
