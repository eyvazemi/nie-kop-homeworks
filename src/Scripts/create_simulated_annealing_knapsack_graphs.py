import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
import sys
from os.path import exists
import random as rand

def main():
    csv_file_path = sys.argv[1]
    graphs_dir = sys.argv[2]
    final_temperature = sys.argv[3]
    cooling_alpha = sys.argv[4]
    heating_alpha = sys.argv[5]
    inner_loop_to_items_ratio = sys.argv[6]

    if not exists(csv_file_path):
        print('File ' + csv_file_path + ' does not exist.')
        exit(1)

    # Parse csv file into dataframe.
    df = pd.read_csv(csv_file_path)

    # Get CSV headers and a list of parameters
    # that are first 5 headers in the list.
    list_csv_headers = list(df.columns.values)
    list_parameters = list_csv_headers[1:5]

    # Get the solver names.
    list_solver_names = []

    for csv_header in list_csv_headers[5::4]:
        # The name of the solver is followed by either '(averate error/time)'
        # or '(maximum error/type)', thus the name of the solver comes before
        # the opening paranthesis. There is also a space between the name of the
        # solver and an opening paranthesis, thus we have to remove the last space too.
        solver_name = csv_header.partition('(')[0].rstrip(' ')
        list_solver_names.append(solver_name)

    # Unique values of instance sizes.
    list_instance_size = df['Instance size'].unique()

    # We will split all graphs by instance sizes.
    for instance_size in list_instance_size:
        # Create a dictionary with default values.
        dict_parameters_default_values = {
            'Final temperature': final_temperature,
            'Cooling alpha': cooling_alpha,
            'Heating alpha': heating_alpha,
            'Inner loop length': float(inner_loop_to_items_ratio) * float(instance_size)
        }

        # Make a dictionary with NK, ZKC, and ZKW instances.
        dict_instance_types = {
            'NK': pd.DataFrame(columns = list_csv_headers),
            'ZKC': pd.DataFrame(columns = list_csv_headers),
            'ZKW': pd.DataFrame(columns = list_csv_headers)
        }

        # Get all rows with the given instance size.
        df_inst_size = df.loc[df['Instance size'] == instance_size]

        # Divide each combination of parameters into
        # NK, ZKC, and ZKW instances.
        for row_id in range(0, len(df_inst_size.index)):
            instance_type = ''

            if row_id % 3 == 0:
                instance_type = 'NK'
            elif row_id % 3 == 1:
                instance_type = 'ZKC'
            else:
                instance_type = 'ZKW'

            # Get this row.
            df_row = df_inst_size.iloc[row_id]

            if dict_instance_types[instance_type].empty:
                # As warning occurs, when we concatenate an empty data frame
                # with the row, we check if the data frame is empty, in which 
                # case we just instantiate it with this row.
                dict_instance_types[instance_type] = pd.DataFrame([df_row])
            else:
                dict_instance_types[instance_type] = pd.concat([dict_instance_types[instance_type], \
                    pd.DataFrame([df_row])], ignore_index = True \
                )

        # todo:_ remove this
        #for instance_type in dict_instance_types.keys():
        #    print(instance_type + ' : ' + str(dict_instance_types[instance_type].iloc[:, 0:5]))

        # Create a data frame for each parameter, the values of which are variated
        # in that data frame, so that all other parameters have default values.
        for instance_type, df_instance_type in dict_instance_types.items():
            for measurement in ['average error', 'maximum error', 'average time', 'maximum time']:
                for parameter in dict_parameters_default_values.keys():
                    df_parameter = pd.DataFrame(columns = list_csv_headers)

                    for row_id, row in df_instance_type.iterrows():
                        # All parameters except for the one we are looking 
                        # for right now, should have a default value.
                        flag_accept_this_row = True

                        for another_parameter in dict_parameters_default_values.keys():
                            if another_parameter == parameter:
                                continue

                            if float(row[another_parameter]) != float(dict_parameters_default_values[another_parameter]):
                                flag_accept_this_row = False
                                break

                        if flag_accept_this_row:
                            if df_parameter.empty:
                                df_parameter = pd.DataFrame([row])
                            else:
                                df_parameter = pd.concat([df_parameter, \
                                    pd.DataFrame([row])], ignore_index = True \
                                )

                    # Sort the rows by the given parameter value in case if this column is of numerical type.
                    df_parameter = df_parameter.sort_values(by = [parameter])

                    #print('Instance size: ' + str(instance_size) + ', instance type: ' + instance_type + \
                    #    ', parameter: ' + parameter + ', ' + str(df_parameter.iloc[:, 0:5]) \
                    #)

                    # Give size to the figure.
                    plt.figure(figsize = (10, 6))

                    # Set labels to the axes.
                    plt.xlabel(parameter)
                    plt.ylabel(measurement.title())

                    # Markers that will be used for each solver in the plot.
                    # These markers are static, thus we will overuse them, 
                    # in case if there are more solvers, by finding a modulus
                    # of the solver index with the size of this list of markers.
                    list_plot_markers = ['o', '^', 's', 'x', 'D', '+']

                    for solver_id, solver_name in enumerate(list_solver_names):
                        # In order to be able to differentiate intersecting plots,
                        # each plot is given an `alpha` value for its color, in such a way
                        # that the first plot has `alpha = 1`, and every next graph, 
                        # is plotted with lower alpha value than the previous one, so that
                        # in case if it overlaps with one of the previous plots, that previous
                        # plot will be visible, as it will have higher alpha value than the 
                        # currently drawn one.
                        plt.plot(df_parameter[parameter], df_parameter[solver_name + ' (' + measurement + ')'], \
                            label = solver_name, marker = list_plot_markers[solver_id % len(list_plot_markers)], \
                            markerfacecolor = 'none', alpha = 1 - solver_id * 0.15  \
                        )

                    # Simulated Annealing solver is much slower than Dynamic Programming by Weight solver,
                    # thus we have to logarithmically scale the `time` axis.
                    if 'time' in measurement:
                        ax = plt.gca()
                        ax.set_yscale('log')

                    # Create a title for the graph.
                    plot_title = 'Dependency of ' + measurement + ' from ' + parameter + \
                        ' with ' + instance_type + ' ' + str(instance_size) + ' instances'
                    
                    plt.suptitle(plot_title)

                    # Show legend.
                    plt.legend()

                    # Save the graph as an image.
                    image_file_name = graphs_dir + '/parameter_' + parameter.replace(' ', '_') + '_' + \
                        measurement.replace(' ', '_') + '_' + instance_type + '_' + str(instance_size) + '.png'

                    plt.savefig(image_file_name)

                    plt.close()


# Call `main()` function if this script is called directly.
if __name__ == '__main__':
    main()
