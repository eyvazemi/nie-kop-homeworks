import pandas as pd
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import seaborn as sns
import sys

def generate_graph(in_file_name, out_file_name, instance_name, values_type):
    # x and y values for the graph
    x_values = []
    y_values = {}

    # read csv files into data frames
    df = pd.read_csv(in_file_name)
    
    # names of the csv file headers
    csv_header = list(df.columns.values)

    for solver_name in csv_header[1:]:
        y_values[solver_name] = []
        
    # process the rows one by one
    for i, row in df.iterrows():
        x_values.append(row[0])
        for (solver_name, solver_val) in zip(csv_header[1:], row[1:]):
            y_values[solver_name].append(solver_val)

    # give size to the figure
    plt.figure(figsize = (8, 5))

    # set labels to the axes
    plt.xlabel('Instance size (' + instance_name + ' instances)')
    plt.ylabel('Time of execution(' + values_type + ' values)')

    # instantiate graph
    for header in y_values.keys():
        plt.plot(x_values, y_values[header], label = header, marker = 'o')

    # show text on plot points
    #for i, inst_size in enumerate(x_values):
        #if i == len(x_values) - 1:
            #for header in y_values.keys():
            #    plt.text(inst_size, y_values[header][i] + 50, str(y_values[header][i]))

    # show legend
    plt.legend()

    # turn scientific notation off
    ax = plt.gca()
    ax.ticklabel_format(useOffset = False, style = 'plain', axis = 'x')
    ax.ticklabel_format(useOffset = False, style = 'plain', axis = 'y')

    # save the graph as an image
    plt.savefig(out_file_name)

generate_graph(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
