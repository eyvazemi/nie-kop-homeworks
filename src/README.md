# Emil Eyvazov - Homework 1

## The problem statement
The problem consisted of solving a knapsack problem.

## Used solutions
For solving a knapsack problem, brute force and branch and bound method were used.

### Brute Force
Consists of simple DFS recursion with finding of the most optimal solution: the highest cost with the least total weight of items in the knapsack.
If any two possible solutions (with the highest possible costs), are of the same cost, but of different total weight, the one with the least total weight is chosen.

### Branch and Bound
Consists of DFS with pruning regarding cost: if the sum of an upped bound of cost of observed branch and the current cost is less than the current highest cost, the branch is neglected. For that, upper bounds of each item is calculated, before starting DFS with pruning.
This solution if more efficient than the Brute Force solution, as we totally neglect the branches that have total cost less than highest cost of the knapsack.

#### Choosing an upper bound:
Upper bound of cost for each item is calculated via summing up the cost of this item and the costs of all items coming after this item.

### Complexity
Complexity is measured via number of innermost iterations and the number of stack calls for DFS recursions.

### Time of execution
Time of execution is measured in microseconds.

## Samples

### NR and ZR
__NR__ instances consisted of items with the one possible solution.
__ZR__ instances consisted of items with different possible combinations that will bring to the most optimal knapsack. It is possible to have different combinations of items that will bring to the same cost and the same total weight and all of them are eligible solutions. 

### The graphs
Graphs are located in the _Graphs_ directory in the zipped file with the source code, it is one of the direct subdirectories of the root directory with the source code, so, it is easy to find it. The graphs are of 2 types: graphs with complexity plotted on them and graphs with time of executoins plotted on them.
For both: complexity and time of execution, average and maximum values are calculated for each solver.
Graphs for NR and ZR instances are located in different file, so that one plot is not oversaturated with graphs and it could be possible to read them.
The X-axis of the plots, consist of instance sizes ot either NR or ZR instances and Y-axis consists of Complexity of each solver or Time of Execution of each solver, depending on the file.
The files with Comlexity are _Complexity\_*_, whereas, the files with Time of Execution are _Time\_*_.

## Result
It is possible to observe in the files with graphs that Branch and Bound method is more efficient than Brute Force method and that is achieved via pruning based on cost on every DFS iteration.

# Acronyms
DFS - Depth First Search.