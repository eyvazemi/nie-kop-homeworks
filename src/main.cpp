#include "Common/FileParsers/file_parser.h"
#include "Common/FileParsers/file_csv_parser.h"
#include "Common/FileParsers/file_sat_parser.h"
#include "Tests/test_file_parser.cpp"
#include "Tests/test_solver.cpp"
#include <iostream>
#include <vector>
#include <string>
#include <fstream>

std::string str_set_precision(double, int = 3);
int str_find_first_digit(const std::string &);

bool tests_caller_decision(const std::string &, const std::string &,
    const std::string &, const std::string &,
    const std::string &, int, int
);

bool tests_caller_optimisation(const std::string &, const std::string &,
    const std::string &, const std::string &,
    const std::string &, int, int
);

bool tests_caller_fptas(const std::string &, const std::string &,
    const std::string &, const std::string &,
    const std::string &, std::vector<float> &, int, int
);

bool tests_caller_experimental(const std::string & csv_dir, const std::string & samples_dir,
    const std::string & inst_size, const std::string & file_name_inst,
    int range_start, int range_end, const std::string & str_weight_balance,
    const std::string & str_cost_weight_correlation, float maximum_cost,
    float maximum_weight, float capacity_total_weight_ratio, float granularity
);

bool tests_caller_simulated_annealing_knapsack(const std::string & csv_dir,
    const std::string & samples_dir, const std::string & inst_size,
    const std::string & inst_type, const std::string & file_name_inst,
    const std::string & file_name_sol, int range_start, int range_end,
    float final_temperature, float cooling_alpha, float heating_alpha,
    float inner_loop_to_items_ratio
);

bool tests_caller_simulated_annealing_sat(const std::string & csv_dir,
    const std::string & samples_dir, const std::string & inst_type, 
    const std::string & file_name_inst, const std::string & file_name_sol, 
    float final_temperature, float cooling_alpha, float heating_alpha, 
    float inner_loop_to_items_ratio
);

int main(int argc, char ** argv) {
    int args_counter = 0;

    // check if all arguments are present
    if(argc < 10) {
        std::cout << "Incorrect number of arguments." << std::endl;
        return 1;
    }

    // type of the problem: Decision, Optimisation, FPTAS, or Experimental
    std::string str_problem_type = std::string(argv[++args_counter]);

    // directory with csv files
    std::string csv_dir = std::string(argv[++args_counter]);

    // sample files' directory
    std::string samples_dir = std::string(argv[++args_counter]);

    // size of the instance file
    std::string inst_size;

    // names of the files that must be parsed
    std::string file_name_inst;
    std::string file_name_sol;

    // starting and ending ids of lines that must be parsed
    // in the file with knapsacks
    int range_start;
    int range_end;

    // get the number of FPTAS bins
    std::vector<float> fptas_max_errors;

    // parameters of instances generator for "Experimental" problem
    std::string str_weight_balance;
    std::string str_cost_weight_correlation;
    float maximum_cost;
    float maximum_weight;
    float capacity_total_weight_ratio;
    float granularity;

    // parameters for Simulated Annealing for Knapsack problem
    std::string inst_type; // NK, ZKC, or ZKW instances.
    float final_temperature;
    float cooling_alpha;
    float heating_alpha;
    float inner_loop_to_items_ratio;

    // Parameter for Simulated Annealing for SAT problem.
    std::string solutions_dir;

    if(str_problem_type == "Experimental") {
        file_name_inst = std::string(argv[++args_counter]);
        inst_size = std::string(argv[++args_counter]);
        maximum_cost = std::stof(std::string(argv[++args_counter]));
        maximum_weight = std::stof(std::string(argv[++args_counter]));
        capacity_total_weight_ratio = std::stof(std::string(argv[++args_counter]));
        str_cost_weight_correlation = std::string(argv[++args_counter]);
        str_weight_balance = std::string(argv[++args_counter]);
        granularity = std::stof(std::string(argv[++args_counter]));
    } else {
        if(str_problem_type != "Simulated_Annealing_SAT") {
            // Size of the instance file.
            inst_size = std::string(argv[++args_counter]);
        }

        // Names of the files that must be parsed.
        file_name_inst = std::string(argv[++args_counter]);
        file_name_sol = std::string(argv[++args_counter]);
    }

    // starting and ending ids of lines that must be parsed
    // in the file with knapsacks
    if(str_problem_type != "Simulated_Annealing_SAT") {
        range_start = std::stoi(argv[++args_counter]);
        range_end = std::stoi(argv[++args_counter]);

        if(range_start > range_end) {
            std::cout << "Main: start of the range must be smaller or equal to the end of the range" << std::endl;
            return 1;
        }
    }

    if(str_problem_type == "FPTAS" || str_problem_type == "Experimental") {
        for(int i = ++args_counter; i < argc; ++i)
            fptas_max_errors.push_back(std::stof(argv[i]));
    } else if(str_problem_type == "Simulated_Annealing_Knapsack" ||
        str_problem_type == "Simulated_Annealing_SAT"
    ) {
        inst_type = argv[++args_counter];
        final_temperature = std::stof(argv[++args_counter]);
        cooling_alpha = std::stof(argv[++args_counter]);
        heating_alpha = std::stof(argv[++args_counter]);
        inner_loop_to_items_ratio = std::stof(argv[++args_counter]);
    }

    // run all required unit tests
    std::cout << "Instance file: " << file_name_inst << "\nInstance size: " << inst_size << std::endl;

    if(str_problem_type == "Decision") {
        if(!tests_caller_decision(csv_dir, samples_dir, inst_size,
            file_name_inst, file_name_sol, range_start, range_end)
        ) {
            std::cout << "\tSome error in the unit tests" << std::endl;
            return 1;
        } else
            std::cout << "\tAll unit tests ran successfully" << std::endl;
    } else if(str_problem_type == "Optimisation") {
        if(!tests_caller_optimisation(csv_dir, samples_dir, inst_size,
            file_name_inst, file_name_sol, range_start, range_end)
        ) {
            std::cout << "\tSome error in the unit tests" << std::endl;
            return 1;
        } else
            std::cout << "\tAll unit tests ran successfully" << std::endl;
    } else if(str_problem_type == "FPTAS") {
        if(!tests_caller_fptas(csv_dir, samples_dir, inst_size,
            file_name_inst, file_name_sol, fptas_max_errors, range_start, range_end)
        ) {
            std::cout << "\tSome error in the unit tests" << std::endl;
            return 1;
        } else
            std::cout << "\tAll unit tests ran successfully" << std::endl;
    } else if(str_problem_type == "Experimental") {
        if(!tests_caller_experimental(csv_dir, samples_dir, inst_size, file_name_inst,
            range_start, range_end, str_weight_balance, str_cost_weight_correlation,
            maximum_cost, maximum_weight, capacity_total_weight_ratio, granularity)
        ) {
            std::cout << "\tSome error in the unit tests" << std::endl;
            return 1;
        } else
            std::cout << "\tAll unit tests ran successfully" << std::endl;
    } else if(str_problem_type == "Simulated_Annealing_Knapsack") {
        std::cout << "\tInstance type: " << inst_type <<
            "\n\tFinal temperature: " << final_temperature <<
            "\n\tCooling alpha: " << cooling_alpha <<
            "\n\tHeating alpha: " << heating_alpha <<
            "\n\tInner loop to items ratio: " << inner_loop_to_items_ratio << std::endl;

        if(!tests_caller_simulated_annealing_knapsack(csv_dir, samples_dir, inst_size,
            inst_type, file_name_inst, file_name_sol, range_start, range_end,
            final_temperature, cooling_alpha, heating_alpha, inner_loop_to_items_ratio)
        ) {
            std::cout << "\tSome error in the unit tests" << std::endl;
            return 1;
        } else
            std::cout << "\tAll unit tests ran successfully" << std::endl;
    } else if(str_problem_type == "Simulated_Annealing_SAT") {
        std::cout << "\tInstance type: " << inst_type <<
            "\n\tFinal temperature: " << final_temperature <<
            "\n\tCooling alpha: " << cooling_alpha <<
            "\n\tHeating alpha: " << heating_alpha <<
            "\n\tInner loop to items ratio: " << inner_loop_to_items_ratio << std::endl;

        if(!tests_caller_simulated_annealing_sat(csv_dir, samples_dir, inst_type, 
            file_name_inst, file_name_sol, final_temperature, cooling_alpha, 
            heating_alpha, inner_loop_to_items_ratio)
        ) {
            std::cout << "\tSome error in the unit tests" << std::endl;
            return 1;
        } else {
            std::cout << "\tAll unit tests ran successfully" << std::endl;
        }
    } else {
        std::cout << "\tUnknown problem type." << std::endl;
        return 1;
    }

    // Print splitting characters.
    for(int i = 0; i < 50; ++i) {
        std::cout << "=";
    }

    std::cout << std::endl;

    return 0;
}

bool tests_caller_decision(const std::string & csv_dir, const std::string & samples_dir,
    const std::string & inst_size, const std::string & file_name_inst,
    const std::string & file_name_sol, int range_start, int range_end
) {
    EnumProblemType enum_problem_type = EnumProblemType::Decision;
    TestSolver test_solver_brute_force, test_solver_branch_bound;
    bool test_results = //TestFileParser::run() &&
        test_solver_brute_force.run(
            samples_dir + file_name_inst, samples_dir + file_name_sol,
            EnumSolvers::BruteForce, enum_problem_type, -1, range_start, range_end
        ) &&
        test_solver_branch_bound.run(
            samples_dir + file_name_inst, samples_dir + file_name_sol,
            EnumSolvers::BranchBound, enum_problem_type, -1, range_start, range_end
        );

    if(test_results) {
        // name of the csv files
        std::string csv_file_name = csv_dir + "/" + file_name_inst.substr(0, str_find_first_digit(file_name_inst));
        std::string csv_file_name_average_complexity = csv_file_name + "_average_complexity";
        std::string csv_file_name_maximum_complexity = csv_file_name + "_maximum_complexity";
        std::string csv_file_name_average_time = csv_file_name + "_average_time";
        std::string csv_file_name_maximum_time = csv_file_name + "_maximum_time";
        std::string csv_file_name_complexity_frequencies;
        std::string csv_file_name_complexity_frequencies_brute_force;
        std::string csv_file_name_complexity_frequencies_branch_bound;
        int file_name_underscore_pos = file_name_inst.find("_");

        csv_file_name_complexity_frequencies = csv_dir + "/" +
            file_name_inst.substr(0, file_name_underscore_pos) + "_complexity_frequencies";
        csv_file_name_complexity_frequencies_brute_force = csv_file_name_complexity_frequencies + "_brute_force";
        csv_file_name_complexity_frequencies_branch_bound = csv_file_name_complexity_frequencies + "_branch_bound";

        // csv files header
        std::vector<std::string> vec_csv_header;
        vec_csv_header.push_back("Instance size");
        vec_csv_header.push_back("Brute Force Solver");
        vec_csv_header.push_back("Branch and Bound Solver");

        // write into average comlexity csv file
        std::vector<std::string> vec_values_average_complexity;
        vec_values_average_complexity.push_back(inst_size);
        vec_values_average_complexity.push_back(
            std::to_string(test_solver_brute_force.get_average_complexity())
        );
        vec_values_average_complexity.push_back(
            std::to_string(test_solver_branch_bound.get_average_complexity())
        );

        FileCSVParser::write_line(csv_file_name_average_complexity, vec_csv_header, vec_values_average_complexity);

        // write into maximum comlexity csv file
        std::vector<std::string> vec_values_maximum_complexity;
        vec_values_maximum_complexity.push_back(inst_size);
        vec_values_maximum_complexity.push_back(
            std::to_string(test_solver_brute_force.get_maximum_complexity())
        );
        vec_values_maximum_complexity.push_back(
            std::to_string(test_solver_branch_bound.get_maximum_complexity())
        );

        FileCSVParser::write_line(csv_file_name_maximum_complexity, vec_csv_header, vec_values_maximum_complexity);

        // write into average time of execution csv file
        std::vector<std::string> vec_values_average_time;
        vec_values_average_time.push_back(inst_size);
        vec_values_average_time.push_back(
            std::to_string(test_solver_brute_force.get_average_time())
        );
        vec_values_average_time.push_back(
            std::to_string(test_solver_branch_bound.get_average_time())
        );

        FileCSVParser::write_line(csv_file_name_average_time, vec_csv_header, vec_values_average_time);

        // write into maximum time of execution csv file
        std::vector<std::string> vec_values_maximum_time;
        vec_values_maximum_time.push_back(inst_size);
        vec_values_maximum_time.push_back(
            std::to_string(test_solver_brute_force.get_maximum_time())
        );
        vec_values_maximum_time.push_back(
            std::to_string(test_solver_branch_bound.get_maximum_time())
        );

        FileCSVParser::write_line(csv_file_name_maximum_time, vec_csv_header, vec_values_maximum_time);

        // complexity frequencies csv file header
        std::vector<std::string> vec_csv_complexity_frequencies_header;
        vec_csv_complexity_frequencies_header.push_back("Complexity");
        vec_csv_complexity_frequencies_header.push_back("Frequency");

        // write into complexity frequencies csv file
        std::vector<std::vector<std::string>> vec_values_complexity_frequencies_brute_force;
        std::vector<std::vector<std::string>> vec_values_complexity_frequencies_branch_bound;
        
        for(const auto & complexity : *(test_solver_brute_force.get_vec_complexities())) {
            vec_values_complexity_frequencies_brute_force.push_back(std::vector<std::string>{
                std::to_string(complexity.first), std::to_string(complexity.second)
            });
        }

        for(const auto & complexity : *(test_solver_branch_bound.get_vec_complexities())) {
            vec_values_complexity_frequencies_branch_bound.push_back(std::vector<std::string>{
                std::to_string(complexity.first), std::to_string(complexity.second)
            });
        }

        FileCSVParser::write_full(csv_file_name_complexity_frequencies_brute_force,
            vec_csv_complexity_frequencies_header, vec_values_complexity_frequencies_brute_force
        );

        FileCSVParser::write_full(csv_file_name_complexity_frequencies_branch_bound,
            vec_csv_complexity_frequencies_header, vec_values_complexity_frequencies_branch_bound
        );
    }

    return test_results;
}

bool tests_caller_optimisation(const std::string & csv_dir, const std::string & samples_dir,
    const std::string & inst_size, const std::string & file_name_inst,
    const std::string & file_name_sol, int range_start, int range_end
) {
    EnumProblemType enum_problem_type = EnumProblemType::Optimisation;
    TestSolver test_solver_branch_bound, test_solver_dynamic_programming_weight;
    TestSolver test_solver_dynamic_programming_cost;

    bool test_results = //TestFileParser::run() &&
        test_solver_branch_bound.run(
            samples_dir + file_name_inst, samples_dir + file_name_sol,
            EnumSolvers::BranchBound, enum_problem_type, -1, range_start, range_end
        ) &&
        test_solver_dynamic_programming_weight.run(
            samples_dir + file_name_inst, samples_dir + file_name_sol,
            EnumSolvers::DynamicProgrammingWeight, enum_problem_type, -1, range_start, range_end
        ) &&
        test_solver_dynamic_programming_cost.run(
            samples_dir + file_name_inst, samples_dir + file_name_sol,
            EnumSolvers::DynamicProgrammingCost, enum_problem_type, -1, range_start, range_end
        );

    if(test_results) {
        // name of the csv files
        std::string csv_file_name = csv_dir + "/" + file_name_inst.substr(0, str_find_first_digit(file_name_inst));
        std::string csv_file_name_average_time = csv_file_name + "_average_time_optimisation";
        std::string csv_file_name_maximum_time = csv_file_name + "_maximum_time_optimisation";

        // csv files header
        std::vector<std::string> vec_csv_header;
        vec_csv_header.push_back("Instance size");
        vec_csv_header.push_back("Branch and Bound Solver");
        vec_csv_header.push_back("Dynamic Programming(by weight)");
        vec_csv_header.push_back("Dynamic Programming(by cost)");

        // write into average time of execution csv file
        std::vector<std::string> vec_values_average_time;
        vec_values_average_time.push_back(inst_size);
        vec_values_average_time.push_back(
            std::to_string(test_solver_branch_bound.get_average_time())
        );
        vec_values_average_time.push_back(
            std::to_string(test_solver_dynamic_programming_weight.get_average_time())
        );
        vec_values_average_time.push_back(
            std::to_string(test_solver_dynamic_programming_cost.get_average_time())
        );

        FileCSVParser::write_line(csv_file_name_average_time, vec_csv_header, vec_values_average_time);

        // write into maximum time of execution csv file
        std::vector<std::string> vec_values_maximum_time;
        vec_values_maximum_time.push_back(inst_size);
        vec_values_maximum_time.push_back(
            std::to_string(test_solver_branch_bound.get_maximum_time())
        );
        vec_values_maximum_time.push_back(
            std::to_string(test_solver_dynamic_programming_weight.get_maximum_time())
        );
        vec_values_maximum_time.push_back(
            std::to_string(test_solver_dynamic_programming_cost.get_maximum_time())
        );

        FileCSVParser::write_line(csv_file_name_maximum_time, vec_csv_header, vec_values_maximum_time);
    }

    return test_results;
}

bool tests_caller_fptas(const std::string & csv_dir, const std::string & samples_dir,
    const std::string & inst_size, const std::string & file_name_inst,
    const std::string & file_name_sol, std::vector<float> & fptas_max_errors,
    int range_start, int range_end
) {
    EnumProblemType enum_problem_type = EnumProblemType::FPTAS;
    TestSolver test_solver_heuristics, test_solver_extended_heuristics;
    std::vector<TestSolver> vec_test_solver_dynamic_programming_cost_fptas;

    bool test_results = //TestFileParser::run() &&
        test_solver_heuristics.run(
            samples_dir + file_name_inst, samples_dir + file_name_sol,
            EnumSolvers::Heuristics, enum_problem_type, -1, range_start, range_end
        );
        test_solver_extended_heuristics.run(
            samples_dir + file_name_inst, samples_dir + file_name_sol,
            EnumSolvers::ExtendedHeuristics, enum_problem_type, -1, range_start, range_end
        );

        for(const float max_error : fptas_max_errors) {
            TestSolver test_solver_dynamic_programming_cost_fptas;
            test_solver_dynamic_programming_cost_fptas.run(
                samples_dir + file_name_inst, samples_dir + file_name_sol,
                EnumSolvers::DynamicProgrammingCostFPTAS, enum_problem_type,
                max_error, range_start, range_end
            );
            vec_test_solver_dynamic_programming_cost_fptas.push_back(
                test_solver_dynamic_programming_cost_fptas
            );
        }

    if(test_results) {
        // name of the csv files
        std::string csv_file_name = csv_dir + "/" + file_name_inst.substr(0, str_find_first_digit(file_name_inst));
        std::string csv_file_name_average_error = csv_file_name + "_average_error";
        std::string csv_file_name_maximum_error = csv_file_name + "_maximum_error";
        std::string csv_file_name_average_performance_guarantee = csv_file_name + "_average_performance_guarantee";
        std::string csv_file_name_maximum_performance_guarantee = csv_file_name + "_maximum_performance_guarantee";
        std::string csv_file_name_average_time = csv_file_name + "_average_time_fptas";
        std::string csv_file_name_maximum_time = csv_file_name + "_maximum_time_fptas";

        // csv files header
        std::vector<std::string> vec_csv_header;
        vec_csv_header.push_back("Instance size");
        vec_csv_header.push_back("Heuristics");
        vec_csv_header.push_back("Extended Heuristics");

        for(int i = 0; i < fptas_max_errors.size(); ++i) {
            vec_csv_header.push_back("FPTAS(error = " + str_set_precision(fptas_max_errors[i], 2) + ")");
        }

        // write into average error csv file
        std::vector<std::string> vec_values_average_error;
        vec_values_average_error.push_back(inst_size);
        vec_values_average_error.push_back(
            str_set_precision(test_solver_heuristics.get_average_error())
        );
        vec_values_average_error.push_back(
            str_set_precision(test_solver_extended_heuristics.get_average_error())
        );

        for(int i = 0; i < fptas_max_errors.size(); ++i) {
            vec_values_average_error.push_back(
                str_set_precision(vec_test_solver_dynamic_programming_cost_fptas[i].get_average_error())
            );
        }

        FileCSVParser::write_line(csv_file_name_average_error, vec_csv_header, vec_values_average_error);

        // write into maximum error csv file
        std::vector<std::string> vec_values_maximum_error;
        vec_values_maximum_error.push_back(inst_size);
        vec_values_maximum_error.push_back(
            str_set_precision(test_solver_heuristics.get_maximum_error())
        );
        vec_values_maximum_error.push_back(
            str_set_precision(test_solver_extended_heuristics.get_maximum_error())
        );

        // for FPTAS solver, maximum error is already given, thus there is no need to get it from the tester
        for(const float max_error : fptas_max_errors) {
            vec_values_maximum_error.push_back(
                str_set_precision(max_error)
            );
        }

        FileCSVParser::write_line(csv_file_name_maximum_error, vec_csv_header, vec_values_maximum_error);

        // write into average performance guarantee csv file
        std::vector<std::string> vec_values_average_performance_guarantee;
        vec_values_average_performance_guarantee.push_back(inst_size);
        vec_values_average_performance_guarantee.push_back(
            str_set_precision(test_solver_heuristics.get_average_performance_guarantee())
        );
        vec_values_average_performance_guarantee.push_back(
            str_set_precision(test_solver_extended_heuristics.get_average_performance_guarantee())
        );

        for(int i = 0; i < fptas_max_errors.size(); ++i) {
            vec_values_average_performance_guarantee.push_back(
                str_set_precision(vec_test_solver_dynamic_programming_cost_fptas[i].get_average_performance_guarantee())
            );
        }

        FileCSVParser::write_line(csv_file_name_average_performance_guarantee,
            vec_csv_header, vec_values_average_performance_guarantee
        );

        // write into maximum performance guarantee csv file
        std::vector<std::string> vec_values_maximum_performance_guarantee;
        vec_values_maximum_performance_guarantee.push_back(inst_size);
        vec_values_maximum_performance_guarantee.push_back(
            str_set_precision(test_solver_heuristics.get_maximum_performance_guarantee())
        );
        vec_values_maximum_performance_guarantee.push_back(
            str_set_precision(test_solver_extended_heuristics.get_maximum_performance_guarantee())
        );

        for(int i = 0; i < fptas_max_errors.size(); ++i) {
            vec_values_maximum_performance_guarantee.push_back(
                str_set_precision(vec_test_solver_dynamic_programming_cost_fptas[i].get_maximum_performance_guarantee())
            );
        }

        FileCSVParser::write_line(csv_file_name_maximum_performance_guarantee,
            vec_csv_header, vec_values_maximum_performance_guarantee
        );

        // write into average time of execution csv file
        std::vector<std::string> vec_values_average_time;
        vec_values_average_time.push_back(inst_size);
        vec_values_average_time.push_back(
            std::to_string(test_solver_heuristics.get_average_time())
        );
        vec_values_average_time.push_back(
            std::to_string(test_solver_extended_heuristics.get_average_time())
        );

        for(int i = 0; i < fptas_max_errors.size(); ++i) {
            vec_values_average_time.push_back(
                std::to_string(vec_test_solver_dynamic_programming_cost_fptas[i].get_average_time())
            );
        }

        FileCSVParser::write_line(csv_file_name_average_time, vec_csv_header, vec_values_average_time);

        // write into maximum time of execution csv file
        std::vector<std::string> vec_values_maximum_time;
        vec_values_maximum_time.push_back(inst_size);
        vec_values_maximum_time.push_back(
            std::to_string(test_solver_heuristics.get_maximum_time())
        );
        vec_values_maximum_time.push_back(
            std::to_string(test_solver_extended_heuristics.get_maximum_time())
        );

        for(int i = 0; i < fptas_max_errors.size(); ++i) {
            vec_values_maximum_time.push_back(
                std::to_string(vec_test_solver_dynamic_programming_cost_fptas[i].get_maximum_time())
            );
        }

        FileCSVParser::write_line(csv_file_name_maximum_time, vec_csv_header, vec_values_maximum_time);
    }

    return test_results;
}

bool tests_caller_experimental(const std::string & csv_dir, const std::string & samples_dir,
    const std::string & inst_size, const std::string & file_name_inst,
    int range_start, int range_end, const std::string & str_weight_balance,
    const std::string & str_cost_weight_correlation, float maximum_cost,
    float maximum_weight, float capacity_total_weight_ratio, float granularity
) {
    // First, we will solve the dynamic programming with decomposition by weight,
    // so that we could obtain the solution file, which will be used for all the other solvers.
    const std::string file_name_solution = TestSolver::create_solution_file(file_name_inst,
        samples_dir, EnumSolvers::DynamicProgrammingWeight, range_start, range_end
    );

    std::cout << "Solution file " << file_name_solution << " was generated." << std::endl;

    // Run all the solvers.
    EnumProblemType enum_problem_type = EnumProblemType::Experimental;
    TestSolver test_solver_brute_force;
    TestSolver test_solver_branch_bound;
    TestSolver test_solver_dynamic_programming_weight;
    TestSolver test_solver_dynamic_programming_cost;
    TestSolver test_solver_heuristics;
    TestSolver test_solver_extended_heuristics;

    bool test_results =
        test_solver_dynamic_programming_weight.run(
            samples_dir + "/" + file_name_inst, file_name_solution,
            EnumSolvers::DynamicProgrammingWeight, enum_problem_type, -1, range_start, range_end
        ) &&
        test_solver_dynamic_programming_cost.run(
            samples_dir + "/" + file_name_inst, file_name_solution,
            EnumSolvers::DynamicProgrammingCost, enum_problem_type, -1, range_start, range_end
        ) &&
        test_solver_heuristics.run(
            samples_dir + "/" + file_name_inst, file_name_solution,
            EnumSolvers::Heuristics, enum_problem_type, -1, range_start, range_end
        );
        test_solver_extended_heuristics.run(
            samples_dir + "/" + file_name_inst, file_name_solution,
            EnumSolvers::ExtendedHeuristics, enum_problem_type, -1, range_start, range_end
        );

    // Brute force and branch and bound solvers are extremely slow 
    // with instances larger than 20.
    const bool run_brute_force_solver = std::stoi(inst_size) <= 15;
    const bool run_branch_bound_solver = std::stoi(inst_size) <= 20;
    
    if(run_brute_force_solver) {
        test_results = test_results && test_solver_brute_force.run(
            samples_dir + "/" + file_name_inst, file_name_solution,
            EnumSolvers::BruteForce, enum_problem_type, -1, range_start, range_end
        );
    }

    if(run_branch_bound_solver) {
        test_results = test_results && test_solver_branch_bound.run(
            samples_dir + "/" + file_name_inst, file_name_solution,
            EnumSolvers::BranchBound, enum_problem_type, -1, range_start, range_end
        );
    }

    if(test_results) {
        // CSV files header.
        std::vector<std::string> vec_csv_header = {
            "Instance size",
            "Maximum cost",
            "Maximum weight",
            "Capacity to total weight ratio",
            "Cost/weight correlation",
            "Weight balance",
            "Granularity",
            "Brute Force (average error)",
            "Brute Force (maximum error)",
            "Brute Force (average time)",
            "Brute Force (maximum time)",
            "Branch Bound (average error)",
            "Branch Bound (maximum error)",
            "Branch Bound (average time)",
            "Branch Bound (maximum time)",
            "Dynamic Programming by Weight (average error)",
            "Dynamic Programming by Weight (maximum error)",
            "Dynamic Programming by Weight (average time)",
            "Dynamic Programming by Weight (maximum time)",
            "Dynamic Programming by Cost (average error)",
            "Dynamic Programming by Cost (maximum error)",
            "Dynamic Programming by Cost (average time)",
            "Dynamic Programming by Cost (maximum time)",
            "Simple Heuristics (average error)",
            "Simple Heuristics (maximum error)",
            "Simple Heuristics (average time)",
            "Simple Heuristics (maximum time)",
            "Extended Heuristics (average error)",
            "Extended Heuristics (maximum error)",
            "Extended Heuristics (average time)",
            "Extended Heuristics (maximum time)"
        };

        // Fill out csv data.
        std::vector<std::string> vec_csv_data;
        vec_csv_data.push_back(inst_size);
        vec_csv_data.push_back(std::to_string(maximum_cost));
        vec_csv_data.push_back(std::to_string(maximum_weight));
        vec_csv_data.push_back(std::to_string(capacity_total_weight_ratio));
        vec_csv_data.push_back(str_cost_weight_correlation);
        vec_csv_data.push_back(str_weight_balance);
        vec_csv_data.push_back(std::to_string(granularity));

        if(run_brute_force_solver) {
            vec_csv_data.push_back(std::to_string(test_solver_brute_force.get_average_error()));
            vec_csv_data.push_back(std::to_string(test_solver_brute_force.get_maximum_error()));
            vec_csv_data.push_back(std::to_string(test_solver_brute_force.get_average_time()));
            vec_csv_data.push_back(std::to_string(test_solver_brute_force.get_maximum_time()));
        } else {
            // In case if Brute Force will not run,
            // just fill out its data with 4 zeros.
            for(int i = 0; i < 4; ++i) {
                vec_csv_data.push_back(std::string("0"));
            }
        }

        if(run_branch_bound_solver) {
            vec_csv_data.push_back(std::to_string(test_solver_branch_bound.get_average_error()));
            vec_csv_data.push_back(std::to_string(test_solver_branch_bound.get_maximum_error()));
            vec_csv_data.push_back(std::to_string(test_solver_branch_bound.get_average_time()));
            vec_csv_data.push_back(std::to_string(test_solver_branch_bound.get_maximum_time()));
        } else {
            // Filling out with zeros, the same way,
            // as it was done for Brute Force solver.
            for(int i = 0; i < 4; ++i) {
                vec_csv_data.push_back(std::string("0"));
            }
        }

        vec_csv_data.push_back(std::to_string(test_solver_dynamic_programming_weight.get_average_error()));
        vec_csv_data.push_back(std::to_string(test_solver_dynamic_programming_weight.get_maximum_error()));
        vec_csv_data.push_back(std::to_string(test_solver_dynamic_programming_weight.get_average_time()));
        vec_csv_data.push_back(std::to_string(test_solver_dynamic_programming_weight.get_maximum_time()));

        vec_csv_data.push_back(std::to_string(test_solver_dynamic_programming_cost.get_average_error()));
        vec_csv_data.push_back(std::to_string(test_solver_dynamic_programming_cost.get_maximum_error()));
        vec_csv_data.push_back(std::to_string(test_solver_dynamic_programming_cost.get_average_time()));
        vec_csv_data.push_back(std::to_string(test_solver_dynamic_programming_cost.get_maximum_time()));

        vec_csv_data.push_back(std::to_string(test_solver_heuristics.get_average_error()));
        vec_csv_data.push_back(std::to_string(test_solver_heuristics.get_maximum_error()));
        vec_csv_data.push_back(std::to_string(test_solver_heuristics.get_average_time()));
        vec_csv_data.push_back(std::to_string(test_solver_heuristics.get_maximum_time()));

        vec_csv_data.push_back(std::to_string(test_solver_extended_heuristics.get_average_error()));
        vec_csv_data.push_back(std::to_string(test_solver_extended_heuristics.get_maximum_error()));
        vec_csv_data.push_back(std::to_string(test_solver_extended_heuristics.get_average_time()));
        vec_csv_data.push_back(std::to_string(test_solver_extended_heuristics.get_maximum_time()));

        // Write the data to the CSV file.
        std::string csv_file_name = csv_dir + "/experimental_results";
        FileCSVParser::write_line(csv_file_name, vec_csv_header, vec_csv_data);
    }

    return test_results;
}

bool tests_caller_simulated_annealing_knapsack(const std::string & csv_dir,
    const std::string & samples_dir, const std::string & inst_size, 
    const std::string & inst_type, const std::string & file_name_inst,
    const std::string & file_name_sol, int range_start, int range_end, 
    float final_temperature, float cooling_alpha, float heating_alpha,
    float inner_loop_to_items_ratio
) {
    EnumProblemType enum_problem_type = EnumProblemType::SimulatedAnnealingKnapsack;
    TestSolver test_solver_dynamic_programming_weight;
    TestSolver test_solver_simulated_annealing_knapsack;

    bool test_results = //TestFileParser::run() &&
        test_solver_dynamic_programming_weight.run(
            samples_dir + file_name_inst, samples_dir + file_name_sol,
            EnumSolvers::DynamicProgrammingWeight, enum_problem_type, -1, range_start, range_end
        ) &&
        test_solver_simulated_annealing_knapsack.run(
            samples_dir + file_name_inst, samples_dir + file_name_sol,
            EnumSolvers::SimulatedAnnealingKnapsack, enum_problem_type, -1,
            range_start, range_end, final_temperature, cooling_alpha, heating_alpha, inner_loop_to_items_ratio
        );

    if(test_results) {
        // CSV file header for average/maximum error/time measurements.
        std::vector<std::string> vec_csv_header = {
            "Instance size",
            "Final temperature",
            "Cooling alpha",
            "Heating alpha",
            "Inner loop length",
            "Dynamic Programming by Weight (average error)",
            "Dynamic Programming by Weight (maximum error)",
            "Dynamic Programming by Weight (average time)",
            "Dynamic Programming by Weight (maximum time)",
            "Simulated Annealing Knapsack (average error)",
            "Simulated Annealing Knapsack (maximum error)",
            "Simulated Annealing Knapsack (average time)",
            "Simulated Annealing Knapsack (maximum time)"
        };

        // Fill out CSV data.
        std::vector<std::string> vec_csv_data;
        vec_csv_data.push_back(inst_size);
        vec_csv_data.push_back(std::to_string(final_temperature));
        vec_csv_data.push_back(std::to_string(cooling_alpha));
        vec_csv_data.push_back(std::to_string(heating_alpha));
        vec_csv_data.push_back(std::to_string(std::stoi(inst_size) * inner_loop_to_items_ratio));

        vec_csv_data.push_back(std::to_string(test_solver_dynamic_programming_weight.get_average_error()));
        vec_csv_data.push_back(std::to_string(test_solver_dynamic_programming_weight.get_maximum_error()));
        vec_csv_data.push_back(std::to_string(test_solver_dynamic_programming_weight.get_average_time()));
        vec_csv_data.push_back(std::to_string(test_solver_dynamic_programming_weight.get_maximum_time()));

        vec_csv_data.push_back(std::to_string(test_solver_simulated_annealing_knapsack.get_average_error()));
        vec_csv_data.push_back(std::to_string(test_solver_simulated_annealing_knapsack.get_maximum_error()));
        vec_csv_data.push_back(std::to_string(test_solver_simulated_annealing_knapsack.get_average_time()));
        vec_csv_data.push_back(std::to_string(test_solver_simulated_annealing_knapsack.get_maximum_time()));

        // Write the data to the CSV file.
        std::string csv_file_name = csv_dir + "/simulated_annealing_knapsack_results";
        FileCSVParser::write_line(csv_file_name, vec_csv_header, vec_csv_data);

        // CSV file header for cost of each iteration of the Simulated Annealing.
        std::vector<std::string> vec_csv_costs_header = {"Iteration", "Cost"};
        std::vector<std::vector<std::string>> vec_csv_costs;

        // Fill out CSV data and write the data to the file.
        std::vector<float> simulated_annealing_costs =
            test_solver_simulated_annealing_knapsack.get_simulated_annealing_costs();

        for(int iter_id = 0; iter_id < simulated_annealing_costs.size(); ++iter_id) {
            const std::vector<std::string> vec_csv_line{std::to_string(iter_id),
                std::to_string(simulated_annealing_costs[iter_id])
            };

            vec_csv_costs.push_back(vec_csv_line);
        }

        // Write the data to the CSV file.
        std::string csv_costs_file_name = csv_dir + "/simulated_annealing_knapsack_costs_" + 
            inst_type + "_" + inst_size + "_f_" + std::to_string(final_temperature) + "_c_" +
            std::to_string(cooling_alpha) + "_h_" + std::to_string(heating_alpha) + "_i_" +
            std::to_string(inner_loop_to_items_ratio);
        
        FileCSVParser::write_full(csv_costs_file_name, vec_csv_costs_header, vec_csv_costs);
    }

    return test_results;
}

bool tests_caller_simulated_annealing_sat(const std::string & csv_dir,
    const std::string & samples_dir, const std::string & inst_type, 
    const std::string & file_name_inst, const std::string & file_name_sol, 
    float final_temperature, float cooling_alpha, float heating_alpha, 
    float inner_loop_to_items_ratio
) {
    TestSolver test_solver_simulated_annealing_sat;

    test_solver_simulated_annealing_sat.run_sat(
        samples_dir, file_name_inst, file_name_sol,
        final_temperature, cooling_alpha, heating_alpha, inner_loop_to_items_ratio
    );

    // todo:_ remove this
    std::cout << "SAT error: " << test_solver_simulated_annealing_sat.get_sat_error() << std::endl;

    // CSV file header for average/maximum error/time measurements.
    std::vector<std::string> vec_csv_header = {
        "Instance type",
        "Final temperature",
        "Cooling alpha",
        "Heating alpha",
        "Inner loop length",
        "Simulated Annealing SAT (error)",
        "Simulated Annealing SAT (time)",
    };

    // Fill out CSV data.
    std::vector<std::string> vec_csv_data;
    vec_csv_data.push_back(inst_type);
    vec_csv_data.push_back(std::to_string(final_temperature));
    vec_csv_data.push_back(std::to_string(cooling_alpha));
    vec_csv_data.push_back(std::to_string(heating_alpha));
    vec_csv_data.push_back(std::to_string(test_solver_simulated_annealing_sat.get_sat_num_variables() * inner_loop_to_items_ratio));
    vec_csv_data.push_back(std::to_string(test_solver_simulated_annealing_sat.get_sat_error()));
    vec_csv_data.push_back(std::to_string(test_solver_simulated_annealing_sat.get_sat_execution_time()));

    // Write the data to the CSV file.
    std::string csv_file_name = csv_dir + "/simulated_annealing_sat_results_" + inst_type;
    FileCSVParser::write_line(csv_file_name, vec_csv_header, vec_csv_data);

    // CSV file header for cost of each iteration of the Simulated Annealing.
    std::vector<std::string> vec_csv_costs_header = {"Iteration", "Cost"};
    std::vector<std::vector<std::string>> vec_csv_costs;

    // Fill out CSV data and write the data to the file.
    std::vector<float> simulated_annealing_costs =
        test_solver_simulated_annealing_sat.get_simulated_annealing_costs();

    for(int iter_id = 0; iter_id < simulated_annealing_costs.size(); ++iter_id) {
        const std::vector<std::string> vec_csv_line{std::to_string(iter_id),
            std::to_string(simulated_annealing_costs[iter_id])
        };

        vec_csv_costs.push_back(vec_csv_line);
    }

    // Write the data to the CSV file.
    std::string csv_costs_file_name = csv_dir + "/simulated_annealing_sat_costs_" + 
        inst_type + "_f_" + std::to_string(final_temperature) + "_c_" +
        std::to_string(cooling_alpha) + "_h_" + std::to_string(heating_alpha) + "_i_" +
        std::to_string(inner_loop_to_items_ratio);
    
    FileCSVParser::write_full(csv_costs_file_name, vec_csv_costs_header, vec_csv_costs);

    return true;
}

int str_find_first_digit(const std::string & str) {
    for(int i = 0; i < str.length(); ++i) {
        if(std::isdigit(str[i]))
            return i;
    }

    return -1;
}

std::string str_set_precision(double num, int precision) {
    std::string str_num = std::to_string(num);
    size_t period_id = str_num.find(".");
    size_t formatted_str_length;

    if(period_id == std::string::npos)
        return str_num;

    formatted_str_length = period_id + precision + 1;

    if(formatted_str_length > str_num.length())
        formatted_str_length = str_num.length();
    
    return str_num.substr(0, formatted_str_length);
}
