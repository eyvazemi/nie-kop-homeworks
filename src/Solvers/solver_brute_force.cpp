#include "solver_brute_force.h"

SolverBruteForce::SolverBruteForce() : SolverInterface("SolverBruteForce") {}

void SolverBruteForce::run(const KnapSack * knapsack, const SAT * sat) {
    // run DFS with each item
    for(int i = 0; i < knapsack->get_items_count(); i++) {
        if(((*(knapsack->get_items()))[i]).m_weight <= knapsack->get_max_weight()) {
            DFS(knapsack, std::set<int>{i}, i,
                ((*(knapsack->get_items()))[i]).m_weight,
                ((*(knapsack->get_items()))[i]).m_cost
            );
        }
    }
}

void SolverBruteForce::DFS(const KnapSack * knapsack, std::set<int> set_ids,
    int current_item_id, int current_weight, int current_cost
) {
    const std::vector<Item> * ptr_vec_items = knapsack->get_items();

    // check for cost
    if(current_cost > m_exec_results.m_highest_cost ||
        (current_cost == m_exec_results.m_highest_cost &&
            current_weight < m_exec_results.m_total_weight
        )
    ) {
        // update highest cost
        m_exec_results.m_highest_cost = current_cost;

        // update total weight
        m_exec_results.m_total_weight = current_weight;
        
        // store current best solution
        m_exec_results.m_set_item_ids = set_ids;
    }

    for(int i = current_item_id + 1; i < knapsack->get_items_count(); ++i) {
        // check for knapsack overweight
        if(((*ptr_vec_items)[i]).m_weight + current_weight > knapsack->get_max_weight())
            continue;

        std::set<int> tmp_set_ids = set_ids;
        tmp_set_ids.insert(i);

        DFS(knapsack, tmp_set_ids, i,
            current_weight + ((*ptr_vec_items)[i]).m_weight,
            current_cost + ((*ptr_vec_items)[i]).m_cost
        );
    }

    ++(m_exec_results.m_complexity);
}
