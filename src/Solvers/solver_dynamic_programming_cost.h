/**
 * @brief File constains implementation of dynamic programming with decomposition by cost.
 */

#ifndef SOLVER_DYNAMIC_PROGRAMMING_COST_H
#define SOLVER_DYNAMIC_PROGRAMMING_COST_H

#include "solver_interface.h"
#include "../Common/Structs/dynamic_array_entry.h"

class SolverDynamicProgrammingCost : public SolverInterface {
public: // methods
    SolverDynamicProgrammingCost();
    ~SolverDynamicProgrammingCost();

private: // methods
    void pre_calculation_stage(const KnapSack * knapsack, const SAT * sat) override;
    void run(const KnapSack * knapsack, const SAT * sat) override;
    void post_calculation_stage(const KnapSack * knapsack, const SAT * sat) override;
    inline void set_best_solution(const KnapSack * knapsack, int * final_weight,
        int * final_cost, std::vector<int> ** vec_final_items, int cost, int item_id
    );

private: // members
    std::vector<std::vector<DynamicArrayEntry>> m_vec_dynamic;
    int m_max_cost;
};

#endif // SOLVER_DYNAMIC_PROGRAMMING_COST_H
