#include "solver_simulated_annealing_sat.h"

SolverSimulatedAnnealingSAT::SolverSimulatedAnnealingSAT(float final_temperature,
    float cooling_alpha, float heating_alpha, float inner_loop_to_items_ratio
) : SolverSimulatedAnnealingInterface("SolverSimulatedAnnealingSAT",
        final_temperature, cooling_alpha, heating_alpha, inner_loop_to_items_ratio
) {}

SolverSimulatedAnnealingSAT::~SolverSimulatedAnnealingSAT() = default;

int SolverSimulatedAnnealingSAT::get_num_items(const KnapSack * knapsack, const SAT * sat) {
    return sat->get_num_variables();
}

void SolverSimulatedAnnealingSAT::set_initial_state(const KnapSack * knapsack, const SAT * sat) {
    // Clean all the members.
    m_new_values.clear();
    m_current_values.clear();
    m_new_cost = 0;
    m_current_cost = 0;
    m_new_num_unsat_clauses = sat->get_num_clauses();
    m_current_num_unsat_clauses = sat->get_num_clauses();
    m_exec_results.m_set_item_ids.clear();
    m_exec_results.m_highest_cost = 0;
    m_exec_results.m_total_weight = 0;
    m_exec_results.m_num_unsat_clauses = sat->get_num_clauses();

    // Instantiate the vector of variables' initial values to `false`.
    for(int i = 0; i < sat->get_num_variables(); ++i) {
        m_new_values.push_back(false);
        m_current_values.push_back(false);
    }
}

void SolverSimulatedAnnealingSAT::generate_new_state(const KnapSack * knapsack, const SAT * sat) {
    // Write the current values into the new values vector, as we have to do changes to the 
    // new values vector to not mess with the current state of variables' values.
    for(int variable_id = 0; variable_id < sat->get_num_variables(); ++variable_id) {
        m_new_values[variable_id] = m_current_values[variable_id];
    }

    // Assign a random value (either 0 or 1) to the random variable.
    int add_variable_id = SolverSimulatedAnnealingInterface::generate_random_number(
        0, sat->get_num_variables()
    );

    if(add_variable_id == sat->get_num_variables()) {
        --add_variable_id;
    }

    float random_val = SolverSimulatedAnnealingInterface::generate_random_number(
        0.f, 1.f
    );

    m_new_values[add_variable_id] = random_val < 0.5 ? false : true;

    // Calculate the number of unsatisfied clauses.
    int num_unsat_clauses = 0;

    for(int clause_id = 0; clause_id < sat->get_num_clauses(); ++clause_id) {
        const SATClause sat_clause = sat->get_clause(clause_id);
        bool is_clause_satisfied = false;

        for(int variable_clause_id = 0; variable_clause_id < SAT_CLAUSE_SIZE; ++variable_clause_id) {
            // Get value of the variable under `variable_id`.
            int variable = sat_clause.m_variables[variable_clause_id];
            int variable_id = std::abs(variable);
            bool variable_sign = variable >= 0 ? true : false;
            bool value = variable_sign ? m_new_values[variable_id] : !m_new_values[variable_id];

            if(value) {
                is_clause_satisfied = true;
                break;
            }
        }

        if(!is_clause_satisfied) {
            ++num_unsat_clauses;
        }
    }

    m_new_num_unsat_clauses = num_unsat_clauses;

    // Get the sum of weight variables.
    int weight = 0;
    int num_variables_true = 0;

    // Calculate the sum of weights of the variables assigned to 1.
    for(int variable_id = 0; variable_id < sat->get_num_variables(); ++variable_id) {
        if(m_new_values[variable_id]) {
            weight += sat->get_weight(variable_id);
            ++num_variables_true;
        }
    }

    // Check if there are any unsatisfied clauses and if there are, 
    // the "relaxation" will take place. Relaxation is a penalization of the weight 
    // based on the number of unsatisfied clauses, which is calculated in such manner:
    // (sum of weights of variables assigned to 1) - 1000 * (number of unsatisfied clauses).
    // Relaxation is useful only, when we already have a feasible configuration, i.e. all clauses
    // were satisfied, and we are trying to me to the infeasable configuration with some penalization.
    constexpr int penalty = 1000;
    m_new_cost = weight - penalty * m_new_num_unsat_clauses;
}

float SolverSimulatedAnnealingSAT::calculate_cost_change(const KnapSack * knapsack, const SAT * sat) {
    // We are subtracting new cost from the current one, as SAT problem
    // is a maximization problem and as in the classical Simulated Annealing,
    // new cost should be less than the current cost (like in a minimization problem),
    // we subtract the new cost from the current cost, compared to the vice versa,
    // which happens in classical Simulated Annealing.
    
    // We return the number of unsatisfied clauses, instead of the 
    // cost, as the feasible solution hasn't been found yet, thus
    // penalization doesn't occur yet.
    if(m_exec_results.m_num_unsat_clauses > 0) {
        return m_new_num_unsat_clauses - m_current_num_unsat_clauses;
    }

    // Feasible configuration has already been found, thus we can compare the 
    // costs that were calculated based on relaxation technique.
    return m_current_cost - m_new_cost;
}

void SolverSimulatedAnnealingSAT::set_new_state(const KnapSack * knapsack, const SAT * sat) {
    // Current weight will be assigned a value of the new weight, 
    // as new assignment of variables has been accepted.
    m_current_cost = m_new_cost;
    m_current_num_unsat_clauses = m_new_num_unsat_clauses;

    int actual_cost = 0;

    for(int variable_id = 0; variable_id < sat->get_num_variables(); ++variable_id) {
        m_current_values[variable_id] = m_new_values[variable_id];

        if(m_current_values[variable_id]) {
            actual_cost += sat->get_weight(variable_id);
        }
    }

    // Write the execution results only if all clauses were satisfied.
    if(m_current_num_unsat_clauses == 0 && actual_cost > m_exec_results.m_highest_cost) {
        m_exec_results.m_highest_cost = actual_cost;
        m_exec_results.m_total_weight = 0;
        m_exec_results.m_num_unsat_clauses = m_current_num_unsat_clauses;
        m_exec_results.m_set_item_ids.clear();

        // Write the indexes of variables into the the execution results,
        // the values of which is 1.
        for(int variable_id = 0; variable_id < m_current_values.size(); ++variable_id) {
            if(m_current_values[variable_id]) {
                m_exec_results.m_set_item_ids.insert(variable_id);
            }
        }
    }
}

float SolverSimulatedAnnealingSAT::get_current_cost() const {
    // We return the number of unsatisfied clauses, instead of the 
    // cost, as the feasible solution hasn't been found yet, thus
    // penalization doesn't occur yet.
    if(m_exec_results.m_num_unsat_clauses > 0) {
        return m_current_num_unsat_clauses;
    }

    return m_current_cost;
}
