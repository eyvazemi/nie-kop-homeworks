#include "solver_simulated_annealing_knapsack.h"

SolverSimulatedAnnealingKnapsack::SolverSimulatedAnnealingKnapsack(float final_temperature,
    float cooling_alpha, float heating_alpha, float inner_loop_to_items_ratio
) : SolverSimulatedAnnealingInterface("SolverSimulatedAnnealingKnapsack",
        final_temperature, cooling_alpha, heating_alpha, inner_loop_to_items_ratio
) {}

SolverSimulatedAnnealingKnapsack::~SolverSimulatedAnnealingKnapsack() = default;

int SolverSimulatedAnnealingKnapsack::get_num_items(const KnapSack * knapsack, const SAT * sat) {
    return knapsack->get_items_count();
}

void SolverSimulatedAnnealingKnapsack::set_initial_state(const KnapSack * knapsack, const SAT * sat) {
    // Clean all the members.
    m_new_items.clear();
    m_current_items.clear();
    m_current_cost = 0;
    m_exec_results.m_set_item_ids.clear();
    m_exec_results.m_highest_cost = 0;
    m_exec_results.m_total_weight = 0;

    // Instantiate the vector of items' presence in the knapsack.
    for(int i = 0; i < knapsack->get_items_count(); ++i) {
        m_new_items.push_back(false);
        m_current_items.push_back(false);
    }
}

void SolverSimulatedAnnealingKnapsack::generate_new_state(const KnapSack * knapsack, const SAT * sat) {
    // Write the current items into the new items vector, as we have to do changes to the 
    // new items vector to not mess with the current state of items in the knapsack.
    for(int item_id = 0; item_id < knapsack->get_items_count(); ++item_id) {
        m_new_items[item_id] = m_current_items[item_id];
    }

    // Random item will be picked and placed into the knapsack.
    // We pick a random item from the index 0 till the item with highest index.
    const int add_item_id = SolverSimulatedAnnealingInterface::generate_random_number(
        0, knapsack->get_items_count() - 1
    );

    m_new_items[add_item_id] = true;

    // Check if knapsack is overloaded, in which case, randomly pick items
    // out of the knapsack, until it is not overloaded anymore.
    // First, calculate the current total weight of the knapsack.
    unsigned weight = 0;

    for(int item_id = 0; item_id < knapsack->get_items_count(); ++item_id) {
        if(m_new_items[item_id]) {
            weight += (*knapsack->get_items())[item_id].m_weight;
        }
    }

    // While knapsack is overloaded, remove random items from it,
    // thus decreasing its weight by the weight of that item, 
    // until the knapsack is not overloaded anymore.
    while(weight > knapsack->get_max_weight()) {
        const int remove_item_id = SolverSimulatedAnnealingInterface::generate_random_number(
            0, knapsack->get_items_count() - 1
        );

        // Remove the item only if it was in the knapsack before.
        if(m_new_items[remove_item_id]) {
            m_new_items[remove_item_id] = false;
            weight -= (*knapsack->get_items())[remove_item_id].m_weight;
        }
    }
}

float SolverSimulatedAnnealingKnapsack::calculate_cost_change(const KnapSack * knapsack, const SAT * sat) {
    // Calculate the new cost of the knapsack (the state that hasn't been accepted yet)
    // and the current cost of the knapsack (the currently accepted state).
    float new_cost = 0;
    m_current_cost = 0;

    for(int item_id = 0; item_id < knapsack->get_items_count(); ++item_id) {
        if(m_current_items[item_id]) {
            m_current_cost += (*knapsack->get_items())[item_id].m_cost;
        }

        if(m_new_items[item_id]) {
            new_cost += (*knapsack->get_items())[item_id].m_cost;
        }
    }

    //std::cout << "Current_cost: " << current_cost << ", new_cost: " << new_cost << std::endl;

    // We are subtracting new cost from the current one, as knapsack problem
    // is a maximization problem and as in the classical Simulated Annealing,
    // new cost should be less than the current cost (like in a minimization problem),
    // we subtract the new cost from the current cost, compared to the vice versa,
    // which happens in classical Simulated Annealing.
    return m_current_cost - new_cost;
    //return new_cost - current_cost;
}

void SolverSimulatedAnnealingKnapsack::set_new_state(const KnapSack * knapsack, const SAT * sat) {
    int weight = 0;
    m_current_cost = 0;

    for(int item_id = 0; item_id < knapsack->get_items_count(); ++item_id) {
        m_current_items[item_id] = m_new_items[item_id];

        if(m_current_items[item_id]) {
            m_current_cost += (*knapsack->get_items())[item_id].m_cost;
            weight += (*knapsack->get_items())[item_id].m_weight;
        }
    }

    if(m_current_cost >= m_exec_results.m_highest_cost && weight <= knapsack->get_max_weight()) {
        m_exec_results.m_highest_cost = m_current_cost;
        m_exec_results.m_total_weight = weight;
        m_exec_results.m_set_item_ids.clear();

        for(int item_id = 0; item_id < m_current_items.size(); ++item_id) {
            if(m_current_items[item_id]) {
                m_exec_results.m_set_item_ids.insert(item_id);
            }
        }
    }
}

float SolverSimulatedAnnealingKnapsack::get_current_cost() const {
    return m_current_cost;
}
