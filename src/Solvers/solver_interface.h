#ifndef SOLVER_INTERFACE_H
#define SOLVER_INTERFACE_H

#include <string>
#include <set>
#include <chrono>
#include "../Common/Structs/knapsack.h"
#include "../Common/Structs/sat.h"
#include "../Common/Structs/exec_results.h"

struct Solution;

class SolverInterface {
public: // methods
    SolverInterface(const std::string & solver_name);
    virtual ~SolverInterface();

    /**
     * @brief Solves the knapsack problem and measures elapsed time.
     */
    void solve(const KnapSack * knapsack, const SAT * sat);

    /**
     * @brief Returns results of the solver.
     *
     * @return Result of an algorithm.
     */
    const ExecResults * get_exec_results() const;

protected: // methods
    /**
     * @brief Operations executed before the main calculations.
     */
    virtual void pre_calculation_stage(const KnapSack * knapsack, const SAT * sat);

    /**
     * @brief Calculations will be done in this function in child classes.
     */
    virtual void run(const KnapSack * knapsack, const SAT * sat) = 0;

    /**
     * @brief Operations executed after the main calculations.
     */
    virtual void post_calculation_stage(const KnapSack * knapsack, const SAT * sat);

protected: // members
    ExecResults m_exec_results;
};

#endif
