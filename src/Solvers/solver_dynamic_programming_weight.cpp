#include "solver_dynamic_programming_weight.h"

#include <climits>

SolverDynamicProgrammingWeight::SolverDynamicProgrammingWeight() : SolverInterface("SolverDynamicProgrammingWeight") {}

SolverDynamicProgrammingWeight::~SolverDynamicProgrammingWeight() {}

void SolverDynamicProgrammingWeight::pre_calculation_stage(const KnapSack * knapsack, const SAT * sat) {
    // initialize dynamic array
    std::vector<DynamicArrayEntry> vec_line(knapsack->get_items_count() + 1);
    std::vector<DynamicArrayEntry> vec_line_first(knapsack->get_items_count() + 1);
    std::vector<int> vec_items(knapsack->get_items_count()); // items in the knapsack in particular entry

    for(int i = 0; i < knapsack->get_items_count(); ++i)
        vec_items[i] = 0;

    for(int i = 0; i <= knapsack->get_items_count(); ++i) {
        vec_line[i].m_cost = -1;
        vec_line[i].m_weight = -1;
        vec_line[i].m_vec_items = vec_items;

        // set the first line as all zeros
        vec_line_first[i].m_cost = 0;
        vec_line_first[i].m_weight = 0;
        vec_line_first[i].m_vec_items = vec_items;
    }

    // set the first column values
    vec_line[0].m_cost = 0;
    vec_line[0].m_weight = 0;

    // insert the first line
    m_vec_dynamic.push_back(vec_line_first);

    // insert all other lines
    for(int i = 0; i < knapsack->get_max_weight(); ++i)
        m_vec_dynamic.push_back(vec_line);
}

void SolverDynamicProgrammingWeight::run(const KnapSack * knapsack, const SAT * sat) {
    std::vector<int> vec_items(knapsack->get_items_count()); // items that will be present in the entry
    const std::vector<Item> * vec_all_items = knapsack->get_items(); // all items
    std::vector<int> * vec_final_items;
    int final_cost = -1, final_weight = INT_MAX, new_weight;
    int current_item_weight, current_item_cost;

    for(int i = 0; i < knapsack->get_items_count(); ++i)
        vec_items[i] = 0;

    for(int item_id = 0; item_id < knapsack->get_items_count(); ++item_id) {
        for(int weight = 0; weight <= knapsack->get_max_weight(); ++weight) {
            // if the entry in dynamic array wasn't filled before, no need to do the calculations
            if(m_vec_dynamic[weight][item_id].m_weight < 0)
                continue;

            current_item_weight = (*vec_all_items)[item_id].m_weight;
            current_item_cost = (*vec_all_items)[item_id].m_cost;

            // calculate the obtained weight that is a sum of the dynamic array entry and current item
            new_weight = current_item_weight + m_vec_dynamic[weight][item_id].m_weight;

            // if the obtained weight is less than or equal to the maximum weight of the knapsack,
            // add current item to this entry of the dynamic array
            if(new_weight <= knapsack->get_max_weight() &&
                current_item_cost + m_vec_dynamic[weight][item_id].m_cost > m_vec_dynamic[new_weight][item_id + 1].m_cost
            ) {
                m_vec_dynamic[new_weight][item_id + 1].m_weight = new_weight;
                m_vec_dynamic[new_weight][item_id + 1].m_cost = current_item_cost + m_vec_dynamic[weight][item_id].m_cost;
                m_vec_dynamic[new_weight][item_id + 1].m_vec_items = m_vec_dynamic[weight][item_id].m_vec_items;
                m_vec_dynamic[new_weight][item_id + 1].m_vec_items[item_id] = 1;

                set_best_solution(knapsack, &final_weight,
                    &final_cost, &vec_final_items, new_weight, item_id
                );
            }

            // if the entry has positive value, copy that entry's data too, as it would be needed in further iterations
            if(m_vec_dynamic[weight][item_id].m_weight > 0 &&
                m_vec_dynamic[weight][item_id].m_cost > m_vec_dynamic[weight][item_id + 1].m_cost
            ) {
                m_vec_dynamic[weight][item_id + 1].m_weight = m_vec_dynamic[weight][item_id].m_weight;
                m_vec_dynamic[weight][item_id + 1].m_cost = m_vec_dynamic[weight][item_id].m_cost;
                m_vec_dynamic[weight][item_id + 1].m_vec_items = m_vec_dynamic[weight][item_id].m_vec_items;

                set_best_solution(knapsack, &final_weight,
                    &final_cost, &vec_final_items, weight, item_id
                );
            }
        }
    }

    for(int item_id = 0; item_id < vec_final_items->size(); ++item_id) {
        if((*vec_final_items)[item_id])
            m_exec_results.m_set_item_ids.insert(item_id);
    }
}

void SolverDynamicProgrammingWeight::post_calculation_stage(const KnapSack * knapsack, const SAT * sat) {
    m_vec_dynamic.clear();
}

void SolverDynamicProgrammingWeight::set_best_solution(const KnapSack * knapsack, int * final_weight,
    int * final_cost, std::vector<int> ** vec_final_items, int weight, int item_id
) {
    if(item_id == knapsack->get_items_count() - 1 &&
        ((m_vec_dynamic[weight][item_id + 1].m_cost > *final_cost &&
        m_vec_dynamic[weight][item_id + 1].m_weight <= knapsack->get_max_weight()) ||
        (m_vec_dynamic[weight][item_id + 1].m_cost == *final_cost &&
        m_vec_dynamic[weight][item_id + 1].m_weight < *final_weight))
    ) {
        m_exec_results.m_highest_cost = m_vec_dynamic[weight][item_id + 1].m_cost;
        m_exec_results.m_total_weight = m_vec_dynamic[weight][item_id + 1].m_weight;
        *final_cost = m_vec_dynamic[weight][item_id + 1].m_cost;
        *final_weight = m_vec_dynamic[weight][item_id + 1].m_weight;
        *vec_final_items = &(m_vec_dynamic[weight][item_id + 1].m_vec_items);
    }
}
