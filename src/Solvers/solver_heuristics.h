#ifndef SOLVER_HEURISTICS_H
#define SOLVER_HEURISTICS_H

#include "solver_interface.h"

enum HeuristicsType {
    Simple,
    Extended
};

class SolverHeuristics : public SolverInterface {
public: // methods
    SolverHeuristics(HeuristicsType heuristics_type);
    ~SolverHeuristics() = default;

private: // methods
    void run(const KnapSack * knapsack, const SAT * sat) override;

private: // members
    HeuristicsType m_heuristics_type;
};

#endif // SOLVER_HEURISTICS_H
