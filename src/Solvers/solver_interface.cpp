#include "solver_interface.h"

SolverInterface::SolverInterface(const std::string & solver_name) {
    m_exec_results.m_solver_name = solver_name;
}

SolverInterface::~SolverInterface() {}

void SolverInterface::pre_calculation_stage(const KnapSack * knapsack, const SAT * sat) {}

void SolverInterface::solve(const KnapSack * knapsack, const SAT * sat) {
    std::chrono::high_resolution_clock::time_point start, end;

    // reset members
    m_exec_results.m_complexity = 0;
    m_exec_results.m_highest_cost = 0;
    m_exec_results.m_total_weight = 0;
    (m_exec_results.m_set_item_ids).erase(
        (m_exec_results.m_set_item_ids).begin(),
        (m_exec_results.m_set_item_ids).end()
    );
    
    // run solver
    pre_calculation_stage(knapsack, sat);
    start = std::chrono::high_resolution_clock::now();
    run(knapsack, sat);
    end = std::chrono::high_resolution_clock::now();
    post_calculation_stage(knapsack, sat);
    
    // store elapsed time
    m_exec_results.m_elapsed_time = (std::chrono::duration_cast<std::chrono::microseconds>(end - start)).count();
}

void SolverInterface::post_calculation_stage(const KnapSack * knapsack, const SAT * sat) {}

const ExecResults * SolverInterface::get_exec_results() const {
    return &m_exec_results;
}
