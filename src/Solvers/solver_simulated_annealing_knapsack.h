/**
 * @brief File constains implementation of Simulated Annealing for Knapsack problem.
 */

#ifndef SOLVER_SIMULATED_ANNEALING_KNAPSACK_H
#define SOLVER_SIMULATED_ANNEALING_KNAPSACK_H

#include "solver_simulated_annealing_interface.h"

class SolverSimulatedAnnealingKnapsack : public SolverSimulatedAnnealingInterface {
public: // methods
    SolverSimulatedAnnealingKnapsack(float final_temperature,
        float cooling_alpha, float heating_alpha, float inner_loop_to_items_ratio
    );

    ~SolverSimulatedAnnealingKnapsack();

private: // methods
    int get_num_items(const KnapSack * knapsack, const SAT * sat) override;
    void set_initial_state(const KnapSack * knapsack, const SAT * sat) override;
    void generate_new_state(const KnapSack * knapsack, const SAT * sat) override;
    float calculate_cost_change(const KnapSack * knapsack, const SAT * sat) override;

    // Will write the values of `m_new_items` into
    // `m_current_items`, as currently generated items were accepted.
    void set_new_state(const KnapSack * knapsack, const SAT * sat) override;

    float get_current_cost() const override;

private: // members
    // For each items stores `false` if item is
    // not in the knapsack and `true` if it is.
    // `m_current_items` contains items that were currently generated
    // and not yet accepted, whereas `m_previous_items` contains items
    // that are currently in the knapsack.
    std::vector<bool> m_new_items;
    std::vector<bool> m_current_items;

    // Current cost of the items in the knapsack.
    float m_current_cost;
};

#endif // SOLVER_SIMULATED_ANNEALING_KNAPSACK_H
