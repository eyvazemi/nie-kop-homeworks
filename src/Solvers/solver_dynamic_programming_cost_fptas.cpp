#include "solver_dynamic_programming_cost_fptas.h"

#include <climits>
#include <cmath>

SolverDynamicProgrammingCostFPTAS::SolverDynamicProgrammingCostFPTAS(float error) :
    SolverInterface("SolverDynamicProgrammingCostFPTAS"), m_error(error) {}

SolverDynamicProgrammingCostFPTAS::~SolverDynamicProgrammingCostFPTAS() {}

void SolverDynamicProgrammingCostFPTAS::pre_calculation_stage(const KnapSack * knapsack, const SAT * sat) {
    // initialize dynamic array
    std::vector<DynamicArrayEntry> vec_line(knapsack->get_items_count() + 1);
    std::vector<DynamicArrayEntry> vec_line_first(knapsack->get_items_count() + 1);
    std::vector<int> vec_items(knapsack->get_items_count()); // items in the knapsack in particular entry
    
    for(int i = 0; i < knapsack->get_items_count(); ++i)
        vec_items[i] = 0;

    for(int i = 0; i <= knapsack->get_items_count(); ++i) {
        vec_line[i].m_cost = -1;
        vec_line[i].m_weight = -1;
        vec_line[i].m_vec_items = vec_items;

        // set the first line as all zeros
        vec_line_first[i].m_cost = 0;
        vec_line_first[i].m_weight = 0;
        vec_line_first[i].m_vec_items = vec_items;
    }

    // set the first column values
    vec_line[0].m_cost = 0;
    vec_line[0].m_weight = 0;

    // cost of the item with maximum cost
    int item_max_cost = 0;

    // get the sum of costs of all items along with the cost of the item with the highest cost
    m_max_cost = 0;

    for(int i = 0; i < knapsack->get_items()->size(); ++i) {
        int current_cost = (*(knapsack->get_items()))[i].m_cost;
        m_max_cost += current_cost;

        if(current_cost > item_max_cost)
            item_max_cost = current_cost;
    }
    
    // For FPTAS algorithm, we need to calculate the number of chunks (bins) that will serve as 
    // ranges of costs instead of handling each cost idividually, as it was done in plain dynamic programming
    // by cost. In order to achieve this, we find the chunk index of the total cost, as it is the cost of the 
    // last array in 2D array. We calculate chunk index of the given cost based on the given error via the
    // formula that is taken from here: https://moodle-vyuka.cvut.cz/mod/page/view.php?id=259938
    m_fptas_num_chunks = fmax(floorf((float) m_max_cost / ((float) (m_error * item_max_cost) / knapsack->get_items_count())), 1);
    m_fptas_chunk_size = (float) m_max_cost / m_fptas_num_chunks;

    // write number of chunks to the exectution results
    m_exec_results.m_fptas_num_chunks = m_fptas_num_chunks;

    // insert the first line
    m_vec_dynamic.push_back(DynamicArrayLine{vec_line_first, 0, 0});

    // insert all other lines
    for(int chunk_id = 1, chunk_start = 1; chunk_id <= m_fptas_num_chunks; ++chunk_id, chunk_start += m_fptas_chunk_size) {
        if(chunk_id == m_fptas_num_chunks) {
            int last_chunk_size = m_max_cost - (m_fptas_num_chunks - 1) * m_fptas_chunk_size;
            m_vec_dynamic.push_back(DynamicArrayLine{vec_line, chunk_start, chunk_start + last_chunk_size - 1});
        } else
            m_vec_dynamic.push_back(DynamicArrayLine{vec_line, chunk_start, chunk_start + m_fptas_chunk_size - 1});
    }
}

void SolverDynamicProgrammingCostFPTAS::run(const KnapSack * knapsack, const SAT * sat) {
    std::vector<int> vec_items(knapsack->get_items_count()); // items that will be present in the entry
    const std::vector<Item> * vec_all_items = knapsack->get_items(); // all items
    std::vector<int> * vec_final_items;
    int final_cost = -1, final_weight = INT_MAX;
    int current_item_weight, current_item_cost;
    int new_cost, new_cost_chunk_id;

    for(int i = 0; i < knapsack->get_items_count(); ++i)
        vec_items[i] = 0;

    for(int item_id = 0; item_id < knapsack->get_items_count(); ++item_id) {
        for(int cost_chunk_id = 0; cost_chunk_id <= m_fptas_num_chunks; ++cost_chunk_id) {
            // if the entry in dynamic array wasn't filled before, no need to do the calculations
            if(m_vec_dynamic[cost_chunk_id].m_vec_line[item_id].m_weight < 0)
                continue;

            current_item_weight = (*vec_all_items)[item_id].m_weight;
            current_item_cost = (*vec_all_items)[item_id].m_cost;

            // calculate the obtained weight that is a sum of the dynamic array entry and current item
            new_cost = current_item_cost + m_vec_dynamic[cost_chunk_id].m_vec_line[item_id].m_cost;
            new_cost_chunk_id = new_cost / m_fptas_chunk_size + 1;

            // check if it is the last chunk
            if(new_cost <= m_max_cost && new_cost_chunk_id > m_fptas_num_chunks)
                new_cost_chunk_id = m_fptas_num_chunks;

            // if the obtained weight is less than or equal to the maximum weight of the knapsack,
            // add current item to this entry of the dynamic array
            if(new_cost <= m_max_cost && (m_vec_dynamic[new_cost_chunk_id].m_vec_line[item_id + 1].m_weight < 0 ||
                current_item_weight + m_vec_dynamic[cost_chunk_id].m_vec_line[item_id].m_weight <
                m_vec_dynamic[new_cost_chunk_id].m_vec_line[item_id + 1].m_weight)
            ) {
                m_vec_dynamic[new_cost_chunk_id].m_vec_line[item_id + 1].m_weight = current_item_weight +
                    m_vec_dynamic[cost_chunk_id].m_vec_line[item_id].m_weight;
                m_vec_dynamic[new_cost_chunk_id].m_vec_line[item_id + 1].m_cost = new_cost;
                m_vec_dynamic[new_cost_chunk_id].m_vec_line[item_id + 1].m_vec_items =
                    m_vec_dynamic[cost_chunk_id].m_vec_line[item_id].m_vec_items;
                m_vec_dynamic[new_cost_chunk_id].m_vec_line[item_id + 1].m_vec_items[item_id] = 1;

                set_best_solution(knapsack, &final_weight,
                    &final_cost, &vec_final_items, new_cost_chunk_id, item_id
                );
            }

            // if the entry has positive value, copy that entry's data too, as it would be needed in further iterations
            if(m_vec_dynamic[cost_chunk_id].m_vec_line[item_id].m_weight > 0 &&
                (m_vec_dynamic[cost_chunk_id].m_vec_line[item_id + 1].m_weight < 0 ||
                m_vec_dynamic[cost_chunk_id].m_vec_line[item_id].m_weight <
                m_vec_dynamic[cost_chunk_id].m_vec_line[item_id + 1].m_weight)
            ) {
                m_vec_dynamic[cost_chunk_id].m_vec_line[item_id + 1].m_weight =
                    m_vec_dynamic[cost_chunk_id].m_vec_line[item_id].m_weight;
                m_vec_dynamic[cost_chunk_id].m_vec_line[item_id + 1].m_cost =
                    m_vec_dynamic[cost_chunk_id].m_vec_line[item_id].m_cost;
                m_vec_dynamic[cost_chunk_id].m_vec_line[item_id + 1].m_vec_items =
                    m_vec_dynamic[cost_chunk_id].m_vec_line[item_id].m_vec_items;

                set_best_solution(knapsack, &final_weight,
                    &final_cost, &vec_final_items, cost_chunk_id, item_id
                );
            }
        }
    }

    for(int item_id = 0; item_id < vec_final_items->size(); ++item_id) {
        if((*vec_final_items)[item_id])
            m_exec_results.m_set_item_ids.insert(item_id);
    }
}

void SolverDynamicProgrammingCostFPTAS::post_calculation_stage(const KnapSack * knapsack, const SAT * sat) {
    m_vec_dynamic.clear();
}

void SolverDynamicProgrammingCostFPTAS::set_best_solution(const KnapSack * knapsack, int * final_weight,
    int * final_cost, std::vector<int> ** vec_final_items, int cost_chunk_id, int item_id
) {
    if(item_id == knapsack->get_items_count() - 1 &&
        ((m_vec_dynamic[cost_chunk_id].m_vec_line[item_id + 1].m_cost > *final_cost &&
        m_vec_dynamic[cost_chunk_id].m_vec_line[item_id + 1].m_weight <= knapsack->get_max_weight()) ||
        (m_vec_dynamic[cost_chunk_id].m_vec_line[item_id + 1].m_cost == *final_cost &&
        m_vec_dynamic[cost_chunk_id].m_vec_line[item_id + 1].m_weight < *final_weight))
    ) {
        m_exec_results.m_highest_cost = m_vec_dynamic[cost_chunk_id].m_vec_line[item_id + 1].m_cost;
        m_exec_results.m_total_weight = m_vec_dynamic[cost_chunk_id].m_vec_line[item_id + 1].m_weight;
        *final_cost = m_vec_dynamic[cost_chunk_id].m_vec_line[item_id + 1].m_cost;
        *final_weight = m_vec_dynamic[cost_chunk_id].m_vec_line[item_id + 1].m_weight;
        *vec_final_items = &(m_vec_dynamic[cost_chunk_id].m_vec_line[item_id + 1].m_vec_items);
    }
}
