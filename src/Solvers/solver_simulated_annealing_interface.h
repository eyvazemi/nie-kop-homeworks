/**
 * @brief File constains implementation of interface for Simulated Annealing.
 */

#ifndef SOLVER_SIMULATED_ANNEALING_INTERFACE_H
#define SOLVER_SIMULATED_ANNEALING_INTERFACE_H

#include "solver_interface.h"

class SolverSimulatedAnnealingInterface : public SolverInterface {
    enum TemperatureModificationType {
        Cooling,
        Heating
    };

public: // methods
    SolverSimulatedAnnealingInterface(const std::string & solver_name,
        float final_temperature, float cooling_alpha,
        float heating_alpha, float inner_loop_to_items_ratio
    );

    virtual ~SolverSimulatedAnnealingInterface();

private: // methods
    void pre_calculation_stage(const KnapSack * knapsack, const SAT * sat) override;
    void run(const KnapSack * knapsack, const SAT * sat) override;
    virtual void post_calculation_stage(const KnapSack * knapsack, const SAT * sat) override;

    /**
     * @brief Runs Simulated Annealing, where temperature either cools down or heats up,
     * depending on the given parameter.
     * 
     * This method will first run with heating up temperature to calculate the probability
     * of getting out of the local minimum (this probability should be around 50%),
     * this way, we will find the needed initial temperature.
     * 
     * After that, we will find run actual Simulated Annealing with the found initial temperature.
    */
    void run_simulated_annealing(
        const KnapSack * knapsack, const SAT * sat, int num_items,
        const TemperatureModificationType temperature_modification_type
    );

    /**
    * @brief Should return the number of items we are dealing with in this problem.
    */ 
    virtual int get_num_items(const KnapSack * knapsack, const SAT * sat) = 0;

    /**
    * @brief Should randomly set the initial state.
    */
    virtual void set_initial_state(const KnapSack * knapsack, const SAT * sat) = 0;

    /**
    * @brief Should randomly generate the new state. The newly generated state
    * should also be stored, as we will set it as the next state, in case if it will be accepted.
    */
    virtual void generate_new_state(const KnapSack * knapsack, const SAT * sat) = 0;

    /**
     * @brief Should calculate and return the cost change.
    */
    virtual float calculate_cost_change(const KnapSack * knapsack, const SAT * sat) = 0;

    /**
     * @brief Should set the generated new state, that was stored, as the new state.
    */
    virtual void set_new_state(const KnapSack * knapsack, const SAT * sat) = 0;

    /**
     * @brief Should return the current cost of the items in the knapsack.
    */
    virtual float get_current_cost() const = 0;

protected: // methods
    static float generate_random_number(float range_start, float range_end);

private: // methods
    /**
     * @brief Returns `true` in case if the newly generated state should be accepted.
    */
    bool accept(float temperature, float cost_change);

private: // members
    float m_final_temperature;
    float m_cooling_alpha;
    float m_heating_alpha;
    float m_inner_loop_to_items_ratio;

protected: // members
    float m_initial_temperature;
};

#endif // SOLVER_SIMULATED_ANNEALING_INTERFACE_H
