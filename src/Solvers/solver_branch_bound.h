#ifndef SOLVER_BRANCH_BOUND_H
#define SOLVER_BRANCH_BOUND_H

#include <set>
#include "solver_interface.h"

class SolverBranchBound : public SolverInterface {
public: // members
    SolverBranchBound();
    ~SolverBranchBound() = default;

private: // members
    void run(const KnapSack * knapsack, const SAT * sat) override;
    void DFS_pruning(const KnapSack * knapsack, const std::vector<Item> * vec_best_solutions,
        std::set<int> set_ids, int current_item_id,
        int current_weight, int current_cost, int * target_cost
    );
};

#endif
