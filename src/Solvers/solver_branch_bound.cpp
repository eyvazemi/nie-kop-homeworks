#include "solver_branch_bound.h"

SolverBranchBound::SolverBranchBound() : SolverInterface("SolverBranchBound") {}

void SolverBranchBound::run(const KnapSack * knapsack, const SAT * sat) {
    // knapsack items
    const std::vector<Item> * ptr_vec_items = knapsack->get_items();

    // vector with best solutions, regarding to cost, possible to achieve for each item
    // weights are also taken into the account,
    // so that the best local solution doesn't overweight the knapsack
    std::vector<Item> vec_best_solutions((*ptr_vec_items).size());

    // populate the vector of upper bounds 
    for(int i = (*ptr_vec_items).size() - 1; i >= 0; --i) {
        // current item
        const Item * ptr_item = &((*ptr_vec_items)[i]);

        // upper bound of cost for this item
        Item best_solution;

        if(i == (*ptr_vec_items).size() - 1) {
            (vec_best_solutions[i]).m_cost = ptr_item->m_cost;
            (vec_best_solutions[i]).m_weight = ptr_item->m_weight;
        } else {
            (vec_best_solutions[i]).m_cost = ptr_item->m_cost + (vec_best_solutions[i + 1]).m_cost;
            (vec_best_solutions[i]).m_weight = ptr_item->m_weight + (vec_best_solutions[i + 1]).m_weight;
        }

        //++(m_exec_results.m_complexity);
    }

    // lower bound for the cost
    int * target_cost = new int;
    *target_cost = 0; //knapsack->get_target_cost();

    // call DFS function with pruning regarding cost
    for(int i = 0; i < knapsack->get_items_count(); ++i) {
        if(((*(knapsack->get_items()))[i]).m_weight <= knapsack->get_max_weight()) {
            DFS_pruning(knapsack, &vec_best_solutions, std::set<int>{i}, i,
                ((*(knapsack->get_items()))[i]).m_weight,
                ((*(knapsack->get_items()))[i]).m_cost, target_cost
            );
        }
    }

    delete target_cost;
}

void SolverBranchBound::DFS_pruning(const KnapSack * knapsack, const std::vector<Item> * vec_best_solutions,
        std::set<int> set_ids, int current_item_id, int current_weight, int current_cost, int * target_cost
) {
    const std::vector<Item> * ptr_vec_items = knapsack->get_items();

    // check for cost
    if(current_cost > m_exec_results.m_highest_cost ||
        (current_cost == m_exec_results.m_highest_cost &&
            current_weight < m_exec_results.m_total_weight
        )
    ) {
        // update highest cost
        m_exec_results.m_highest_cost = current_cost;

        // update lower bound for the cost
        if(m_exec_results.m_highest_cost > *target_cost)
            *target_cost = m_exec_results.m_highest_cost;

        // update total weight
        m_exec_results.m_total_weight = current_weight;
        
        // store current best solution
        m_exec_results.m_set_item_ids = set_ids;
    }

    for(int i = current_item_id + 1; i < knapsack->get_items_count(); ++i) {
        // check for knapsack overweight and if the sum of an accumulated costs with the best possible
        // cost with all the remaining items is less than the current highest cost
        if(((*ptr_vec_items)[i]).m_weight + current_weight > knapsack->get_max_weight() ||
            current_cost + ((*vec_best_solutions)[i]).m_cost < *target_cost
        ) {
            continue;
        }

        std::set<int> tmp_set_ids = set_ids;
        tmp_set_ids.insert(i);

        DFS_pruning(knapsack, vec_best_solutions, tmp_set_ids, i,
            current_weight + ((*ptr_vec_items)[i]).m_weight,
            current_cost + ((*ptr_vec_items)[i]).m_cost, target_cost
        );
    }

    ++(m_exec_results.m_complexity);
}
