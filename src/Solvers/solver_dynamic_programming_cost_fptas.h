/**
 * @brief File constains implementation of FPTAS version of dynamic programming with decomposition by cost.
 */

#ifndef SOLVER_DYNAMIC_PROGRAMMING_COST_FPTAS_H
#define SOLVER_DYNAMIC_PROGRAMMING_COST_FPTAS_H

#include "solver_interface.h"
#include "../Common/Structs/dynamic_array_entry.h"

struct DynamicArrayLine;

class SolverDynamicProgrammingCostFPTAS : public SolverInterface {
public: // methods
    SolverDynamicProgrammingCostFPTAS(float error);
    ~SolverDynamicProgrammingCostFPTAS();

private: // methods
    void pre_calculation_stage(const KnapSack * knapsack, const SAT * sat) override;
    void run(const KnapSack * knapsack, const SAT * sat) override;
    void post_calculation_stage(const KnapSack * knapsack, const SAT * sat) override;
    inline void set_best_solution(const KnapSack * knapsack, int * final_weight,
        int * final_cost, std::vector<int> ** vec_final_items, int cost_chunk_id, int item_id
    );

private: // members
    std::vector<DynamicArrayLine> m_vec_dynamic;
    float m_error;
    int m_max_cost;
    int m_fptas_num_chunks;
    int m_fptas_chunk_size;
};

struct DynamicArrayLine {
    std::vector<DynamicArrayEntry> m_vec_line;
    int m_chunk_start, m_chunk_end;
};

#endif // SOLVER_DYNAMIC_PROGRAMMING_COST_FPTAS_H
