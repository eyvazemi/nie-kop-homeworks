#include "solver_dynamic_programming_cost.h"

#include <climits>

SolverDynamicProgrammingCost::SolverDynamicProgrammingCost() : SolverInterface("SolverDynamicProgrammingCost") {}

SolverDynamicProgrammingCost::~SolverDynamicProgrammingCost() {}

void SolverDynamicProgrammingCost::pre_calculation_stage(const KnapSack * knapsack, const SAT * sat) {
    // initialize dynamic array
    std::vector<DynamicArrayEntry> vec_line(knapsack->get_items_count() + 1);
    std::vector<DynamicArrayEntry> vec_line_first(knapsack->get_items_count() + 1);
    std::vector<int> vec_items(knapsack->get_items_count()); // items in the knapsack in particular entry

    for(int i = 0; i < knapsack->get_items_count(); ++i)
        vec_items[i] = 0;

    for(int i = 0; i <= knapsack->get_items_count(); ++i) {
        vec_line[i].m_cost = -1;
        vec_line[i].m_weight = -1;
        vec_line[i].m_vec_items = vec_items;

        // set the first line as all zeros
        vec_line_first[i].m_cost = 0;
        vec_line_first[i].m_weight = 0;
        vec_line_first[i].m_vec_items = vec_items;
    }

    // set the first column values
    vec_line[0].m_cost = 0;
    vec_line[0].m_weight = 0;

    // insert the first line
    m_vec_dynamic.push_back(vec_line_first);

    // get the sum of costs of all items
    m_max_cost = 0;

    for(int i = 0; i < knapsack->get_items()->size(); ++i)
        m_max_cost += (*(knapsack->get_items()))[i].m_cost;

    // insert all other lines
    for(int i = 0; i < m_max_cost; ++i)
        m_vec_dynamic.push_back(vec_line);
}

void SolverDynamicProgrammingCost::run(const KnapSack * knapsack, const SAT * sat) {
    std::vector<int> vec_items(knapsack->get_items_count()); // items that will be present in the entry
    const std::vector<Item> * vec_all_items = knapsack->get_items(); // all items
    std::vector<int> * vec_final_items;
    int final_cost = -1, final_weight = INT_MAX, new_cost;
    int current_item_weight, current_item_cost;

    for(int i = 0; i < knapsack->get_items_count(); ++i)
        vec_items[i] = 0;

    for(int item_id = 0; item_id < knapsack->get_items_count(); ++item_id) {
        for(int cost = 0; cost <= m_max_cost; ++cost) {
            // if the entry in dynamic array wasn't filled before, no need to do the calculations
            if(m_vec_dynamic[cost][item_id].m_weight < 0)
                continue;

            current_item_weight = (*vec_all_items)[item_id].m_weight;
            current_item_cost = (*vec_all_items)[item_id].m_cost;

            // calculate the obtained cost, as a sum of the dynamic array entry and current item
            new_cost = current_item_cost + m_vec_dynamic[cost][item_id].m_cost;

            // if the obtained weight is less than or equal to the maximum weight of the knapsack,
            // add current item to this entry of the dynamic array
            if(new_cost <= m_max_cost && (m_vec_dynamic[new_cost][item_id + 1].m_weight < 0 ||
                current_item_weight + m_vec_dynamic[cost][item_id].m_weight <
                m_vec_dynamic[new_cost][item_id + 1].m_weight)
            ) {
                m_vec_dynamic[new_cost][item_id + 1].m_weight = current_item_weight + m_vec_dynamic[cost][item_id].m_weight;
                m_vec_dynamic[new_cost][item_id + 1].m_cost = new_cost;
                m_vec_dynamic[new_cost][item_id + 1].m_vec_items = m_vec_dynamic[cost][item_id].m_vec_items;
                m_vec_dynamic[new_cost][item_id + 1].m_vec_items[item_id] = 1;

                set_best_solution(knapsack, &final_weight,
                    &final_cost, &vec_final_items, new_cost, item_id
                );
            }

            // if the entry has positive value, copy that entry's data too, as it would be needed in further iterations
            if(m_vec_dynamic[cost][item_id].m_weight > 0 && (m_vec_dynamic[cost][item_id + 1].m_weight < 0 ||
                m_vec_dynamic[cost][item_id].m_weight < m_vec_dynamic[cost][item_id + 1].m_weight)
            ) {
                m_vec_dynamic[cost][item_id + 1].m_weight = m_vec_dynamic[cost][item_id].m_weight;
                m_vec_dynamic[cost][item_id + 1].m_cost = m_vec_dynamic[cost][item_id].m_cost;
                m_vec_dynamic[cost][item_id + 1].m_vec_items = m_vec_dynamic[cost][item_id].m_vec_items;

                set_best_solution(knapsack, &final_weight,
                    &final_cost, &vec_final_items, cost, item_id
                );
            }
        }
    }

    for(int item_id = 0; item_id < vec_final_items->size(); ++item_id) {
        if((*vec_final_items)[item_id])
            m_exec_results.m_set_item_ids.insert(item_id);
    }
}

void SolverDynamicProgrammingCost::post_calculation_stage(const KnapSack * knapsack, const SAT * sat) {
    m_vec_dynamic.clear();
}

void SolverDynamicProgrammingCost::set_best_solution(const KnapSack * knapsack, int * final_weight,
    int * final_cost, std::vector<int> ** vec_final_items, int cost, int item_id
) {
    if(item_id == knapsack->get_items_count() - 1 &&
        ((m_vec_dynamic[cost][item_id + 1].m_cost > *final_cost &&
        m_vec_dynamic[cost][item_id + 1].m_weight <= knapsack->get_max_weight()) ||
        (m_vec_dynamic[cost][item_id + 1].m_cost == *final_cost &&
        m_vec_dynamic[cost][item_id + 1].m_weight < *final_weight))
    ) {
        m_exec_results.m_highest_cost = m_vec_dynamic[cost][item_id + 1].m_cost;
        m_exec_results.m_total_weight = m_vec_dynamic[cost][item_id + 1].m_weight;
        *final_cost = m_vec_dynamic[cost][item_id + 1].m_cost;
        *final_weight = m_vec_dynamic[cost][item_id + 1].m_weight;
        *vec_final_items = &(m_vec_dynamic[cost][item_id + 1].m_vec_items);
    }
}
