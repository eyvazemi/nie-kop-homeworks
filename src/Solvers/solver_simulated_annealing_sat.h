 /**
 * @brief File constains implementation of Simulated Annealing for SAT problem.
 */

#ifndef SOLVER_SIMULATED_ANNEALING_SAT_H
#define SOLVER_SIMULATED_ANNEALING_SAT_H

#include "solver_simulated_annealing_interface.h"

class SolverSimulatedAnnealingSAT : public SolverSimulatedAnnealingInterface {
public: // methods
    SolverSimulatedAnnealingSAT(float final_temperature,
        float cooling_alpha, float heating_alpha, float inner_loop_to_items_ratio
    );

    ~SolverSimulatedAnnealingSAT();

private: // methods
    int get_num_items(const KnapSack * knapsack, const SAT * sat) override;
    void set_initial_state(const KnapSack * knapsack, const SAT * sat) override;
    void generate_new_state(const KnapSack * knapsack, const SAT * sat) override;
    float calculate_cost_change(const KnapSack * knapsack, const SAT * sat) override;
    void set_new_state(const KnapSack * knapsack, const SAT * sat) override;
    float get_current_cost() const override;

private: // members
    // For each variable, stores the value of it.
    // `m_new_values` contains values that were currently generated
    // and not yet accepted, whereas `m_current_values` contains 
    // the current value of variables.
    std::vector<bool> m_new_values;
    std::vector<bool> m_current_values;

    // New and current costs of the SAT.
    float m_new_cost;
    float m_current_cost;

    // Number of unsatisfied clauses in the current interation.
    int m_new_num_unsat_clauses;
    int m_current_num_unsat_clauses;
};

#endif // SOLVER_SIMULATED_ANNEALING_SAT_H