#include "solver_heuristics.h"

#include <string>
#include <map>

SolverHeuristics::SolverHeuristics(HeuristicsType heuristics_type) : SolverInterface("SolverHeuristics") {
    m_heuristics_type = heuristics_type;

    if(m_heuristics_type == Extended)
        m_exec_results.m_solver_name += std::string("Extended");
}

void SolverHeuristics::run(const KnapSack * knapsack, const SAT * sat) {
    std::map<double, int> map_cost_weight; // map with cost/weight as a key and id as value
    std::vector<int> vec_cost_weight; // vector where map will be copied to
    const std::vector<Item> * vec_items = knapsack->get_items(); // all items
    std::vector<int> vec_final_items(knapsack->get_items_count());
    int most_expensive_item_cost = -1, most_expensive_item_id;
    int final_cost = 0, final_weight = 0;

    // initialize the array of final items in the knapsack
    for(auto & it : vec_final_items)
        it = 0;

    // insert items into map
    for(int item_id = 0; item_id < vec_items->size(); ++item_id) {
        map_cost_weight[(*vec_items)[item_id].m_cost / (*vec_items)[item_id].m_weight] = item_id;

        // update the most expensive item
        if((*vec_items)[item_id].m_cost > most_expensive_item_cost) {
            most_expensive_item_cost = (*vec_items)[item_id].m_cost;
            most_expensive_item_id = item_id;
        }
    }

    // push items from map to vector in sorted order
    for(auto it = map_cost_weight.begin(); it != map_cost_weight.end(); ++it) {
        // for extended heuristics don't add the most expensive item to the vector
        //if(m_heuristics_type == Extended && it->second == most_expensive_item_id)
        //    continue;
        
        vec_cost_weight.push_back(it->second);
    }

    // calculate sum of sorted items in the vector as long as the weight of the knapsack allows so
    for(int i = vec_cost_weight.size() - 1, item_id; i >= 0; --i) {
        item_id = vec_cost_weight[i];

        if(final_weight + (*vec_items)[item_id].m_weight > knapsack->get_max_weight())
            continue;

        final_cost += (*vec_items)[item_id].m_cost;
        final_weight += (*vec_items)[item_id].m_weight;
        vec_final_items[item_id] = 1;
    }

    // for the extended heuristics, compare the obaitened result with the most expensive item
    if(m_heuristics_type == Extended && (most_expensive_item_cost > final_cost ||
        (most_expensive_item_cost == final_cost && (*vec_items)[most_expensive_item_id].m_weight < final_weight))
    ) {
        final_cost = most_expensive_item_cost;
        final_weight = (*vec_items)[most_expensive_item_id].m_weight;

        for(auto & it : vec_final_items)
            it = 0;

        vec_final_items[most_expensive_item_id] = 1;
    }

    // set the results
    m_exec_results.m_highest_cost = final_cost;
    m_exec_results.m_total_weight = final_weight;

    for(int item_id = 0; item_id < vec_final_items.size(); ++item_id) {
        if(vec_final_items[item_id])
            m_exec_results.m_set_item_ids.insert(item_id);
    }
}
