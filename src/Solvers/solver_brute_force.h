#ifndef SOLVER_BRUTE_FORCE_H
#define SOLVER_BRUTE_FORCE_H

#include <set>
#include "solver_interface.h"

class SolverBruteForce : public SolverInterface {
public: // members
    SolverBruteForce();
    ~SolverBruteForce() = default;

private: // members
    void run(const KnapSack * knapsack, const SAT * sat) override;
    void DFS(const KnapSack * knapsack, std::set<int> set_ids,
        int current_item_id, int current_weight, int current_cost
    );
};

#endif
