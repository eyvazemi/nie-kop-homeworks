#include "solver_simulated_annealing_interface.h"
#include <climits>
#include <limits>
#include <random>

static constexpr int INITIAL_TEMPERATURE = 1;

SolverSimulatedAnnealingInterface::SolverSimulatedAnnealingInterface(
    const std::string & solver_name, float final_temperature,
    float cooling_alpha, float heating_alpha, float inner_loop_to_items_ratio
) : SolverInterface(solver_name),
    m_initial_temperature(INITIAL_TEMPERATURE), m_final_temperature(final_temperature),
    m_cooling_alpha(cooling_alpha), m_heating_alpha(heating_alpha),
    m_inner_loop_to_items_ratio(inner_loop_to_items_ratio) {}

SolverSimulatedAnnealingInterface::~SolverSimulatedAnnealingInterface() = default;

void SolverSimulatedAnnealingInterface::pre_calculation_stage(const KnapSack * knapsack, const SAT * sat) {
    // Calculate initial temperature here via reverse Simulated Annealing, 
    // i.e. instead of cooling down, we increase the temperature.
    run_simulated_annealing(knapsack, sat, get_num_items(knapsack, sat), TemperatureModificationType::Heating);

    // todo:_ remove this
    std::cout << "-- Initial temperature: " << m_initial_temperature << " --" << std::endl;
}

void SolverSimulatedAnnealingInterface::run(const KnapSack * knapsack, const SAT * sat) {
    // Run actual Simulated Annealing, i.e. initial temperature was already found.
    run_simulated_annealing(knapsack, sat, get_num_items(knapsack, sat), TemperatureModificationType::Cooling);
}

void SolverSimulatedAnnealingInterface::post_calculation_stage(const KnapSack * knapsack, const SAT * sat) {
    // Reset the initial temperature.
    m_initial_temperature = INITIAL_TEMPERATURE;
}

void SolverSimulatedAnnealingInterface::run_simulated_annealing(
    const KnapSack * knapsack, const SAT * sat, int num_items,
    const TemperatureModificationType temperature_modification_type
) {
    float temperature = m_initial_temperature;
    int inner_loop_length = num_items * m_inner_loop_to_items_ratio;
    
    set_initial_state(knapsack, sat);

    unsigned iterations_count = 0;

    // In case of heating temperature (when we want to find an initial temperature),
    // we heat until the maximum value of the `float`, but in case of cooling
    // (actually running Simulated Annealing with already found initial temperature)
    // we check for the convergence (freeze temperature).
    while((temperature_modification_type == TemperatureModificationType::Heating &&
        temperature < std::numeric_limits<float>::max()) ||
        (temperature_modification_type == TemperatureModificationType::Cooling &&
        temperature > m_final_temperature)
    ) {
        // Number of times the Simulated Annealing got out of the local minimum.
        // Used to calculate the initial temperature by running 
        // Simulated Annealing multiple times, until value of acceptance rate is close to 50%.
        int count_accepted = 0;

        // Number of times the Simulated Annealing didn't get out of local minimum.
        // This member will be used to calculate the acceptance percentage.
        int count_not_accepted = 0;

        for(int i = 0; i < inner_loop_length; ++i) {
            generate_new_state(knapsack, sat);

            const float cost_change = calculate_cost_change(knapsack, sat);

            if(cost_change < 0) {
                set_new_state(knapsack, sat);
                continue;
            }
            
            if(accept(temperature, cost_change)) {
                set_new_state(knapsack, sat);
                ++count_accepted;
            } else {
                ++count_not_accepted;
            }

            // Write the current cost to the execution results.
            m_exec_results.m_costs.push_back(get_current_cost());

            ++iterations_count;
        }

        if(temperature_modification_type == TemperatureModificationType::Cooling) {
            temperature = temperature * m_cooling_alpha;
        } else {
            // As we are starting from the lower temperature and heating up and the higher is
            // the temperature, the higher is the percentage of accepting the given state,
            // the moment a given temperature gives us higher acceptance rate than 50%,
            // we take this temperature as an initial temperature and break from the loop.
            const int total_accept_calls = count_accepted + count_not_accepted;
            int ratio;

            // In case if all the cost change was always negative, thus the state was always changed and 
            // `accept()` was never called or it was called only once, take this temperature as an initial one.
            if(total_accept_calls <= 1) {
                ratio = 51;
            } else {
                ratio = (int) (((float) count_accepted / (float) total_accept_calls) * 100);
            }
            
            if(ratio >= 50) {
                m_initial_temperature = temperature;
                break;
            }

            // In case of heating up the temperature, we simply multiply it by 2.
            temperature *= m_heating_alpha;
        }
    }
}

float SolverSimulatedAnnealingInterface::generate_random_number(float range_start, float range_end) {
    // We use uniform real distribution for generating random numbers.
    std::random_device random_dev;
    std::mt19937 generator(random_dev());
    std::uniform_real_distribution<float> dist(range_start, range_end);
    return (float) dist(generator);
}

bool SolverSimulatedAnnealingInterface::accept(float temperature, float cost_change) {
    float random_number = SolverSimulatedAnnealingInterface::generate_random_number(0.f, 1.f);
    const float eular_constant = (float) std::exp(1.0);

    if(random_number < powf(eular_constant, -(cost_change / temperature))) {
        return true;
    }

    return false;
}
