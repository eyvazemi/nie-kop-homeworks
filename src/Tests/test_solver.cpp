#ifndef TEST_SOLVER_CPP
#define TEST_SOLVER_CPP

#include "../Common/Structs/knapsack.h"
#include "../Common/Structs/enum_solvers.h"
#include "../Common/FileParsers/file_parser_decision.h"
#include "../Common/FileParsers/file_parser_optimisation.h"
#include "../Common/FileParsers/file_sat_parser.h"
#include "../Solvers/solver_brute_force.h"
#include "../Solvers/solver_branch_bound.h"
#include "../Solvers/solver_heuristics.h"
#include "../Solvers/solver_dynamic_programming_weight.h"
#include "../Solvers/solver_dynamic_programming_cost.h"
#include "../Solvers/solver_dynamic_programming_cost_fptas.h"
#include "../Solvers/solver_simulated_annealing_knapsack.h"
#include "../Solvers/solver_simulated_annealing_sat.h"
#include <vector>
#include <map>

// todo:_ for FPTAS maximum error should be already given, thus there is no need to calculate it

class TestSolver {
public:
    TestSolver() = default;
    ~TestSolver() = default;

    /**
     * @brief Tests file parser class.
     * 
     * @return True, in case of successfull test, false otherwise.
     */
    bool run(const std::string & file_name_inst, const std::string & file_name_sol,
        EnumSolvers solver_type, EnumProblemType problem_type,
        float fptas_max_error, int range_start, int range_end,
        float final_temperature = -1, float cooling_alpha = -1,
        float heating_alpha = -1, float inner_loop_to_items_ratio = -1
    ) {
        m_maximum_error = -1;
        m_maximum_performance_guarantee = -1;
        m_maximum_complexity = -1;
        m_maximum_time = -1;

        // create solver and parser
        SolverInterface * solver;
        FileParser * file_parser;
        
        if(solver_type == EnumSolvers::BruteForce)
            solver = new SolverBruteForce();    
        else if(solver_type == EnumSolvers::BranchBound)
            solver = new SolverBranchBound();
        else if(solver_type == EnumSolvers::Heuristics)
            solver = new SolverHeuristics(Simple);
        else if(solver_type == EnumSolvers::ExtendedHeuristics)
            solver = new SolverHeuristics(Extended);
        else if(solver_type == EnumSolvers::DynamicProgrammingWeight)
            solver = new SolverDynamicProgrammingWeight();
        else if(solver_type == EnumSolvers::DynamicProgrammingCost)
            solver = new SolverDynamicProgrammingCost();
        else if(solver_type == EnumSolvers::DynamicProgrammingCostFPTAS)
            solver = new SolverDynamicProgrammingCostFPTAS(fptas_max_error);
        else if(solver_type == EnumSolvers::SimulatedAnnealingKnapsack) {
            solver = new SolverSimulatedAnnealingKnapsack(
                final_temperature, cooling_alpha, heating_alpha, inner_loop_to_items_ratio
            );
        }

        if(problem_type == EnumProblemType::Decision)
            file_parser = new FileParserDecision();
        else if(problem_type == EnumProblemType::Optimisation ||
            problem_type == EnumProblemType::FPTAS || problem_type == EnumProblemType::Experimental ||
            problem_type == EnumProblemType::SimulatedAnnealingKnapsack
        ) {
            file_parser = new FileParserOptimisation();
        }

        // parse the file and get knapsacks
        std::vector<KnapSack *> vec_knapsacks = file_parser->parse(
            file_name_inst, file_name_sol, range_start, range_end
        );
        std::map<int, int> map_complexities;
        double current_error, current_performance_guarantee;
        double sum_error = 0, sum_performance_guarantee = 0;
        double sum_complexity = 0, sum_time = 0;

        for(int i = 0; i < vec_knapsacks.size(); i++) {
            // check if knapsack is empty
            if(!(vec_knapsacks[i]->get_items_count()))
                continue;

            // solve the knapsack problem
            solver->solve(vec_knapsacks[i], nullptr);
            const ExecResults * solver_exec_results = solver->get_exec_results();

            // compare solution to the reference one
            std::vector<int> vec_solution;
            int reference_weight = 0;

            for(int item_id = 0; item_id < (vec_knapsacks[i])->get_items_count(); item_id++) {
                if((solver_exec_results->m_set_item_ids).find(item_id) != (solver_exec_results->m_set_item_ids).end())
                    vec_solution.push_back(1);
                else
                    vec_solution.push_back(0);
            }

            for(int j = 0; j < vec_solution.size(); j++) {
                reference_weight += (*((vec_knapsacks[i])->get_solution()))[j] *
                    ((*((vec_knapsacks[i])->get_items()))[j]).m_weight;
            }

            if((problem_type == EnumProblemType::Decision || problem_type == EnumProblemType::Optimisation) &&
                ((vec_knapsacks[i])->get_solution_cost() != solver_exec_results->m_highest_cost ||
                reference_weight < solver_exec_results->m_total_weight)
            ) {
                std::cout << "Reference solution : Solver's solution" << std::endl;
                for(int j = 0; j < vec_solution.size(); j++) {
                    std::cout << (*((vec_knapsacks[i])->get_solution()))[j] << " : " <<
                        vec_solution[j] << std::endl;
                }

                std::cout << "Reference cost : Solver's cost\n" << (vec_knapsacks[i])->get_solution_cost() << " : "
                    << solver_exec_results->m_highest_cost << std::endl;

                std::cout << "Reference weight : Solver's weight\n" << reference_weight << " : "
                    << solver_exec_results->m_total_weight << std::endl;

                std::cout << "TestSolver: Incorrectly solved with knapsack " << i + range_start + 1 << std::endl;

                // free knapsacks
                for(int i = 0; i < vec_knapsacks.size(); i++)
                    delete vec_knapsacks[i];

                // free solver
                delete solver;

                return false;
            }

            // write the occurence of this complexity into the map of complexities
            if(map_complexities.find(solver_exec_results->m_complexity) != map_complexities.end())
                ++(map_complexities[solver_exec_results->m_complexity]);
            else
                map_complexities[solver_exec_results->m_complexity] = 1;

            // accumulate sums of error and performance guarantee
            current_error = (double) ((vec_knapsacks[i])->get_solution_cost() - solver_exec_results->m_highest_cost) /
                (vec_knapsacks[i])->get_solution_cost();

            current_performance_guarantee = (double) (vec_knapsacks[i])->get_solution_cost() /
                solver_exec_results->m_highest_cost;

            sum_error += current_error;
            sum_performance_guarantee += current_performance_guarantee;

            // accumulate sums of complexity and average
            sum_complexity += solver_exec_results->m_complexity;
            sum_time += solver_exec_results->m_elapsed_time;

            // update maximum error
            if(current_error > m_maximum_error) {
                m_maximum_error = current_error;
            }

            // update maximum performance guarantee
            if(current_performance_guarantee > m_maximum_performance_guarantee)
                m_maximum_performance_guarantee = current_performance_guarantee;

            // update maximum complexity
            if(solver_exec_results->m_complexity > m_maximum_complexity)
                m_maximum_complexity = solver_exec_results->m_complexity;

            // update maximum time of execution
            if(solver_exec_results->m_elapsed_time > m_maximum_time)
                m_maximum_time = solver_exec_results->m_elapsed_time;

            // In case of the Simulated Annealing for Knapsack problem,
            // save the cost in each iteration of the last knapsack instance.
            if(problem_type == EnumProblemType::SimulatedAnnealingKnapsack &&
                i == vec_knapsacks.size() - 1
            ) {
                m_simulated_annealing_costs.clear();

                for(int i = 0; i < solver_exec_results->m_costs.size(); ++i) {
                    m_simulated_annealing_costs.push_back((int) solver_exec_results->m_costs[i]);
                }
            }
        }

        // write the obtained map of complexities into the member
        m_map_complexities = map_complexities;

        // calculate average complexity and average time
        m_average_error = (double) sum_error / vec_knapsacks.size();
        m_average_performance_guarantee = (double) sum_performance_guarantee / vec_knapsacks.size();

        // calculate average complexity and average time
        m_average_complexity = (int) (sum_complexity / vec_knapsacks.size());
        m_average_time = (int) (sum_time / vec_knapsacks.size());

        // get number of chunks, which will only be valid for FPTAS algorithm
        m_fptas_num_chunks = solver->get_exec_results()->m_fptas_num_chunks;

        // get the initial temperature in Simulated Annealing
        m_simulated_annealing_initial_temperature =
            solver->get_exec_results()->m_simulated_annealing_initial_temperature;

        // free knapsacks
        for(int i = 0; i < vec_knapsacks.size(); i++)
            delete vec_knapsacks[i];

        // free solver and file parser
        delete solver;
        delete file_parser;

        return true;
    }

    void run_sat(const std::string & samples_dir, const std::string & file_name_inst,
        const std::string & file_name_sol, float final_temperature = -1,
        float cooling_alpha = -1, float heating_alpha = -1,
        float inner_loop_to_items_ratio = -1
    ) {
        m_maximum_error = -1;
        m_maximum_performance_guarantee = -1;
        m_maximum_complexity = -1;
        m_maximum_time = -1;

        // Parse an input file.
        SAT * sat = new SAT(FileSATParser::parse(samples_dir + "/" + file_name_inst, file_name_sol));
        
        // Create a solver and solve the parsed SAT problem.
        SolverInterface * solver = new SolverSimulatedAnnealingSAT(
            final_temperature, cooling_alpha, heating_alpha, inner_loop_to_items_ratio
        );

        solver->solve(nullptr, sat);
        const ExecResults * solver_exec_results = solver->get_exec_results();

        // todo:_ remove this
        std::cout << "Highest cost: " << solver_exec_results->m_highest_cost << 
            ", number of unsatisfied clauses: " << solver_exec_results->m_num_unsat_clauses << std::endl;

        std::cout << "SAT variables: ";
        
        for(const int variable_id : solver_exec_results->m_set_item_ids) {
            std::cout << variable_id << " ";
        }

        std::cout << std::endl;

        // Calculate the error.
        m_sat_error = solver_exec_results->m_highest_cost > sat->get_solution_weight() ? 0 :
            (double) (sat->get_solution_weight() - solver_exec_results->m_highest_cost) /
            sat->get_solution_weight();

        m_sat_execution_time = solver_exec_results->m_elapsed_time;

        // Write the costs.
        for(int i = 0; i < solver_exec_results->m_costs.size(); ++i) {
            m_simulated_annealing_costs.push_back((int) solver_exec_results->m_costs[i]);
        }

        // Write the number of SAT variables.
        m_sat_num_variables = sat->get_num_variables();

        // Free the allocated memory for the SAT instance and the solver.
        delete sat;
        delete solver;
    }

    /**
     * @brief Used in experimental evaluation, when based on a given solver's results,
     * solution file is created, which will be used with other solvers for comparison of results.
     * 
     * @return The name of the solution file that was created.
    */
    static std::string create_solution_file(const std::string & file_name_inst,
        const std::string & samples_dir, EnumSolvers solver_type,
        int range_start, int range_end
    ) {
        // Create the solver.
        SolverInterface * solver;
        
        if(solver_type == EnumSolvers::BruteForce)
            solver = new SolverBruteForce();    
        else if(solver_type == EnumSolvers::BranchBound)
            solver = new SolverBranchBound();
        else if(solver_type == EnumSolvers::Heuristics)
            solver = new SolverHeuristics(Simple);
        else if(solver_type == EnumSolvers::ExtendedHeuristics)
            solver = new SolverHeuristics(Extended);
        else if(solver_type == EnumSolvers::DynamicProgrammingWeight)
            solver = new SolverDynamicProgrammingWeight();
        else if(solver_type == EnumSolvers::DynamicProgrammingCost)
            solver = new SolverDynamicProgrammingCost();

        // Parse the file and get knapsacks.
        // Solution file is passed as an empty string, as we are generating
        // this file in this method.
        FileParser * file_parser = new FileParserOptimisation();
        std::vector<KnapSack *> vec_knapsacks = file_parser->parse(
            samples_dir + "/" + file_name_inst, "", range_start, range_end
        );

        // String that will store the solutions of all knapsacks that will eventually
        // be written into the solution file.
        std::string str_file_contents;

        for(int i = 0; i < vec_knapsacks.size(); i++) {
            // Check if knapsack is empty. It is done with real number of items,
            // as this is the number of items that was given to us, before we threw them 
            // off during parsing, because of their weight being higher than the maximum one.
            if(vec_knapsacks[i]->get_real_items_count() == 0) {
                continue;
            }

            // Fill the solution with zeros, in case all the items' weights were higher than the maximum one,
            // thus they got thrown out during parsing.
            if(vec_knapsacks[i]->get_items_count() == 0) {
                str_file_contents += std::to_string(i + 1) + " " + std::to_string(vec_knapsacks[i]->get_real_items_count()) +
                " 0";

                for(int item_id = 0; item_id < vec_knapsacks[i]->get_real_items_count(); ++item_id) {
                    str_file_contents += " 0";
                }
            } else {
                // Solve the knapsack problem.
                solver->solve(vec_knapsacks[i], nullptr);
                const ExecResults * solver_exec_results = solver->get_exec_results();

                // Write the line, number of items, total cost, and items into the solution string.
                str_file_contents += std::to_string(i + 1) + " " + std::to_string(vec_knapsacks[i]->get_items_count()) +
                    " " + std::to_string(solver_exec_results->m_highest_cost);
                
                for(int item_id = 0; item_id < (vec_knapsacks[i])->get_items_count(); item_id++) {
                    if((solver_exec_results->m_set_item_ids).find(item_id) !=
                        (solver_exec_results->m_set_item_ids).end()
                    ) {
                        str_file_contents += " 1";
                    } else {
                        str_file_contents += " 0";
                    }
                }
            }

            str_file_contents += "\n";
        }

        // Create a solution file and write into it.
        std::ofstream file_solution;
        const std::string file_name_solution = samples_dir + "/solution_" + file_name_inst;

        file_solution.open(file_name_solution, std::ios::trunc);
        file_solution.write(str_file_contents.c_str(), str_file_contents.length());

        // Free solver and file parser and close the solution file.
        file_solution.close();
        delete solver;
        delete file_parser;

        return file_name_solution;
    }

    const std::map<int, int> * get_vec_complexities() {
        return &m_map_complexities;
    }

    int get_average_complexity() {
        return m_average_complexity;
    }

    int get_maximum_complexity() {
        return m_maximum_complexity;
    }

    int get_average_time() {
        return m_average_time;
    }

    int get_maximum_time() {
        return m_maximum_time;
    }

    double get_average_error() {
        return m_average_error;
    }

    double get_maximum_error() {
        return m_maximum_error;
    }

    double get_average_performance_guarantee() {
        return m_average_performance_guarantee;
    }

    double get_maximum_performance_guarantee() {
        return m_maximum_performance_guarantee;
    }

    int get_fptas_num_chunks() {
        return m_fptas_num_chunks;
    }

    float get_simulated_annealing_initial_temperature() const {
        return m_simulated_annealing_initial_temperature;
    }

    std::vector<float> get_simulated_annealing_costs() const {
        return m_simulated_annealing_costs;
    }

    double get_sat_error() const {
        return m_sat_error;
    }

    int get_sat_execution_time() const {
        return m_sat_execution_time;
    }

    int get_sat_num_variables() const {
        return m_sat_num_variables;
    }

private: // members
    std::vector<float> m_simulated_annealing_costs;
    std::map<int, int> m_map_complexities; // map with complexities and their occurences
    double m_average_error;
    double m_maximum_error;
    double m_average_performance_guarantee;
    double m_maximum_performance_guarantee;
    float m_simulated_annealing_initial_temperature;
    int m_fptas_num_chunks;
    int m_average_time;
    int m_maximum_time;
    int m_average_complexity;
    int m_maximum_complexity;

    // For SAT.
    double m_sat_error;
    int m_sat_execution_time;
    int m_sat_num_variables;
};

#endif
