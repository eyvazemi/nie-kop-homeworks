#ifndef TEST_FILE_PARSER_CPP
#define TEST_FILE_PARSER_CPP

#include <vector>
#include "../Common/Structs/knapsack.h"
#include "../Common/FileParsers/file_parser_decision.h"

class TestFileParser {
public:
    TestFileParser() = default;
    ~TestFileParser() = default;

    /**
     * @brief Tests file parser class.
     * 
     * @return True, in case of successfull test, false otherwise.
     */
    static bool run() {
        // reference data
        std::vector<std::vector<int>> m_reference_data_inst = {
            {-1, 4, 46, 324, 36, 3, 43, 1129, 202, 94, 149, 2084},
            {-2, 4, 131, 7, 65, 540, 74, 1463, 30, 1503, 61, 151},
            {-3, 4, 22, 0, 214, 1142, 68, 2253, 238, 1541, 245, 1413},
            {-4, 4, 92, 63, 23, 70, 194, 1367, 129, 1649, 118, 1826},
            {-5, 4, 243, 4457, 122, 1525, 23, 1003, 136, 221, 200, 1755}
        };

        std::vector<std::vector<int>> m_reference_data_sol = {
            {1, 4, 1129, 0, 1, 0, 0},
            {2, 4, 2966, 0, 1, 1, 0},
            {3, 4, 0, 0, 0, 0, 0},
            {4, 4, 70, 1, 0, 0, 0},
            {5, 4, 2758, 0, 1, 0, 1}
        };

        // parse the file and get knapsacks
        FileParserDecision file_parser_decision;
        std::vector<KnapSack *> vec_knapsacks = file_parser_decision.parse(
                                                    "./Samples/NR/NR4_inst.dat",
                                                    "./Samples/NR/NK4_sol.dat", 0, 4
                                                );

        // check instance file
        for(int i = 0; i < 4; ++i) {
            const std::vector<Item> * ptr_vec_items = (vec_knapsacks[i])->get_items();
            
            if((m_reference_data_inst[i]).size() != (vec_knapsacks[i])->get_items_count() * 2 + 4 ||
                (vec_knapsacks[i])->get_items_count() != m_reference_data_inst[i][1] ||
                (vec_knapsacks[i])->get_max_weight() != m_reference_data_inst[i][2] ||
                (vec_knapsacks[i])->get_target_cost() != m_reference_data_inst[i][3]
            ) {
                std::cout << "TestFileParser: Incorrect file header in instance file." << std::endl;
                return false;
            }

            for(int j = 4, k = 0; j < (m_reference_data_inst[i]).size(); j += 2, k++) {
                if(((*ptr_vec_items)[k]).m_weight <= vec_knapsacks[i]->get_max_weight() &&
                    (((*ptr_vec_items)[k]).m_weight != m_reference_data_inst[i][j] ||
                    ((*ptr_vec_items)[k]).m_cost != m_reference_data_inst[i][j + 1])
                ) {
                    std::cout << "TestFileParser: Incorrect items in instance file." << std::endl;
                    return false;
                }
            }
        }

        // check solution file
        for(int i = 0; i < 4; ++i) {
            const std::vector<int> * ptr_vec_solution = (vec_knapsacks[i])->get_solution();
            
            if((m_reference_data_sol[i]).size() != (vec_knapsacks[i])->get_items_count() + 3 ||
                (vec_knapsacks[i])->get_solution_cost() != m_reference_data_sol[i][2]
            ) {
                std::cout << "TestFileParser: Incorrect file header in solution file." << std::endl;
                return false;
            }

            for(int j = 3; j < (m_reference_data_sol[i]).size(); ++j) {
                if((*ptr_vec_solution)[j - 3] != m_reference_data_sol[i][j]) {
                    std::cout << "TestFileParser: Incorrect solution in solution file." << std::endl;
                    return false;
                }
            }
        }

        // free allocated memory for knapsacks
        for(int i = 0; i < vec_knapsacks.size(); ++i)
            delete vec_knapsacks[i];

        return true;
    }
};

#endif
