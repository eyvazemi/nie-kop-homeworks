\documentclass[12pt]{book}

\usepackage[acronym]{glossaries}
\usepackage{hyperref}
\usepackage{listings}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage{systeme}
\usepackage{algorithm,algpseudocode}
\usepackage{afterpage}
\usepackage[a4paper, total={6in, 10in}]{geometry}
\usepackage{scrextend}
\usepackage{tcolorbox}
\usepackage{polynom}
\usepackage{amsthm}

\pagestyle{plain}

\newcommand{\example}[1]{
    \begin{addmargin}[1em]{1em}
    \begin{tcolorbox}
    #1
    \end{tcolorbox}
    \end{addmargin}
}

\newcommand{\mychapter}[1]{
    \renewcommand\chaptername{}
    \renewcommand\thechapter{}
    \chapter{#1}
    %\renewcommand\thechapter{#1}
}

\newcommand{\mysection}[1]{
    \renewcommand\thesection{}
    \section{#1}
    %\renewcommand\thechapter{#1}
}

\newcommand{\mysubsection}[1]{
    \renewcommand\thesubsection{}
    \subsection{#1}
    %\renewcommand\thechapter{#1}
}

\newcommand{\code}[1]{
    {\color{blue}\texttt{#1}}
}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\begin{document}

\mychapter{Description of the Simulated Annealing for Knapsack problem}
Listing below illustrates the Simulated Annealing algorithm in C++.

\begin{lstlisting}[
    language=C++,
    caption=Sumulated Annealing,
    columns=flexible,
    basicstyle={\footnotesize\ttfamily},
    numbers=left,
    numberstyle=\tiny\color{gray},
    keywordstyle=\color{blue},
    commentstyle=\color{dkgreen},
    stringstyle=\color{mauve},
    tabsize=4
]
float temperature = m_initial_temperature;
int inner_loop_length = num_items * m_inner_loop_to_items_ratio;

set_initial_state(knapsack);

unsigned iterations_count = 0;

// In case of heating temperature (when we want to find an initial temperature),
// we heat until the maximum value of the `float`, but in case of cooling
// (actually running Simulated Annealing with already found initial temperature)
// we check for the convergence (freeze temperature).
while((temperature_modification_type == TemperatureModificationType::Heating &&
    temperature < std::numeric_limits<float>::max()) ||
    (temperature_modification_type == TemperatureModificationType::Cooling &&
    temperature > m_final_temperature)
) {
    // Number of times the Simulated Annealing got out of the local minimum.
    // Used to calculate the initial temperature by running 
    // Simulated Annealing multiple times, until value of acceptance rate is close to 50%.
    int count_accepted = 0;

    // Number of times the Simulated Annealing didn't get out of local minimum.
    // This member will be used to calculate the acceptance percentage.
    int count_not_accepted = 0;

    for(int i = 0; i < inner_loop_length; ++i) {
        generate_new_state(knapsack);

        const float cost_change = calculate_cost_change(knapsack);

        if(cost_change < 0) {
            set_new_state(knapsack);
            continue;
        }
        
        if(accept(temperature, cost_change)) {
            set_new_state(knapsack);
            ++count_accepted;
        } else {
            ++count_not_accepted;
        }

        // Write the current cost to the execution results.
        m_exec_results.m_costs.push_back(get_current_cost());

        ++iterations_count;
    }

    if(temperature_modification_type == TemperatureModificationType::Cooling) {
        temperature = temperature * m_cooling_alpha;
    } else {
        // As we are starting from the lower temperature and heating up and the higher is
        // the temperature, the higher is the percentage of accepting the given state,
        // the moment a given temperature gives us higher acceptance rate than 50%,
        // we take this temperature as an initial temperature and break from the loop.
        const int total_accept_calls = count_accepted + count_not_accepted;
        int ratio;

        // In case if all the cost change was always negative, thus the state was always 
        // changed and `accept()` was never called or it was called only once, take this 
        // temperature as an initial one.
        if(total_accept_calls <= 1) {
            ratio = 51;
        } else {
            ratio = (int) (((float) count_accepted / (float) total_accept_calls) * 100);
        }
        
        if(ratio >= 50) {
            m_initial_temperature = temperature;
            break;
        }

        // In case of heating up the temperature, we simply multiply it by 2.
        temperature *= m_heating_alpha;
    }
}
\end{lstlisting}

Simulated Annealing consists of these main parts:
\begin{itemize}
    \item Setting the initial temperature and an initial state.
    \item Outer loop that runs until the temperature is greater than or equal to the final temperature.
    \item In the outer loop, we have an inner loop that runs as many times as we set, in which
        \begin{itemize}
            \item We generate a random state via \code{generate\_new\_state()}.
            \item Calculate the cost change via \code{calculate\_cost\_change()} method.
            \item In case if the cost change is less than 0, we set the generated state as the new state.
            \item In case if the cost change is not less than 0, we check it with \code{accept()} method.
        \end{itemize}

    \item Modify the temperature for the next iteration.
\end{itemize}

Simulated Annealing runs twice:
\begin{enumerate}
    \item When we want to find the initial temperature via setting it to 1, then we calculate the ratio of the number of times the
        \code{accept()} method returned \code{true} and the number of times it ran. If the ratio is above 0.5, i.e. 50\%, then we 
        can stop the Simulated Annealing, as we found an initial temperature that is good enough to be able to escape the local optimum.

    \item When we actually want to find the optimum or close to the optimum cost. We choose the initial temperature from the first run and 
        cool it down with the given alpha value, until the final temperature is reached.
\end{enumerate}

Important functions of Simulated Annealing algorithm:
\begin{itemize}
    \item \code{generate\_new\_state()}: randomly picks items and puts them into the knapsack and in case of knapsack overloading, it randomly 
        pulls items out from the knapsack, until the knapsack is not overloaded anymore.

    \item \code{calculate\_cost\_change()}: returns the difference between the previous cost with previous set of items in the knapsack and the new 
        cost with new set of items in the knapsack. Knapsack problem is the maximization problem thus, we subtract the new from the old and not the 
        vice versa, as Simulated Annealing is portrayed with with minimization problem.

    \item \code{accept()}: return true if the generated random number is less than $e^{-\frac{\Delta{C}}{t}}$, where $\Delta{C}$ is the cost change 
        and $t$ is the temperature.
\end{itemize}


\mychapter{Experiments}
\mysection{Description of experiments}
\mysubsection{Types of intances}
The instances used are the default ones, i.e. \emph{NK}, \emph{ZKC}, and \emph{ZKW} instances.

\mysubsection{Measurements and parameters}
We will be doing these measurements:
\begin{itemize}
    \item \textbf{Time of execution}: the amount of time it takes an algorithm to finish solving the problem. For Simulated Annealing
        we will take the time of execution after the initial temperature was found, as the first run of Simulated Annealing is done 
        to just find the most appropriate initial temperature and, as we could have given an initial temperature manually, as a 
        parameter, we don't include it to the time of execution.

    \item \textbf{Results quality}: measurement of relative error compared to the optimum, i.e. how far are the obtained results from 
        the optimum. In our case, we will be dealing with already provided instances that have a solution with the optimum cost, thus 
        we will compare our results with provided optimum.

    \item \textbf{Convergence curve}: graph with cost convergence and number of iterations for one instance of NK 40 type. This is needed
        to analyse how the given parameter of the Simulated Annealing influences the cost in the given iteration.
\end{itemize}

Along with \emph{Simulated Annealing} solver, we will also run tests of \emph{Dynamic Programming by Weight}, so that we have another 
solver with which we could compare the time of exection of \emph{Simulated Annealing}.

Parameters we will be modifying to test their impact on measurements and their default values are
\begin{itemize}
    \item \textbf{Final temperature}: the temperature at which, the Simulated Annealing stops and doesn't cool down any further.
        The default value is 0.01 .

    \item \textbf{Cooling alpha}: alpha value that will be used for cooling. The default value is 0.99 .
    \item \textbf{Heating alpha}: alpha value that will be used for heating, when we run the Simulated Annealing the first time to find 
        the appropriate initial temperature. The default value is 1.1 .

    \item \textbf{Inner loop to items ratio}: instead of giving a fixed length of the inner loop, we will pass a ratio of the expected 
        length of the inner loop to the number of items in the given instance. This way, the length of the inner loop will be calculated 
        automatically in the algorithm, simply by multiplying the number of items with the value of this parameter. The default value is 10.
\end{itemize}

\mysection{Actual experiments}
\mysubsection{Final temperature}
These values were chosen as values of the final temperature: $\{0.001, 0.005, 0.05, 0.1\}$.

Figures below illustrate the average and maximum errors for $NK$, $ZKC$, and $ZKW$ instances. As it can be seen, for $NK$ and $ZKC$ instances, 
it is very difficult to tell if there is any correlation between final temperature and the error, but for $ZKW$ instances, it is quite obvious 
that error doesn't depend on the final temperature.

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Final_temperature_average_error_NK_30.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Final_temperature_maximum_error_NK_30.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Final_temperature_average_error_NK_35.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Final_temperature_maximum_error_NK_35.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Final_temperature_average_error_NK_40.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Final_temperature_maximum_error_NK_40.png}\hfill
\label{fig:parameter_Final_temperature_error_NK}
\end{figure}

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Final_temperature_average_error_ZKC_30.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Final_temperature_maximum_error_ZKC_30.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Final_temperature_average_error_ZKC_35.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Final_temperature_maximum_error_ZKC_35.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Final_temperature_average_error_ZKC_40.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Final_temperature_maximum_error_ZKC_40.png}\hfill
\label{fig:parameter_Final_temperature_error_ZKC}
\end{figure}

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Final_temperature_average_error_ZKW_30.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Final_temperature_maximum_error_ZKW_30.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Final_temperature_average_error_ZKW_35.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Final_temperature_maximum_error_ZKW_35.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Final_temperature_average_error_ZKW_40.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Final_temperature_maximum_error_ZKW_40.png}\hfill
\label{fig:parameter_Final_temperature_error_ZKW}
\end{figure}

\clearpage

Figures below illustrate the average and maximum time for $NK$, $ZKC$, and $ZKW$ instances. And, as it is expected, for all instances,
once the final temperature is incremented, the time of execution decrements, as tjere are less iterations of the Simulated Annealing.

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Final_temperature_average_time_NK_30.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Final_temperature_maximum_time_NK_30.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Final_temperature_average_time_NK_35.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Final_temperature_maximum_time_NK_35.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Final_temperature_average_time_NK_40.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Final_temperature_maximum_time_NK_40.png}\hfill
\label{fig:parameter_Final_temperature_time_NK}
\end{figure}

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Final_temperature_average_time_ZKC_30.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Final_temperature_maximum_time_ZKC_30.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Final_temperature_average_time_ZKC_35.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Final_temperature_maximum_time_ZKC_35.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Final_temperature_average_time_ZKC_40.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Final_temperature_maximum_time_ZKC_40.png}\hfill
\label{fig:parameter_Final_temperature_time_ZKC}
\end{figure}

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Final_temperature_average_time_ZKW_30.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Final_temperature_maximum_time_ZKW_30.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Final_temperature_average_time_ZKW_35.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Final_temperature_maximum_time_ZKW_35.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Final_temperature_average_time_ZKW_40.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Final_temperature_maximum_time_ZKW_40.png}\hfill
\label{fig:parameter_Final_temperature_time_ZKW}
\end{figure}

\clearpage

Figure below illustrates how with lower final temperature, it takes more time to converge with the same maximum cost.

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/simulated_annealing_costs_NK_40_f_0.001000_c_0.990000_h_1.100000_i_10.000000.cs.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/simulated_annealing_costs_NK_40_f_0.005000_c_0.990000_h_1.100000_i_10.000000.cs.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.615\textwidth]{Resources/simulated_annealing_costs_NK_40_f_0.050000_c_0.990000_h_1.100000_i_10.000000.cs.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.625\textwidth]{Resources/simulated_annealing_costs_NK_40_f_0.100000_c_0.990000_h_1.100000_i_10.000000.cs.png}\hfill
\label{fig:convergence_curve_final_temperature_NK}
\end{figure}

\clearpage

\mysubsection{Cooling alpha}
These values were chosen as values of the cooling alpha: $\{0.9, 0.95, 0.99, 0.995\}$.

Figures below illustrate the average and maximum errors for $NK$, $ZKC$, and $ZKW$ instances. As with final temperature, it seems that for $NK$ 
and $ZKC$ instances, it is hard to say if there is any correlation between error and the cooling alpha. But for $ZKW$ instances, there definitely 
is no correlation.

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Cooling_alpha_average_error_NK_30.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Cooling_alpha_maximum_error_NK_30.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Cooling_alpha_average_error_NK_35.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Cooling_alpha_maximum_error_NK_35.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Cooling_alpha_average_error_NK_40.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Cooling_alpha_maximum_error_NK_40.png}\hfill
\label{fig:parameter_Cooling_alpha_error_NK}
\end{figure}

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Cooling_alpha_average_error_ZKC_30.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Cooling_alpha_maximum_error_ZKC_30.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Cooling_alpha_average_error_ZKC_35.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Cooling_alpha_maximum_error_ZKC_35.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Cooling_alpha_average_error_ZKC_40.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Cooling_alpha_maximum_error_ZKC_40.png}\hfill
\label{fig:parameter_Cooling_alpha_error_ZKC}
\end{figure}

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Cooling_alpha_average_error_ZKW_30.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Cooling_alpha_maximum_error_ZKW_30.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Cooling_alpha_average_error_ZKW_35.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Cooling_alpha_maximum_error_ZKW_35.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Cooling_alpha_average_error_ZKW_40.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Cooling_alpha_maximum_error_ZKW_40.png}\hfill
\label{fig:parameter_Cooling_alpha_error_ZKW}
\end{figure}

\clearpage

Figures below illustrate the average and maximum time for $NK$, $ZKC$, and $ZKW$ instances. As it is expected, for all the instances, with the 
increase of the cooling alpha, the time of execution increase, as this means that the cooling happens slower, thus more iterations are happening 
in Simulated Annealing.

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Cooling_alpha_average_time_NK_30.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Cooling_alpha_maximum_time_NK_30.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Cooling_alpha_average_time_NK_35.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Cooling_alpha_maximum_time_NK_35.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Cooling_alpha_average_time_NK_40.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Cooling_alpha_maximum_time_NK_40.png}\hfill
\label{fig:parameter_Cooling_alpha_time_NK}
\end{figure}

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Cooling_alpha_average_time_ZKC_30.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Cooling_alpha_maximum_time_ZKC_30.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Cooling_alpha_average_time_ZKC_35.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Cooling_alpha_maximum_time_ZKC_35.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Cooling_alpha_average_time_ZKC_40.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Cooling_alpha_maximum_time_ZKC_40.png}\hfill
\label{fig:parameter_Cooling_alpha_time_ZKC}
\end{figure}

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Cooling_alpha_average_time_ZKW_30.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Cooling_alpha_maximum_time_ZKW_30.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Cooling_alpha_average_time_ZKW_35.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Cooling_alpha_maximum_time_ZKW_35.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Cooling_alpha_average_time_ZKW_40.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Cooling_alpha_maximum_time_ZKW_40.png}\hfill
\label{fig:parameter_Cooling_alpha_time_ZKW}
\end{figure}

\clearpage

Figure below illustrates how with lower cooling alpha, the algorithm converges faster, which makes sense, as in the case of lower cooling 
alpha, we cool down faster, thus Simulated Annealing runs for less iterations.

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/simulated_annealing_costs_NK_40_f_0.010000_c_0.900000_h_1.100000_i_10.000000.cs.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/simulated_annealing_costs_NK_40_f_0.010000_c_0.950000_h_1.100000_i_10.000000.cs.png}\hfill
\hspace*{3cm}
\includegraphics[width=0.625\textwidth]{Resources/simulated_annealing_costs_NK_40_f_0.010000_c_0.995000_h_1.100000_i_10.000000.cs.png}\hfill
\label{fig:convergence_curve_cooling_alpha_NK}
\end{figure}

\clearpage

\mysubsection{Heating alpha}
These values were chosen as values of the heating alpha: $\{1.01, 1.05, 1.1, 1.5\}$.

Figures below illustrate the average and maximum errors for $NK$, $ZKC$, and $ZKW$ instances. The image here is the same, as in previous 2 
parameters: hard to say for $NK$ and $ZKC$, but $ZKW$ instances definitely don't have any correlation between heating alpha and an error.

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Heating_alpha_average_error_NK_30.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Heating_alpha_maximum_error_NK_30.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Heating_alpha_average_error_NK_35.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Heating_alpha_maximum_error_NK_35.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Heating_alpha_average_error_NK_40.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Heating_alpha_maximum_error_NK_40.png}\hfill
\label{fig:parameter_Heating_alpha_error_NK}
\end{figure}

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Heating_alpha_average_error_ZKC_30.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Heating_alpha_maximum_error_ZKC_30.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Heating_alpha_average_error_ZKC_35.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Heating_alpha_maximum_error_ZKC_35.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Heating_alpha_average_error_ZKC_40.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Heating_alpha_maximum_error_ZKC_40.png}\hfill
\label{fig:parameter_Heating_alpha_error_ZKC}
\end{figure}

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Heating_alpha_average_error_ZKW_30.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Heating_alpha_maximum_error_ZKW_30.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Heating_alpha_average_error_ZKW_35.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Heating_alpha_maximum_error_ZKW_35.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Heating_alpha_average_error_ZKW_40.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Heating_alpha_maximum_error_ZKW_40.png}\hfill
\label{fig:parameter_Heating_alpha_error_ZKW}
\end{figure}

\clearpage

Figures below illustrate the average and maximum time for $NK$, $ZKC$, and $ZKW$ instances. Time of execution, for all instances, doesn't depend 
on the heating alpha.

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Heating_alpha_average_time_NK_30.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Heating_alpha_maximum_time_NK_30.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Heating_alpha_average_time_NK_35.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Heating_alpha_maximum_time_NK_35.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Heating_alpha_average_time_NK_40.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Heating_alpha_maximum_time_NK_40.png}\hfill
\label{fig:parameter_Heating_alpha_time_NK}
\end{figure}

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Heating_alpha_average_time_ZKC_30.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Heating_alpha_maximum_time_ZKC_30.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Heating_alpha_average_time_ZKC_35.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Heating_alpha_maximum_time_ZKC_35.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Heating_alpha_average_time_ZKC_40.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Heating_alpha_maximum_time_ZKC_40.png}\hfill
\label{fig:parameter_Heating_alpha_time_ZKC}
\end{figure}

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Heating_alpha_average_time_ZKW_30.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Heating_alpha_maximum_time_ZKW_30.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Heating_alpha_average_time_ZKW_35.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Heating_alpha_maximum_time_ZKW_35.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Heating_alpha_average_time_ZKW_40.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Heating_alpha_maximum_time_ZKW_40.png}\hfill
\label{fig:parameter_Heating_alpha_time_ZKW}
\end{figure}

\clearpage

Figure below illustrates how with the change of the heating alpha, the number of iterations doesn't change much, thus Simulated Annealing 
converges somewhere in the same time. But, as it can be witnessed, with lower heating alpha, the initial produced cost is much lower than with 
higher values of the heating alpha, this has to do with the fact that, when we are calculating the initial temperature on the first run of the 
Simulated Annealing, lower heating alpha leads to lower initial temperature.

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/simulated_annealing_costs_NK_40_f_0.010000_c_0.990000_h_1.010000_i_10.000000.cs.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/simulated_annealing_costs_NK_40_f_0.010000_c_0.990000_h_1.050000_i_10.000000.cs.png}\hfill
\hspace*{3cm}
\includegraphics[width=0.625\textwidth]{Resources/simulated_annealing_costs_NK_40_f_0.010000_c_0.990000_h_1.500000_i_10.000000.cs.png}\hfill
\label{fig:convergence_curve_heating_alpha_NK}
\end{figure}

\clearpage

\mysubsection{Inner loop to items ratio}
These values were chosen as values of the heating alpha: $\{1, 50\}$. The $X$-axis values on the graphs are the actual values of the 
inner loop length, i.e. inner loop to items ratio multiplied by the number of items.

Figures below illustrate the average and maximum errors for $NK$, $ZKC$, and $ZKW$ instances. For $NK$ instances, error decreases with the 
increase of inner loop length. For $ZKC$ and $ZKW$ instances, though, the error seems to be indifferent to the inner loop length.

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Inner_loop_length_average_error_NK_30.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Inner_loop_length_maximum_error_NK_30.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Inner_loop_length_average_error_NK_35.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Inner_loop_length_maximum_error_NK_35.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Inner_loop_length_average_error_NK_40.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Inner_loop_length_maximum_error_NK_40.png}\hfill
\label{fig:parameter_Inner_loop_length_error_NK}
\end{figure}

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Inner_loop_length_average_error_ZKC_30.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Inner_loop_length_maximum_error_ZKC_30.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Inner_loop_length_average_error_ZKC_35.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Inner_loop_length_maximum_error_ZKC_35.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Inner_loop_length_average_error_ZKC_40.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Inner_loop_length_maximum_error_ZKC_40.png}\hfill
\label{fig:parameter_Inner_loop_length_error_ZKC}
\end{figure}

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Inner_loop_length_average_error_ZKW_30.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Inner_loop_length_maximum_error_ZKW_30.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Inner_loop_length_average_error_ZKW_35.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Inner_loop_length_maximum_error_ZKW_35.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Inner_loop_length_average_error_ZKW_40.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Inner_loop_length_maximum_error_ZKW_40.png}\hfill
\label{fig:parameter_Inner_loop_length_error_ZKW}
\end{figure}

\clearpage

Figures below illustrate the average and maximum time for $NK$, $ZKC$, and $ZKW$ instances. Obviously, for all instances, the time of execution 
increases with the increase of inner loop length.

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Inner_loop_length_average_time_NK_30.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Inner_loop_length_maximum_time_NK_30.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Inner_loop_length_average_time_NK_35.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Inner_loop_length_maximum_time_NK_35.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Inner_loop_length_average_time_NK_40.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Inner_loop_length_maximum_time_NK_40.png}\hfill
\label{fig:parameter_Inner_loop_length_time_NK}
\end{figure}

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Inner_loop_length_average_time_ZKC_30.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Inner_loop_length_maximum_time_ZKC_30.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Inner_loop_length_average_time_ZKC_35.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Inner_loop_length_maximum_time_ZKC_35.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Inner_loop_length_average_time_ZKC_40.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Inner_loop_length_maximum_time_ZKC_40.png}\hfill
\label{fig:parameter_Inner_loop_length_time_ZKC}
\end{figure}

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Inner_loop_length_average_time_ZKW_30.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Inner_loop_length_maximum_time_ZKW_30.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Inner_loop_length_average_time_ZKW_35.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Inner_loop_length_maximum_time_ZKW_35.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Inner_loop_length_average_time_ZKW_40.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Inner_loop_length_maximum_time_ZKW_40.png}\hfill
\label{fig:parameter_Inner_loop_length_time_ZKW}
\end{figure}

\clearpage

Figure below illustrates how with the increase of the inner loop to items ratio, we get higher number of the inner loop iterations of 
Simulated Annealing, thus it takes much more time for the algorithm to converge.

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/simulated_annealing_costs_NK_40_f_0.010000_c_0.990000_h_1.100000_i_1.000000.cs.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/simulated_annealing_costs_NK_40_f_0.010000_c_0.990000_h_1.100000_i_50.000000.cs.png}\hfill
\label{fig:convergence_curve_inner_loop_to_items_ratio_NK}
\end{figure}

\clearpage


\mychapter{Conclusions}
As was illustrated, the values of the parameters significantly influence the number of iterations of the Simulated Annealing, thus its time of 
execution.

Decrease of final temperature leads to the increase in the number of iterations, as it means that an algorithm has to run more to cool down to 
that temperature.

Lower cooling alpha leads to the algorithm converge faster, as the temperature change happens more drastically in the outer loop of the algorithm.

Heating alpha is used to find the initial temperature and with the increase of it, the initial temperature gets higher, which means that the 
algorithm will faster approach the optimum or the value close to the optimum.

With the increase of the inner loop to number of items ratio, the number of iterations, obviously, increases.


\end{document}
