\documentclass[12pt]{book}

\usepackage[acronym]{glossaries}
\usepackage{hyperref}
\usepackage{listings}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage{systeme}
\usepackage{algorithm,algpseudocode}
\usepackage{afterpage}
\usepackage[a4paper, total={6in, 10in}]{geometry}
\usepackage{scrextend}
\usepackage{tcolorbox}
\usepackage{polynom}

\pagestyle{plain}

\newcommand{\example}[1]{
    \begin{addmargin}[1em]{1em}
    \begin{tcolorbox}
    #1
    \end{tcolorbox}
    \end{addmargin}
}

\newcommand{\mychapter}[1]{
    \renewcommand\chaptername{}
    \renewcommand\thechapter{}
    \chapter{#1}
    %\renewcommand\thechapter{#1}
}

\newcommand{\mysection}[1]{
    \renewcommand\thesection{}
    \section{#1}
    %\renewcommand\thechapter{#1}
}

\newcommand{\mysubsection}[1]{
    \renewcommand\thesubsection{}
    \subsection{#1}
    %\renewcommand\thechapter{#1}
}

\newcommand{\mysubsubsection}[1]{
    \renewcommand\thesubsubsection{}
    \subsubsection{#1}
    %\renewcommand\thechapter{#1}
}

\newcommand{\code}[1]{
    {\color{blue}\texttt{#1}}
}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\begin{document}

\mychapter{Description of the MAX-SAT problem}
\textbf{SAT}, so called \textbf{satisfiability}, problem is a problem that given a boolean formula $F = (x_{1}, x_{2}, \cdots, x_{n})$ finds
such configuration (assignment of those boolean variables $x_{i}: i \in \{0,\cdots,n\}$) that the formula $F$ is satisfied.

The boolean formulas that we are interested in are in conjunctive normal formal, e.g. $(x_{1} + \neg x_{2} + x_{3}) \cdot (x_{3} + x_{4} + \neg x_{5})$,
with exactly 3 variables per clause, i.e. 3-SAT problem.

\textbf{MAX-SAT}, the problem we will be solving, consists of weighted variables, i.e. each variable has a weight, and we are trying to find such 
an assignment of variables, that the sum of weights of variables assigned to 1 is maximized.

\mychapter{Description of the Simulated Annealing for MAX-SAT problem}
\mysection{The core of the Simulated Annealing algorithm}
The core of the Simulated Annealing algorithm is same for all problems that are solved via this method and is described in C++ language in the 
listing below.

\begin{lstlisting}[
    language=C++,
    caption=Sumulated Annealing,
    columns=flexible,
    basicstyle={\footnotesize\ttfamily},
    numbers=left,
    numberstyle=\tiny\color{gray},
    keywordstyle=\color{blue},
    commentstyle=\color{dkgreen},
    stringstyle=\color{mauve},
    tabsize=4
]
float temperature = m_initial_temperature;
int inner_loop_length = num_items * m_inner_loop_to_items_ratio;

set_initial_state(sat_instance);

unsigned iterations_count = 0;

// In case of heating temperature (when we want to find an initial temperature),
// we heat until the maximum value of the `float`, but in case of cooling
// (actually running Simulated Annealing with already found initial temperature)
// we check for the convergence (freeze temperature).
while((heating() && temperature < std::numeric_limits<float>::max()) ||
    (cooling() && temperature > m_final_temperature)
) {
    // Number of times the Simulated Annealing got out of the local maximum.
    // Used to calculate the initial temperature by running 
    // Simulated Annealing multiple times, until value of acceptance rate is close to 50%.
    int count_accepted = 0;

    // Number of times the Simulated Annealing didn't get out of local maximum.
    // This member will be used to calculate the acceptance percentage.
    int count_not_accepted = 0;

    for(int i = 0; i < inner_loop_length; ++i) {
        generate_new_state(sat_instance);

        const float cost_change = calculate_cost_change(sat_instance);

        if(cost_change < 0) {
            set_new_state(sat_instance);
            continue;
        }
        
        if(accept(temperature, cost_change)) {
            set_new_state(sat_instance);
            ++count_accepted;
        } else {
            ++count_not_accepted;
        }

        // Write the current cost to the execution results.
        save_current_cost(get_current_cost());

        ++iterations_count;
    }

    if(cooling()) {
        temperature = temperature * m_cooling_alpha;
    } else {
        // As we are starting from the lower temperature and heating up and the higher is
        // the temperature, the higher is the percentage of accepting the given state,
        // the moment a given temperature gives us higher acceptance rate than 50%,
        // we take this temperature as an initial temperature and break from the loop.
        const int total_accept_calls = count_accepted + count_not_accepted;
        int ratio;

        // In case if all the cost change was always negative, thus the state was always 
        // changed and `accept()` was never called or it was called only once, take this 
        // temperature as an initial one.
        if(total_accept_calls <= 1) {
            ratio = 51;
        } else {
            ratio = (int) (((float) count_accepted / (float) total_accept_calls) * 100);
        }
        
        if(ratio >= 50) {
            m_initial_temperature = temperature;
            break;
        }

        // In case of heating up the temperature, we simply multiply it by heating alpha.
        temperature *= m_heating_alpha;
    }
}
\end{lstlisting}

Simulated Annealing consists of these main parts:
\begin{itemize}
    \item Setting the initial temperature and an initial state.
    \item Outer loop that runs until the temperature is greater than or equal to the final temperature in case of cooling
        and until the maximum value of the \code{float} in case of heating.

    \item In the outer loop, we have an inner loop that runs as many times as we set, in which
        \begin{itemize}
            \item We generate a random state via \code{generate\_new\_state()}.
            \item Calculate the cost change via \code{calculate\_cost\_change()} method.
            \item In case if the cost change is less than 0, we set the generated state as the new state.
            \item In case if the cost change is not less than 0, we check it with \code{accept()} method.
        \end{itemize}

    \item Modify the temperature for the next iteration either via cooling alpha (in case of cooling) or via heating alpha
        (in case of heating).
\end{itemize}

Simulated Annealing runs twice:
\begin{enumerate}
    \item When we want to find the initial temperature via setting it to 1, then we calculate the ratio of the number of times the
        \code{accept()} method returned \code{true} and the number of times it ran. If the ratio is above 0.5, i.e. 50\%, then we 
        can stop the Simulated Annealing, as we found an initial temperature that is good enough to be able to escape the local optimum.

    \item When we actually want to find the optimum or close to the optimum cost. We choose the initial temperature from the first run and 
        cool it down with the given alpha value, until the final temperature is reached.
\end{enumerate}

\mysection{Finding feasible state and relaxation of the infeasible states}
Before getting to the important functions of the Simulated Anneling for MAX-SAT problem, I would like to point out the method, which I used to 
find feasible state and eventually get into the state with better cost.

The logic consists in finding the feasible state first and only then try to maximize its cost, as during testing, it was found out that if I 
try to increase the cost, while trying to find the feasible state, it is possible (not that often, but still possible) to get to the solutions 
with high costs, but with some clauses unsatisfied. Thus, the strategy that I used, consisted in trying to find the feasible state with any cost,
even the one that could be far from optimum, and then, try to maximize it via \textbf{relaxation} technique.

Thus, the problem, initially, represents itself as a minimization problem, where we find the configuration with the number of unsatisfied clauses 
equal to 0, then we do the maximization problem based on the weight of variables assigned to 1.

\mysubsection{Relaxation}
\textbf{Relaxation} is a process of \emph{penalizing} the current cost of the given solution via some penalty that is based on the number of 
unsatisfied clauses, in case if we have already found one feasible state and are trying to maximize its cost, i.e. get to the optimum, via 
moving from the feasible state to infeasible one.

The cost calculated via relaxation looks like this:
\begin{align*}
    cost = \text{(sum of weight of variables assigned to 1)} - 1000 \cdot \text{(number of unsatisfied clauses)}
\end{align*}

The reason why sum of weights of variables assigned to 1 is used and not the number of variables assigned to 1, has to do with the fact that we are 
trying to maximize the cost, now it is possible to have two configurations, where the first has more variables assigned to 1 than the second one,
but the second one has overall higher weight than the first one, thus the second configuration will be chosen.

The penalty that was chosen is 1000.

\mysection{Important functions of Simulated Annealing algorithm for MAX-SAT problem}
\begin{itemize}
    \item \code{generate\_new\_state()}: the state is generated based on randomly choosing a variable, the value of which will be changed,
        and randomly assigning it either 0 or 1. After this, we get a new state and we calculate the number of clauses this new state 
        satisfies. In case if any feasible state has already been found, we use the relaxation technique to calculate the new cost.

    \item \code{calculate\_cost\_change()}: in case if any feasible state has already been found, it returns the difference of the previous 
        cost of variable assignments with the new one. But, in case if the feasible state hasn't been found yet, it simply returns the difference 
        of the number of unsatisfied clauses between the new configuration and the old one.

    \item \code{accept()}: return true if the generated random number is less than $e^{-\frac{\Delta{C}}{t}}$, where $\Delta{C}$ is the cost change 
        and $t$ is the temperature.
\end{itemize}

\mychapter{Experiments}
\mysection{Description of experiments}
\mysubsection{Types of intances}
The instances used are \textbf{wuf20-78-M}, \textbf{wuf20-78-N}, \textbf{wuf20-78-Q}, \textbf{wuf20-78-R}, \textbf{wuf20-88-A}, and 
\textbf{wuf20-91-A}.

The reason why these exact instances are used has to do with the fact that all they have exactly the same number of variables, thus we can analyze 
the difference of the algorithm's behavior between these instances and these instances have optimum solutions provided, thus we can calculate the 
error.

\mysubsection{Measurements and parameters}
We will be doing these measurements:
\begin{itemize}
    \item \textbf{Time of execution}: the amount of time it takes an algorithm to finish solving the problem. For Simulated Annealing
        we will take the time of execution after the initial temperature was found, as the first run of Simulated Annealing is done 
        to just find the most appropriate initial temperature and, as we could have given an initial temperature manually, as a 
        parameter, we don't include it to the time of execution.

    \item \textbf{Results quality}: measurement of relative error compared to the optimum, i.e. how far are the obtained results from 
        the optimum. In our case, we will be dealing with already provided instances that have a solution with the optimum cost, thus 
        we will compare our results with provided optimum.

    \item \textbf{Convergence curve}: graph with cost convergence and number of iterations for a given instance. This is needed
        to analyse how the given parameter of the Simulated Annealing influences the cost in the given iteration.
\end{itemize}

Parameters we will be modified to test their impact on measurements and their default values are
\begin{itemize}
    \item \textbf{Final temperature}: the temperature at which, the Simulated Annealing stops and doesn't cool down any further.
        The default value is 0.01 .

    \item \textbf{Cooling alpha}: alpha value that will be used for cooling. The default value is 0.99 .
    \item \textbf{Inner loop to items ratio}: instead of giving a fixed length of the inner loop, we will pass a ratio of the expected 
        length of the inner loop to the number of items in the given instance. This way, the length of the inner loop will be calculated 
        automatically in the algorithm, simply by multiplying the number of items with the value of this parameter. The default value is 10.
\end{itemize}

\mysection{Actual experiments}
\mysubsection{Final temperature}
These values were chosen as values of the final temperature: $\{0.001, 0.005, 0.05, 0.1\}$.

\mysubsubsection{Error}

As it can be seen in the figures below, it is quite difficult to tell if there is any actual correlation between the final temperature and an error.
But, one thing is clear that, once the final temperature changes from $0.001$ to $0.005$, in most of the cases, the error rises, as the final
temperature rises 5 times between these two values.

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Final_temperature_Average_error_wuf20-78-M.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Final_temperature_Maximum_error_wuf20-78-M.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Final_temperature_Average_error_wuf20-78-N.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Final_temperature_Maximum_error_wuf20-78-N.png}\hfill
\label{fig:parameter_Final_temperature_error_1}
\end{figure}

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Final_temperature_Average_error_wuf20-78-Q.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Final_temperature_Maximum_error_wuf20-78-Q.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Final_temperature_Average_error_wuf20-78-R.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Final_temperature_Maximum_error_wuf20-78-R.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Final_temperature_Average_error_wuf20-88-A.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Final_temperature_Maximum_error_wuf20-88-A.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Final_temperature_Average_error_wuf20-91-A.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Final_temperature_Maximum_error_wuf20-91-A.png}\hfill
\label{fig:parameter_Final_temperature_error_2}
\end{figure}

\clearpage

\mysubsubsection{Time of execution}

As it can be seen in figures below, the time of execution decrease with the increase of the final temperature, as it makes sense, as in the case of 
higher final temperature, the freezing temperature is higher and Simulated Annealing converges faster.

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Final_temperature_Average_time_wuf20-78-M.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Final_temperature_Maximum_time_wuf20-78-M.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Final_temperature_Average_time_wuf20-78-N.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Final_temperature_Maximum_time_wuf20-78-N.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Final_temperature_Average_time_wuf20-78-Q.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Final_temperature_Maximum_time_wuf20-78-Q.png}\hfill
\label{fig:parameter_Final_temperature_time_1}
\end{figure}

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Final_temperature_Average_time_wuf20-78-R.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Final_temperature_Maximum_time_wuf20-78-R.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Final_temperature_Average_time_wuf20-88-A.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Final_temperature_Maximum_time_wuf20-88-A.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Final_temperature_Average_time_wuf20-91-A.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Final_temperature_Maximum_time_wuf20-91-A.png}\hfill
\label{fig:parameter_Final_temperature_time_2}
\end{figure}

\clearpage

\mysubsubsection{Convergence curve}
As it can be seen in the figure below, the higher is the final temperature, the faster the optimum (or the solution close to it) is reached, 
as Simulated Annealing converges faster. Plus, these curves also illustrate that we need many less number of iterations in order to find the solution.

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/simulated_annealing_costs_wuf20-78-M_f_f_0.001000_c_0.990000_h_1.100000_i_10.000000.cs.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/simulated_annealing_costs_wuf20-78-M_f_f_0.005000_c_0.990000_h_1.100000_i_10.000000.cs.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.615\textwidth]{Resources/simulated_annealing_costs_wuf20-78-M_f_f_0.050000_c_0.990000_h_1.100000_i_10.000000.cs.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.625\textwidth]{Resources/simulated_annealing_costs_wuf20-78-M_f_f_0.100000_c_0.990000_h_1.100000_i_10.000000.cs.png}\hfill
\label{fig:convergence_curve_final_temperature}
\end{figure}

\clearpage

\mysubsection{Cooling alpha}
These values were chosen as values of the cooling alpha: $\{0.9, 0.95, 0.995\}$.

\mysubsubsection{Error}

Figures below illustrate that in most of the instance types (except for \textbf{wuf20-78-N} and \textbf{wuf20-78-Q} instances), the increase of 
the cooling alpha leads to the decrease in error, which is expected, as then, the Simulated Annealing is cooled down slower, thus it converges 
slower, thus there is higher chance of getting to the optimum or the solution close to it.

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Cooling_alpha_Average_error_wuf20-78-M.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Cooling_alpha_Maximum_error_wuf20-78-M.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Cooling_alpha_Average_error_wuf20-78-N.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Cooling_alpha_Maximum_error_wuf20-78-N.png}\hfill
\label{fig:parameter_Cooling_alpha_error_1}
\end{figure}

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Cooling_alpha_Average_error_wuf20-78-Q.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Cooling_alpha_Maximum_error_wuf20-78-Q.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Cooling_alpha_Average_error_wuf20-78-R.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Cooling_alpha_Maximum_error_wuf20-78-R.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Cooling_alpha_Average_error_wuf20-88-A.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Cooling_alpha_Maximum_error_wuf20-88-A.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Cooling_alpha_Average_error_wuf20-91-A.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Cooling_alpha_Maximum_error_wuf20-91-A.png}\hfill
\label{fig:parameter_Cooling_alpha_error_2}
\end{figure}

\clearpage

\mysubsubsection{Time of execution}

For all instance types, the time of execution increases with the increase of the cooling alpha, which makes sense, as Simulated Annealing 
converges slower with the increase of cooling alpha.

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Cooling_alpha_Average_time_wuf20-78-M.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Cooling_alpha_Maximum_time_wuf20-78-M.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Cooling_alpha_Average_time_wuf20-78-N.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Cooling_alpha_Maximum_time_wuf20-78-N.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Cooling_alpha_Average_time_wuf20-78-Q.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Cooling_alpha_Maximum_time_wuf20-78-Q.png}\hfill
\label{fig:parameter_Cooling_alpha_time_1}
\end{figure}

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Cooling_alpha_Average_time_wuf20-78-R.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Cooling_alpha_Maximum_time_wuf20-78-R.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Cooling_alpha_Average_time_wuf20-88-A.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Cooling_alpha_Maximum_time_wuf20-88-A.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Cooling_alpha_Average_time_wuf20-91-A.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Cooling_alpha_Maximum_time_wuf20-91-A.png}\hfill
\label{fig:parameter_Cooling_alpha_time_2}
\end{figure}

\clearpage

\mysubsubsection{Convergence curve}

Convergence curves also illustrate, how with the increase of cooling alpha, more iterations occur, thus Simulated Annealing converges much slower.

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/simulated_annealing_costs_wuf20-78-M_f_f_0.010000_c_0.900000_h_1.100000_i_10.000000.cs.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/simulated_annealing_costs_wuf20-78-M_f_f_0.010000_c_0.950000_h_1.100000_i_10.000000.cs.png}\hfill
\hspace*{3cm}
\includegraphics[width=0.625\textwidth]{Resources/simulated_annealing_costs_wuf20-78-M_f_f_0.010000_c_0.995000_h_1.100000_i_10.000000.cs.png}\hfill
\label{fig:convergence_curve_cooling_alpha}
\end{figure}

\clearpage

\mysubsection{Inner loop to items ratio}
These values were chosen as values of the inner loop to items ratio: $\{1, 10\}$. The $X$-axis values on the graphs are the actual values of the 
inner loop length, i.e. inner loop to items ratio multiplied by the number of items (variables).

\mysubsubsection{Error}

As it can be seen in the figures below, the average error, in most of the cases, drops with the increase of the inner loop length, as we 
pass through more configurations, there is a higher chance of passing more through good configurations that are close to the optimum, thus 
the average error drops with the increase of the inner loop length. But, the maximum error, mostly, increases, with the increase of the 
inner loop length, as with longer inner loops, we generate more states and there is a higher chance of coming accross a very bad state with 
very low cost.

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Inner_loop_length_Average_error_wuf20-78-M.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Inner_loop_length_Maximum_error_wuf20-78-M.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Inner_loop_length_Average_error_wuf20-78-N.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Inner_loop_length_Maximum_error_wuf20-78-N.png}\hfill
\label{fig:parameter_Inner_loop_length_error_1}
\end{figure}

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Inner_loop_length_Average_error_wuf20-78-Q.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Inner_loop_length_Maximum_error_wuf20-78-Q.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Inner_loop_length_Average_error_wuf20-78-R.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Inner_loop_length_Maximum_error_wuf20-78-R.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Inner_loop_length_Average_error_wuf20-88-A.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Inner_loop_length_Maximum_error_wuf20-88-A.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Inner_loop_length_Average_error_wuf20-91-A.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Inner_loop_length_Maximum_error_wuf20-91-A.png}\hfill
\label{fig:parameter_Inner_loop_length_error_2}
\end{figure}

\clearpage

\mysubsubsection{Time of execution}

With longer inner loop, time of execution increases with all instance types, which is obvious, as Simulated Annealing converges slower.

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Inner_loop_length_Average_time_wuf20-78-M.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Inner_loop_length_Maximum_time_wuf20-78-M.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Inner_loop_length_Average_time_wuf20-78-N.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Inner_loop_length_Maximum_time_wuf20-78-N.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Inner_loop_length_Average_time_wuf20-78-Q.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Inner_loop_length_Maximum_time_wuf20-78-Q.png}\hfill
\label{fig:parameter_Inner_loop_length_time_1}
\end{figure}

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Inner_loop_length_Average_time_wuf20-78-R.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Inner_loop_length_Maximum_time_wuf20-78-R.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Inner_loop_length_Average_time_wuf20-88-A.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Inner_loop_length_Maximum_time_wuf20-88-A.png}\hfill
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/parameter_Inner_loop_length_Average_time_wuf20-91-A.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/parameter_Inner_loop_length_Maximum_time_wuf20-91-A.png}\hfill
\label{fig:parameter_Inner_loop_length_time_2}
\end{figure}

\clearpage

\mysubsubsection{Convergence curve}

Convergence curves illustrate the number of iterations that increase with the increase of the number of the length of the inner loop, which 
makes Simulated Annealing run longer with the same optimum or close to the optimum cost.

\begin{figure}[h]
%\centering
\hspace*{-1.7cm}
\includegraphics[width=0.625\textwidth]{Resources/simulated_annealing_costs_wuf20-78-M_f_f_0.010000_c_0.990000_h_1.100000_i_1.000000.cs.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/simulated_annealing_costs_wuf20-78-M_f_f_0.010000_c_0.990000_h_1.100000_i_50.000000.cs.png}\hfill
\label{fig:convergence_curve_inner_loop_to_items_ratio}
\end{figure}

\clearpage

\mychapter{Conclusions}
\section{Analysis of testing}
During testing it became clear that increase in the final temperature, lead to the decrease in the number of iterations of the algorithm, thus 
in the decrease of the time of execution.

Increase in the cooling alpha, on the other hand, lead to the increase of the number of iterations, as the temperature in the given iteration, 
cooled down much slower with higher cooling alpha, thus it took more time for the algorithm to converge.

Increase in the inner loop to number of items (variables), directly influenced the number of iterations of the algorithm, thus increase of this 
parameter, lead to the increase in time of execution.

When it comes to the errors, in most of the cases, the results were quite random, thus it is difficult to establish a strong correlation between 
parameters and an error. The parts that could be highlighted are:
\begin{itemize}
    \item In most of the instance types, the increase of the final temperature from $0.001$ to $0.005$ lead to the increase in error.
    \item Increase of the cooling alpha, mostly, leads to the decrease in error in most of the instance types.
    \item Increase in the inner loop to items (variables) ratio, lead to the decrease of the average error, although lead to increase of maximum error.
\end{itemize}

\section{Possible improvements}
The algorithm doesn't always find the optimum solution, which has to do with relaxation criteria. As we first solve the minimization problem 
with the number of unsatisfied clauses, then we maximize the cost, once we already found one feasible state, via relaxation, the better relaxation 
criteria would lead to algorithm being able to find a better feasible solution, than an already established one.

I tried to not do minimization based on the number of unsatisfied clauses, then the maximization based on relaxation, and was doing relaxation right 
away, but in that case, there were some cases, when no feasible solution was found, i.e. not all clauses were satisfied. That is the reason why first 
minimization based on the number of unsatisfied clauses, then maximization based on relaxation was chosen. This just shows that a much better 
and sophisticated relaxation technique is needed.

\end{document}
