\documentclass[12pt]{book}

\usepackage[acronym]{glossaries}
\usepackage{hyperref}
\usepackage{listings}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage{systeme}
\usepackage{algorithm,algpseudocode}
\usepackage{afterpage}
\usepackage[a4paper, total={6in, 10in}]{geometry}
\usepackage{scrextend}
\usepackage{tcolorbox}
\usepackage{polynom}

\pagestyle{plain}

\newcommand{\example}[1]{
    \begin{addmargin}[1em]{1em}
    \begin{tcolorbox}
    #1
    \end{tcolorbox}
    \end{addmargin}
}

\newcommand{\lecture}[1]{
    \renewcommand\chaptername{}
    \renewcommand\thechapter{}
    \chapter{Lecture #1}
    \renewcommand\thechapter{#1}
}

\usepackage{hyperref}
\usepackage{tcolorbox}
\usepackage{xcolor}
\usepackage{color}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\newcommand{\code}[1]{
    {\color{blue}\texttt{#1}}
}

\begin{document}

\chapter{Description of algorithms}
\section{Branch and Bound}
Branch and Bound solver for optimisation is very close to the decision version of it with the only difference that there is no target cost
anymore, so an algorithm starts with the highest cost equal to 0 and updates it in the process, as better solutions are found.

\section{Cost/Weight heuristics}
Cost/Weight heuristics are divided into two variants:
\begin{itemize}
    \item Simple heuristic.
    \item Extended heuristic.
\end{itemize}

The basis of this solver consists in sorting the items in the cost/weight ratio order and starting with the item with the highest ratio, 
i.e. item with the greatest cost over the unit of weight, we add items into the knapsack as long as knapsack's total weight is less than
or equal to the maximum allowed weight.

The difference between these two heuristics is that the extended heuristic compares the obtained solution with the item with the highest
cost, thus cutting the maximum possible error to 50\%.

\section{Dynamic programming}
Dynamic programming algorithm relies in heavy usage of memory: for every item that is decided to put into the knapsack, 
the previously measured combinations of items are taken into account.

The heart of this algorithm is decomposition. Two types of decomposition were implemented:
\begin{itemize}
    \item Decomposition by weight: the X coordinates of the dynamic array are items and the Y coordinates are weights decomposed from the 
        lowest possible, i.e. 0, to the maximum weight of the knapsack. So the dimensions of the dynamic array are 
        \code{array[M + 1][N + 1]}, where \code{M} is a maximum possible weight of the knapsack and \code{N} is a number of items.
    
        Algorithm~\ref{lst:dynamic_programming} illustrates a pseudo-code of decomposition by weights.
        \begin{algorithm}
            \caption{Dynamic programming(decomposition by weight)}
            \label{lst:dynamic_programming}
            \begin{algorithmic}[1]
            \For {$i = 0 \text{ \textbf{to} } N - 1$}
                \For {$w = 0 \text{ \textbf{to} } M$}
                    \State $new\_weight \leftarrow items[i].weight + array[w][i].weight$
                    \State $new\_cost \leftarrow items[i].cost + array[w][i].cost$
                    \If {$new\_weight \leq M \land new\_cost > array[new\_weight][i + 1].cost$}
                            \State $array[new\_weight][i + 1].weight = new\_weight$
                            \State $array[new\_weight][i + 1].cost = new\_cost$
                    \EndIf
                    \If {$array[w][i].weight > 0 \land array[w][i].cost > array[w][i + 1].cost$}
                            \State $array[weight][i + 1].weight = array[weight][i].weight$
                            \State $array[weight][i + 1].cost = array[weight][i].cost$
                    \EndIf
                \EndFor
            \EndFor
            \end{algorithmic}
            \end{algorithm}
    
    \item Decomposition by cost: algorithm is close to the decomposition by weight, but now, dynamic array's dimensions look like
        \code{array[C + 1][N + 1]}, where \code{C} is a sum of costs of all items.
\end{itemize}

\section{FPTAS}
FPTAS algorithm is the same algorithm as dynamic programming with decomposition by cost, with the only difference that costs are not 
individual values any more, costs are like chunks/bins, i.e. instead of having individual costs in the rows of the array, we are going to 
have intervals of costs. It is possible to calculate to which chunk/bin a given cost belongs to via the maximum error that we pass to the 
algorithm, as a parameter. The formula for calculating the chunk that the given cost belongs to is
\begin{align*}
    c^{'}_{i} = \lfloor{\dfrac{c_{i}}{\varepsilon \cdot C_{Max}} \cdot n}\rfloor
\end{align*}

Where, $c^{'}_{i}$ is the index of the chunk, $c_{i}$ is the cost, $\varepsilon$ is the error, $C_{Max}$ is the cost of the item with greatest 
cost, and $n$ is the number of items. This formula is taken from \href{https://moodle-vyuka.cvut.cz/mod/page/view.php?id=259938}{NIE-KOP tutorial}.

As, the number of chunks depends on the given error, the higher the error, the less chunks we are going to get, thus interval of costs that each 
chunks spans will be quite high, as with higher error, we don't care much about the precision of the result.


\chapter{Comparison of run-times}
Figure~\ref{fig:time_optimisation} illustrates average and maximum time of execution for all instances for Branch and Bound and Dynamic
Programming solvers. As it can be observed from the figure, the branch and bound solver is usually the slowest one, especially for instances with
greater number of items, as it is a simple DFS with pruning and complexity is exponential. As in most of the instances, the maximum weight of the 
knapsack is lower than the total cost of it, dynamic array in decomposition by cost solver has more rows that in decomposition by weight. 
As cost of the knapsack is very large, decomposition by cost is the slower than decomposition by weight.

\begin{figure}[h]
%\centering
\hspace*{-2.5cm}
\includegraphics[width=0.6\textwidth]{Resources/NK_average_time_optimisation.png}\hfill
\includegraphics[width=0.61\textwidth]{Resources/NK_maximum_time_optimisation.png}\hfill
\hspace*{-2.7cm}
\includegraphics[width=0.625\textwidth]{Resources/ZKC_average_time_optimisation.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/ZKC_maximum_time_optimisation.png}\hfill
\hspace*{-2.7cm}
\includegraphics[width=0.625\textwidth]{Resources/ZKW_average_time_optimisation.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/ZKW_maximum_time_optimisation.png}\hfill
\caption{Comparison of run-times of branch and bound and dynamic programming with decomposition by cost and weight}
\label{fig:time_optimisation}
\end{figure}

\clearpage

Figure~\ref{fig:time_fptas} 
illustrates average and maximum time of execution for all instances for simple and extended cost/weight heuristics and FPTAS with different 
given error values. The fastest solvers are heuristics, because the complexities of those solvers are equal to the complexity of simple sorting,
which is $O(n \cdot \text{log} n)$.

The slowest solver is FPTAS with the lowest given error, as it is very close to plain dynamic programming solver with decomposition by cost.
The greater the given error is, the faster FPTAS solver gets.

\begin{figure}[h]
%\centering
\hspace*{-1.5cm}
\includegraphics[width=0.6\textwidth]{Resources/NK_average_time_fptas.png}\hfill
\includegraphics[width=0.61\textwidth]{Resources/NK_maximum_time_fptas.png}\hfill
\hspace*{-1.5cm}
\includegraphics[width=0.625\textwidth]{Resources/ZKC_average_time_fptas.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/ZKC_maximum_time_fptas.png}\hfill
\hspace*{-1.5cm}
\includegraphics[width=0.625\textwidth]{Resources/ZKW_average_time_fptas.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/ZKW_maximum_time_fptas.png}\hfill
\caption{Comparison of run-times of cost/weight heuristics and FPTAS with different given errors}
\label{fig:time_fptas}
\end{figure}

\chapter{Measurement of errors}
Figures~\ref{fig:NK_error}, ~\ref{fig:ZKC_error}, and ~\ref{fig:ZKW_error} illustrate the average and maximum error and performance
guarantee for heuristics and FPTAS with different given errors. 

As it can be seen, heuristics algorithms usually have higher error than FPTAS algorithms with given error.

\begin{figure}[h]
%\centering
\hspace*{-1.5cm}
\includegraphics[width=0.6\textwidth]{Resources/NK_average_error.png}\hfill
\includegraphics[width=0.61\textwidth]{Resources/NK_maximum_error.png}\hfill
\hspace*{-1.5cm}
\includegraphics[width=0.625\textwidth]{Resources/NK_average_performance_guarantee.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/NK_maximum_performance_guarantee.png}\hfill
\caption{Comparison of errors and performance guarantees of cost/weight heuristics and FPTAS with different given errors for NK instances}
\label{fig:NK_error}
\end{figure}

\begin{figure}[h]
%\centering
\hspace*{-1.5cm}
\includegraphics[width=0.6\textwidth]{Resources/ZKC_average_error.png}\hfill
\includegraphics[width=0.61\textwidth]{Resources/ZKC_maximum_error.png}\hfill
\hspace*{-1.5cm}
\includegraphics[width=0.625\textwidth]{Resources/ZKC_average_performance_guarantee.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/ZKC_maximum_performance_guarantee.png}\hfill
\caption{Comparison of errors and performance guarantees of cost/weight heuristics and FPTAS with different given errors for ZKC instances}
\label{fig:ZKC_error}
\end{figure}

\begin{figure}[h]
%\centering
\hspace*{-1.5cm}
\includegraphics[width=0.6\textwidth]{Resources/ZKW_average_error.png}\hfill
\includegraphics[width=0.61\textwidth]{Resources/ZKW_maximum_error.png}\hfill
\hspace*{-1.5cm}
\includegraphics[width=0.625\textwidth]{Resources/ZKW_average_performance_guarantee.png}\hfill
\hspace*{-0.2cm}
\includegraphics[width=0.615\textwidth]{Resources/ZKW_maximum_performance_guarantee.png}\hfill
\caption{Comparison of errors and performance guarantees of cost/weight heuristics and FPTAS with different given errors for ZKW instances}
\label{fig:ZKW_error}
\end{figure}

\chapter{Discussion}
As it was illustrated, heuristics solvers turned out be the fastest solvers, although, when it comes to error, they give less precise 
results, compared to FPTAS solvers. FPTAS solvers with higher given error, were closer to heuristics solvers by 
time of execution rather than FPTAS solvers with lower error.

\end{document}
